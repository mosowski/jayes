﻿using System.Collections;
using System.Reflection;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Text;
using System.Linq;
#if UNITY_EDITOR
using UnityEngine;
#endif

namespace JS {

	public static class Settings {
		public static int stackTraceLimit = 50;
	}

	public class ParseError : System.Exception {
		internal ParseError(string msg) : base(msg) {
		}
		internal ParseError(string msg, SourceLocation location) : base(msg + " in " + location) {
		}
		internal ParseError(string msg, Token token) : this(msg, token.location) {
		}
	}

	public class JsonError : System.Exception {
		internal JsonError(string json) : base("Invalid Json:\n" + json) {
		}
	}

	public class TypeError : System.Exception {
		internal TypeError(string msg) : base(msg) {
		}
	}

	public class RuntimeError : System.Exception {
		internal RuntimeError(string msg) : base(msg) {
		}
		internal RuntimeError(string msg, System.Exception inner) : base(msg,inner) {
		}
	}

	public class DeserializationException : System.Exception {
		internal DeserializationException(string msg) : base(msg) {
		}
	}

	public class DeserializationConstraintException : System.Exception {
		internal DeserializationConstraintException(string msg) : base(msg) {
		}
	}

	public class SerializationException : System.Exception {
		internal SerializationException(string msg) : base(msg) {
		}
	}

	internal class StationaryStack<T> {
		public T[] buffer = new T[16];
		int length = 0;

		public int Count {
			get {
				return length;
			}
		}

		public void Push(T t) {
			if (length == buffer.Length) {
				T[] newBuffer = new T[length * 2];
				for (int i = 0; i < length; ++i) {
					newBuffer[i] = buffer[i];
				}
				buffer = newBuffer;
			}
			buffer[length++] = t;
		}

		public void UnsafePop() {
			buffer[length - 1] = default(T);
			length--;
		}

		public T UnsafeTop() {
			return buffer[length - 1];
		}
	}


	internal enum TokenType {
		Id,
		Op,
		String,
		Int,
		Float,

		Var, Function, Return, New, For, While, If, Else, Break, Continue, 
		Try, Catch, Finally, Throw, In, Of, InstanceOf, Switch, Case, Default, 
		True, False, Null, Undefined, Delete, Yield, TypeOf, With,
		Eof	
	}

	internal enum OpType {
		RefEq, RefNeq, LogOrAsgn, LogAndAsgn, LShiftAsgn, RShiftAsgn, Eq, Neq, Leq, Geq, LogAnd, LogOr, 
		LShift, RShift, Incr, Decr, IncAsgn, DecrAsgn, MulAsgn, DivAsgn, ModAsgn, BitOrAsgn, BitAndAsgn,  
		LBracket, RBracket, LSqBracket, RSqBracket, LCurBracket, RCurBracket, QMark, Semicolon, Colon, 
		Comma, Dot, Asgn, Less, Greater, Plus, Minus, Mul, Div, Mod, BitAnd, BitOr, BitXor, Not, BitNot, Eof 
	}

	internal class SourceLocation {
		internal int col;
		internal int row;
		internal string filename;
		internal int hashCode;

		internal static SourceLocation nullLocation = new SourceLocation(0, 0, "unknown");

		internal SourceLocation(int col, int row, string filename) {
			this.col = col;
			this.row = row;
			this.filename = filename;
			hashCode = col + row + filename.GetHashCode();
		}

		public override bool Equals(object rhs) {
			return Equals(rhs as SourceLocation);
		}

		public bool Equals(SourceLocation rhs) {
			return rhs != null && col == rhs.col && row == rhs.row && filename == rhs.filename;
		}

		public override int GetHashCode() {
			return hashCode;
		}

		public override string ToString() {
			return filename+":"+(row+1);	
		}
	}

	internal static class Extensions {
		internal static void Clear(this StringBuilder value) {
			value.Length = 0;
			value.Capacity = 0;
		}
	}

	internal class Token {
		internal TokenType type;
		internal string strVal;
		internal double floatVal;
		internal long intVal;
		internal OpType opVal;

		internal SourceLocation location;

		public override string ToString() {
			switch(type) {
				case TokenType.String: return "'"+strVal+"'";
				case TokenType.Id: return strVal;
				case TokenType.Int: return intVal.ToString();
				case TokenType.Float: return floatVal.ToString();
				case TokenType.Op: return opVal.ToString();
			}
			return type.ToString();
		}
	}

	internal class Lexer {

		static string[] opStrMap = {
			"===", // RefEq
			"!==", // RefNeq
			"||=", // LogOrAsgn
			"&&=", // LogAndAsgn
			"<<=", // LShiftAsgn
			">>=", // RShiftAsgn
			"==", // Eq
			"!=", // Neq
			"<=", // Leq
			">=", // Geq
			"&&", // LogAnd
			"||", // LogOr
			"<<", // LShift
			">>", // RShift
			"++", // Incr
			"--", // Decr
			"+=", // IncAsgn
			"-=", // DecrAsgn
			"*=", // MulAsgn
			"/=", // DivAsgn
			"%=", // ModAsgn
			"|=", // BitOrAsgn
			"&=", // BitAndAsgn
			"(", // LBracket
			")", // RBracket
			"[", // LSqBracket
			"]", // RSqBracket
			"{", // LCurBracket
			"}", // RCurBracket
			"?", // QMark
			";", // Semicolon
			":", // Colon
			",", // Comma
			".", // Dot
			"=", // Asgn
			"<", // Less
			">", // Greater
			"+", // Plus
			"-", // Minus
			"*", // Mul
			"/", // Div
			"%", // Mod
			"&", // BitAnd
			"|", // BitOr
			"^", // BitXor
			"!", // Not
			"~", // BitNot
			" " // Eof
		};

		static OpType[] opByLength = (OpType[])System.Enum.GetValues(typeof(OpType));

		static Dictionary<string, TokenType> keywordMap = new Dictionary<string, TokenType>() {
			{ "function", TokenType.Function },
			{ "var", TokenType.Var },
			{ "return", TokenType.Return },
			{ "new", TokenType.New },
			{ "for", TokenType.For },
			{ "while", TokenType.While },
			{ "if", TokenType.If },
			{ "else", TokenType.Else },
			{ "break", TokenType.Break },
			{ "continue", TokenType.Continue },
			{ "try", TokenType.Try},
			{ "catch", TokenType.Catch},
			{ "finally", TokenType.Finally},
			{ "throw", TokenType.Throw},
			{ "in", TokenType.In },
			{ "of", TokenType.Of },
			{ "instanceof", TokenType.InstanceOf },
			{ "switch", TokenType.Switch },
			{ "case", TokenType.Case },
			{ "default", TokenType.Default },
			{ "true", TokenType.True },
			{ "false", TokenType.False },
			{ "null", TokenType.Null },
			{ "undefined", TokenType.Undefined },
			{ "delete", TokenType.Delete },
			{ "yield", TokenType.Yield },
			{ "typeof", TokenType.TypeOf },
			{ "with", TokenType.With },
		};

		string str;
		int ct;
		int row;
		int col;
		string filename;
		StringBuilder sb = new StringBuilder();

		internal Lexer(string str, string filename="") {
			this.str = str + '\0';
			this.filename = filename;
		}

		internal List<Token> Tokenize() {
			ct = 0;
			row = 0;
			col = 0;
			List<Token> tokens = new List<Token>();
			int strLen = str.Length - 1;
			while (ct < strLen) {
				if (str[ct] == ' ' || str[ct] == '\n' || str[ct] == '\r' || str[ct] == '\t') {
					while (str[ct] == ' ' || str[ct] == '\n' || str[ct] == '\r' || str[ct] == '\t') {
						Move();
					}
				} else if (str[ct] == '/' && str[ct+1] == '/') {
					while (ct < str.Length && str[ct] != '\n') {
						Move();
					}
				} else if (str[ct] == '/' && str[ct+1] == '*') {
					Move(2);
					while (str[ct] != '*' || str[ct+1] != '/') {
						Move();
					}
					Move(2);
				} else if (str[ct] >= '0' && str[ct] <= '9') {
					tokens.Add(GetNumber());
				} else if (str[ct] == '\"' || str[ct] == '\'') {
					tokens.Add(GetString());
				} else if (str[ct] >= 'a' && str[ct] <= 'z' || str[ct] >= 'A' && str[ct] <= 'Z' || str[ct] == '_' || str[ct] == '$') {
					tokens.Add(GetIdentifier());
				} else {
					Token opTok = GetOperator();
					if (opTok != null) {
						tokens.Add(opTok);
					} else {
						throw new ParseError("Invalid Token:" + row);
					}
				}
			}
			tokens.Add(new Token() { type = TokenType.Eof });
			return tokens;
		}

		void Move(int n = 1) {
            while (--n >= 0) {
				if (str[ct] == '\n') {
					col = 0;
					row++;
				} else if (str[ct] != '\r') {
					col++;
				}
				ct++;
			}
		}


		Token MarkTok(Token tok) {
			tok.location = new SourceLocation(col, row, filename);
			return tok;
		}

		void AcceptDigits() {
			while (str[ct] >= '0' && str[ct] <= '9') {
				Move();
			}
		}

		Token GetNumber() {
			TokenType tokType = TokenType.Int;
            int start = ct;
			AcceptDigits();
			if (str[ct] == '.') {
				tokType = TokenType.Float;
				Move();
				AcceptDigits();
			}

			if (str[ct] == 'e' || str[ct] == 'E') {
				tokType = TokenType.Float;
				Move();
				if (str[ct] == '-' || str[ct] == '+') {
					Move();
				}
				AcceptDigits();
			}

            string resStr = str.Substring(start, ct - start);
			if (tokType == TokenType.Int) {
				return MarkTok(new Token() {
					type = tokType,
					intVal = (long)double.Parse(resStr, System.Globalization.CultureInfo.InvariantCulture)
				});
			} else {
				return MarkTok(new Token() {
					type = tokType,
					floatVal = double.Parse(resStr, System.Globalization.CultureInfo.InvariantCulture)
				});
			}
		}

#if !WINDOWS_PHONE
		Token GetString() {
			char delimiter = str[ct];
			Move();
            int start = ct;
			sb.Clear();
            while (str[ct] != delimiter) {
                if (str[ct] == '\\') {
                    Move();
                    char c = str[ct];
                    switch (c) {
                        case 'n': c = '\n'; break;
                        case 't': c = '\t'; break;
                        case 'r': c = '\r'; break;
                        case 'a': c = '\a'; break;
                        case '\\': c = '\\'; break;
                        case '"': c = '"'; break;
                        case '\'': c = '\''; break;
                        case '\n': break;
                    }
                    sb.Append(str.Substring(start, ct - start - 1));
					sb.Append(c);
                    Move();
                    start = ct;
                } else {
                    Move();
                }
            }
            sb.Append(str.Substring(start, ct - start));
            Move();
            return MarkTok(new Token {
                type = TokenType.String,
                strVal = sb.ToString() 
            });
        }
#else
		//XXX: For some reason StringBuilder is outperformed by string concatenation on Windows Phone 8.1 with Unity 
		Token GetString() {
			char delimiter = str[ct];
			Move();
            int start = ct;
			string resStr = "";
            while (str[ct] != delimiter) {
                if (str[ct] == '\\') {
                    Move();
                    char c = str[ct];
                    switch (c) {
                        case 'n': c = '\n'; break;
                        case 't': c = '\t'; break;
                        case 'r': c = '\r'; break;
                        case 'a': c = '\a'; break;
                        case '\\': c = '\\'; break;
                        case '"': c = '"'; break;
                        case '\'': c = '\''; break;
                        case '\n': break;
                    }
                    res += str.Substring(start, ct - start - 1) + c;
                    Move();
                    start = ct;
                } else {
                    Move();
                }
            }
            res += str.Substring(start, ct - start);
            Move();
            return MarkTok(new Token {
                type = TokenType.String,
                strVal = resStr
            });
        }
#endif

		Token GetIdentifier() {
            int start = ct;
            while (str[ct] == '_' || str[ct] == '$' || str[ct] >= 'a' && str[ct] <= 'z' || str[ct] >= 'A' && str[ct] <= 'Z' || str[ct] >= '0' && str[ct] <= '9') {
                Move();
            }
            string resStr = str.Substring(start, ct - start);
            return MarkTok(new Token() {
                type = keywordMap.ContainsKey(resStr) ? keywordMap[resStr] : TokenType.Id,
                strVal = resStr
            });
        }

		Token GetOperator() {
			OpType opType = OpType.Eof;
			for (int i = 0; i < opByLength.Length; ++i) {
				string opStr = opStrMap[(int)opByLength[i]];
				int numMatching = 0;
				for (int j = 0; j < opStr.Length; ++j) {
					if (str[ct+j] == opStr[j]) {
						numMatching++;
					} else {
						break;
					}
				}
				if (numMatching == opStr.Length) {
					Move(numMatching);
					opType = opByLength[i];
					break;
				}
			}
			if (opType != OpType.Eof) {
				return MarkTok(new Token() {
					type = TokenType.Op,
					opVal = opType
				});
			} else {
				return null;
			}
		}
	}

	internal class Parser {
		[System.FlagsAttribute]
		enum Flags : int{
			None = 0,
			IsConstructor = 1,
			NoIn = 2,
			LeaveSemicolon = 4,
			NoLabels = 8
		}

		internal static FunctionDefinition ParseFunction(string code) {
			RootNode root = new Parser(code, "function").Parse();
			DiscardedExpr discardedExpr = root.program.children[0] as DiscardedExpr;
			if (discardedExpr != null) {
				FunctionDefinition function = discardedExpr.child as FunctionDefinition;
				if (function != null) {
					function.Compile(new Code());
					return function;
				} else {
					throw new TypeError("Expression " + discardedExpr.child + " is not a function");
				}
			} else {
				throw new TypeError("Expression " + root.program.children[0] + " is not a function");
			}
		}

		Lexer lexer;
		IEnumerator<Token> tokens;
		Token token;

		internal Parser(string code, string filename="") {
			lexer = new Lexer(code, filename);
			tokens = lexer.Tokenize().GetEnumerator();
		}

		internal RootNode Parse() {
			Move();

			RootNode rootNode = new RootNode() {
				program = GetSequence(Flags.None)
			};
			rootNode.program.GetLocals(rootNode.locals);
			return rootNode;
		}

		internal Node ParseJSON() {
			Move();
			return GetExpression(Flags.None);
		}

		void Move() {
			if (tokens.MoveNext()) {
				token = tokens.Current;
				Node.CurrentLocation = token.location;
			} else {
				throw new ParseError("Unexpected end of file");
			}
		}

		void Require(OpType opType) {
			if (!Match(opType)) {
				throw new ParseError("Required operator " + opType, token);
			} else {
				Move();
			}
		}

		void Require(TokenType tokenType) {
			if (!Match(tokenType)) {
				throw new ParseError("Exepcted " + tokenType, token);
			} else {
				Move();
			}
		}

		string RequireId() {
			if (token.type != TokenType.Id) {
				throw new ParseError("Expected identifier, got " + token, token);
			} else {
				string val = token.strVal;
				Move();
				return val;
			}
		}

		bool Match(OpType opType) {
			return token.type == TokenType.Op && token.opVal == opType;
		}

		bool Match(TokenType tokenType) {
			return token.type == tokenType;
		}

		int uniqueVarNameId = 0;
		string GetUniqueVarName() {
			return "__u" + uniqueVarNameId++;
		}

		bool MatchExpr() {
			return Match(TokenType.Id) || Match(TokenType.String) || Match(TokenType.Int) || Match(TokenType.Float)
				|| Match(TokenType.Function) || Match(TokenType.New) || Match(TokenType.Undefined)
				|| Match(TokenType.Null) || Match(TokenType.Yield) || Match(TokenType.TypeOf)
				|| Match(OpType.LBracket) || Match(OpType.Incr) || Match(OpType.Decr);
		}

		Node GetStatement(Flags flags) {
			if (MatchExpr()) {
				Node e = GetMultiExpression(flags);
				if (((flags & Flags.LeaveSemicolon) == 0) && Match(OpType.Semicolon) && !Match(TokenType.Eof)) {
					Require(OpType.Semicolon);
				}
				if (e is SequenceStatement || e is LabeledStatement) { // in case of named function definition or labeled statements
					return e;
				} else {
					return new DiscardedExpr(e);
				}
			} else if (Match(OpType.LCurBracket)) {
				return GetBlock(flags);
			} else if (Match(OpType.Semicolon)) {
				Move();
				return GetStatement(flags);
			} else if (Match(TokenType.Return)) {
				return GetReturn(flags);
			} else if (Match(TokenType.Var)) {
				SequenceStatement vd = (SequenceStatement)GetVarDeclaration(flags);
				for (int i = 0; i < vd.children.Count; ++i) {
					vd.children[i] = new DiscardedExpr(vd.children[i]);
				}
				return vd;
			} else if (Match(TokenType.Delete)) {
				return GetDelete(flags);
			} else if (Match(TokenType.If)) {
				return GetIf(flags);
			} else if (Match(TokenType.Switch)) {
				return GetSwitch(flags);
			} else if (Match(TokenType.While)) {
				return GetWhile(flags);
			} else if (Match(TokenType.For)) {
				return GetFor(flags);
			} else if (Match(TokenType.Try)) {
				return GetTry(flags);
			} else if (Match(TokenType.Throw)) {
				return GetThrow(flags);
			} else if (Match(TokenType.With)) {
				return GetWith(flags);
			} else if (Match(TokenType.Break)) {
				Move();
				string label = "";
				if (Match(TokenType.Id)) {
					label = token.strVal;
					Move();
				}
				Require(OpType.Semicolon);
				return new BreakStatement() { label = label };
			} else if (Match(TokenType.Continue)) {
				Move();
				string label = "";
				if (Match(TokenType.Id)) {
					label = token.strVal;
					Move();
				}
				Require(OpType.Semicolon);
				return new ContinueStatement() { label = label };
			} else {
				throw new ParseError("Unexpected token: " + token.type + ", " + token, token);
			}
		}


		Node GetBlock(Flags flags) {
			Require(OpType.LCurBracket);
			Node seq = GetSequence(flags);
			Require(OpType.RCurBracket);
			return seq;
		}

		SequenceStatement GetSequence(Flags flags) {
			SequenceStatement seq = new SequenceStatement();
			while (!Match(OpType.RCurBracket) && !Match(TokenType.Case) && !Match(TokenType.Default) && token.type != TokenType.Eof) {
				seq.children.Add(GetStatement(flags));
			}
			return seq;
		}

		Node GetReturn(Flags flags) {
			Require(TokenType.Return);
			if (Match(OpType.Semicolon)) {
				Require(OpType.Semicolon);
				return new ReturnStatement();
			} else {
				Node result = new ReturnStatement() {
					child = GetMultiExpression(flags)
				};
				Require(OpType.Semicolon);
				return result;
			}
		}

		Node GetThrow(Flags flags) {
			Require(TokenType.Throw);
			Node result = new ThrowStatement() {
				child = GetMultiExpression(flags)
			};
			Require(OpType.Semicolon);
			return result;
		}

		//NOTE: it's deprecated in ES reference
		Node GetWith(Flags flags) {
			Require(TokenType.With);
			Require(OpType.LBracket);
			Node item = GetMultiExpression(flags);
			Require(OpType.RBracket);
			return new WithStatement() {
				item = item,
				child = GetStatement(flags)
			};
		}

		Node GetYield(Flags flags) {
			Require(TokenType.Yield);
			if (Match(OpType.Mul)) {
				Require(OpType.Mul);
				Node generator = GetMultiExpression(flags);
				//XXX: it should be wrapped in it's own scope
				string varName = GetUniqueVarName();
				return new ForOfStatement() {
					leftSide = new VarDeclaration() { name = varName },
					iterator = generator,
					body = new DiscardedExpr(new YieldStatement() {
						child = new FieldLookup() { name = varName }
					})
				};
			} else {
				Node result = new YieldStatement() {
					child = GetMultiExpression(flags)
				};
				Require(OpType.Semicolon);
				return result;
			}
		}

		Node GetMultiExpression(Flags flags) {
			SequenceStatement seq = new SequenceStatement();
			seq.children.Add(GetExpression(flags));
			while (Match(OpType.Comma)) {
				Move();
				seq.children[seq.children.Count - 1] = new DiscardedExpr(seq.children[seq.children.Count - 1]);
				seq.children.Add(GetExpression(flags));
			}
			if (seq.children.Count > 1) {
				return seq;
			} else {
				return seq.children[0];
			}
		}

		Node GetExpression(Flags flags) {
			Node left = GetTernary(flags);
			if (token.type == TokenType.Op && (token.opVal == OpType.Asgn || token.opVal == OpType.IncAsgn 
				|| Match(OpType.DecrAsgn) || Match(OpType.MulAsgn) || Match(OpType.DivAsgn) || Match(OpType.ModAsgn) 
				|| Match(OpType.LogOrAsgn) || Match(OpType.LogOrAsgn) || Match(OpType.BitOrAsgn) || Match(OpType.BitAndAsgn)
				|| Match(OpType.LShiftAsgn) || Match(OpType.RShiftAsgn))) {
				OpType opType = token.opVal;
				Move();
				Node right = GetExpression(flags);
				Operation asgnOp = Operation.Noop;
				Node newLeft = null;
				switch (opType) {
					case OpType.Asgn: newLeft = new AssignmentExpr() { left = left, right = right }; break;
					case OpType.IncAsgn: asgnOp = Operation.Add; break;
					case OpType.DecrAsgn: asgnOp = Operation.NumSub; break;
					case OpType.MulAsgn: asgnOp = Operation.NumMul; break;
					case OpType.DivAsgn: asgnOp = Operation.NumDiv; break;
					case OpType.ModAsgn: asgnOp = Operation.NumMod; break;
					case OpType.BitOrAsgn: asgnOp = Operation.BitOr; break;
					case OpType.BitAndAsgn: asgnOp = Operation.BitAnd; break;
					case OpType.LShiftAsgn: asgnOp = Operation.Shl; break;
					case OpType.RShiftAsgn: asgnOp = Operation.Shr; break;
					case OpType.LogOrAsgn: 
						newLeft = new AssignmentExpr() { 
							left = left, right = new LogOrExpr() { left = left, right = right } 
						};
						break;
					case OpType.LogAndAsgn:
						newLeft = new AssignmentExpr() { 
							left = left, right = new LogAndExpr() { left = left, right = right } 
						};
						break;
				}
				if (newLeft == null) {
					newLeft = new AssignmentExpr() {
						left = left, right = new BinaryExpr(asgnOp) { left = left, right = right } 
					};
				}
				left = newLeft;
			}
			return left;
		}

		Node GetTernary(Flags flags) {
			Node left = GetLogical(flags);
			if (Match(OpType.QMark)) {
				Move();
				Node positive = GetMultiExpression(flags | Flags.NoLabels);
				Require(OpType.Colon);
				Node negative = GetExpression(flags);
				return new ConditionalExpr() {
					condition = left,
					positive = positive,
					negative = negative
				};
			}
			return left;
		}

		Node GetLogical(Flags flags) {
			return GetLogOr(flags);
		}

		Node GetLogOr(Flags flags) {
			Node left = GetLogAnd(flags);
			while (Match(OpType.LogOr)) {
				Move();
				Node right = GetLogAnd(flags);
				BinaryExpr newLeft = new LogOrExpr();
				newLeft.left = left;
				newLeft.right = right;
				left = newLeft;
			}
			return left;
		}

		Node GetLogAnd(Flags flags) {
			Node left = GetBitOr(flags);
			while (Match(OpType.LogAnd)) {
				Move();
				Node right = GetBitOr(flags);
				BinaryExpr newLeft = new LogAndExpr();
				newLeft.left = left;
				newLeft.right = right;
				left = newLeft;
			}
			return left;
		}

		Node GetBitOr(Flags flags) {
			Node left = GetBitXor(flags);
			while (Match(OpType.BitOr)) {
				Move();
				Node right = GetBitXor(flags);
				BinaryExpr newLeft = new BinaryExpr(Operation.BitOr);
				newLeft.left = left;
				newLeft.right = right;
				left = newLeft;
			}
			return left;
		}

		Node GetBitXor(Flags flags) {
			Node left = GetBitAnd(flags);
			while (Match(OpType.BitXor)) {
				Move();
				Node right = GetBitAnd(flags);
				BinaryExpr newLeft = new BinaryExpr(Operation.BitXor);
				newLeft.left = left;
				newLeft.right = right;
				left = newLeft;
			}
			return left;
		}

		Node GetBitAnd(Flags flags) {
			Node left = GetEquality(flags);
			while (Match(OpType.BitAnd)) {
				Move();
				Node right = GetEquality(flags);
				BinaryExpr newLeft = new BinaryExpr(Operation.BitAnd);
				newLeft.left = left;
				newLeft.right = right;
				left = newLeft;
			}
			return left;
		}


		Node GetEquality(Flags flags) {
			Node left = GetRelational(flags);
			while (Match(OpType.Eq) || Match(OpType.Neq) || Match(OpType.RefEq) || Match(OpType.RefNeq)) {
				Token relToken = token;
				Move();
				Node right = GetRelational(flags);
				BinaryExpr newLeft = null;
				switch (relToken.opVal) {
					case OpType.Eq: newLeft = new BinaryExpr(Operation.RelEq); break;
					case OpType.Neq: newLeft = new BinaryExpr(Operation.RelNeq); break;
					case OpType.RefEq: newLeft = new BinaryExpr(Operation.RelRefEq); break;
					case OpType.RefNeq: newLeft = new BinaryExpr(Operation.RelRefNeq); break;
				}
				newLeft.left = left;
				newLeft.right = right;
				left = newLeft;
			}
			return left;
		}

		Node GetRelational(Flags flags) {
			Node left = GetShift(flags);
			while (Match(OpType.Leq) || Match(OpType.Geq) || Match(OpType.Less) || Match(OpType.Greater) 
				|| ((flags & Flags.NoIn) == 0 && Match(TokenType.In)) || Match(TokenType.InstanceOf)) {
				Token relToken = token;
				Move();
				Node right = GetShift(flags);
				BinaryExpr newLeft = null;
				if (relToken.type == TokenType.Op) {
					switch (relToken.opVal) {
						case OpType.Leq: newLeft = new BinaryExpr(Operation.RelLeq); break;
						case OpType.Geq:
							newLeft = new BinaryExpr(Operation.RelLeq);
							newLeft.left = right;
							newLeft.right = left;
							break;
						case OpType.Less: newLeft = new BinaryExpr(Operation.RelLess); break;
						case OpType.Greater:  // A little twist. In Js there is no > relation
							newLeft = new BinaryExpr(Operation.RelLess); 
							newLeft.left = right;
							newLeft.right = left;
							break;
					}
				} else {
					switch (relToken.type) {
						case TokenType.In: newLeft = new BinaryExpr(Operation.RelIn); break;
						case TokenType.InstanceOf: newLeft = new BinaryExpr(Operation.RelInstanceOf); break;
					}
				}
				if (newLeft.left == null) {
					newLeft.left = left;
					newLeft.right = right;
				}
				left = newLeft;
			}
			return left;
		}

		Node GetShift(Flags flags) {
			Node left = GetSummand(flags);
			while (Match(OpType.LShift) || Match(OpType.RShift)) {
				OpType opType = token.opVal;
				Move();
				Node right = GetSummand(flags);
				BinaryExpr newLeft = null;
				switch (opType) {
					case OpType.LShift: newLeft = new BinaryExpr(Operation.Shl); break;
					case OpType.RShift: newLeft = new BinaryExpr(Operation.Shr); break;
				}
				newLeft.left = left;
				newLeft.right = right;
				left = newLeft;
			}
			return left;
		}

		Node GetSummand(Flags flags) {
			Node left = GetTerm(flags);
			while (Match(OpType.Plus) || Match(OpType.Minus) || Match(OpType.Incr) || Match(OpType.Decr)) {
				OpType opType = token.opVal;
				Move();
				if (opType == OpType.Incr || opType == OpType.Decr) {
					switch (opType) {
						case OpType.Incr: left = new PostIncrementExpr() { child = left }; break;
						case OpType.Decr: left = new PostDecrementExpr() { child = left }; break;
					}
				} else {
					Node right = GetTerm(flags);
					BinaryExpr newLeft = null;
					switch (opType) {
						case OpType.Plus: newLeft = new BinaryExpr(Operation.Add); break;
						case OpType.Minus: newLeft = new BinaryExpr(Operation.NumSub); break;
					}
					newLeft.left = left;
					newLeft.right = right;
					left = newLeft;
				}
			}
			return left;
		}

		Node GetTerm(Flags flags) {
			Node left = GetUnary(flags);
			while (Match(OpType.Mul) || Match(OpType.Div) || Match(OpType.Mod)) {
				OpType opType = token.opVal;
				Move();
				Node right = GetUnary(flags);
				BinaryExpr newLeft = null;
				switch (opType) {
					case OpType.Mul: newLeft = new BinaryExpr(Operation.NumMul); break;
					case OpType.Div: newLeft = new BinaryExpr(Operation.NumDiv); break;
					case OpType.Mod: newLeft = new BinaryExpr(Operation.NumMod); break;
				}
				newLeft.left = left;
				newLeft.right = right;
				left = newLeft;
			}
			return left;
		}

		Node GetUnary(Flags flags) {
			Node left = null;
			if (Match(OpType.Minus) || Match(OpType.Not) || Match(OpType.BitNot) || Match(OpType.Incr) || Match(OpType.Decr) 
				|| Match(OpType.Plus)) {
				OpType opType = token.opVal;
				Move();
				Node right = GetUnary(flags);
				UnaryExpr newLeft = null;
				switch (opType) {
					case OpType.Minus: newLeft = new NegExpr(); break;
					case OpType.Not: newLeft = new NotExpr(); break;
					case OpType.BitNot: newLeft = new BitNotExpr(); break;
					case OpType.Incr: newLeft = new PreIncrementExpr(); break;
					case OpType.Decr: newLeft = new PreDecrementExpr(); break;
					case OpType.Plus: newLeft = new UnaryPlusExpr(); break;
				}
				newLeft.child = right;
				left = newLeft;
			} else {
				left = GetFactor(flags);
			}
			return left;
		}

		Node GetFactor(Flags flags) {
			Node left = null;
			if (Match(OpType.LBracket)) {
				Move();
				left = GetMultiExpression(Flags.None);
				Require(OpType.RBracket);
			} else if (Match(TokenType.True) || Match(TokenType.False) || Match(TokenType.Null)
				|| Match(TokenType.Undefined) || Match(TokenType.Int) || Match(TokenType.Float)
				|| Match(TokenType.String)) {
				Token tok = token;
				Move();
				switch (tok.type) {
					case TokenType.True: left = new BoolExpr() { value = true }; break;
					case TokenType.False: left = new BoolExpr() { value = false }; break;
					case TokenType.Int: left = new NumberExpr() { value = tok.intVal }; break;
					case TokenType.Float: left = new NumberExpr() { value = tok.floatVal }; break;
					case TokenType.String: left = new StringExpr() { value = tok.strVal }; break;
					case TokenType.Null: left = new NullExpr(); break;
					case TokenType.Undefined: left = new UndefinedExpr(); break;
				}
			} else if (Match(TokenType.Function)) {
				left = GetFunctionDefinition(flags);
			} else if (Match(TokenType.New)) {
				left = GetNew(flags);
			} else if (Match(TokenType.Yield)) {
				return GetYield(flags);
			} else if (Match(TokenType.TypeOf)) {
				return GetTypeOf(flags);
			} else if (Match(TokenType.Id)) { 
				string id = token.strVal;
				Move();
				if ((flags & Flags.NoLabels) == 0 && Match(OpType.Colon)) {
					Move();
					Node child = GetStatement(flags);
					if (child is ForStatement) {
						((ForStatement)child).label = id;
						return child;
					} else {
						return new LabeledStatement() { label = id, child = child };
					}
				} else {
					left = new FieldLookup() { name = id };
				}
			} else if (Match(OpType.LSqBracket)) {
				Move();
				ArrayInitializer arrayInitializer = new ArrayInitializer();
				while (!Match(OpType.RSqBracket)) {
					arrayInitializer.items.Add(GetExpression(Flags.None));
					if (Match(OpType.Comma)) {
						Move();
					}
				}
				Require(OpType.RSqBracket);
				left = arrayInitializer;
			} else if (Match(OpType.LCurBracket)) {
				Move();
				ObjectInitializer objectInitializer = new ObjectInitializer();
				while (!Match(OpType.RCurBracket)) {
					ObjectInitializer.FieldInitializer field = new ObjectInitializer.FieldInitializer();
					if (token.type == TokenType.String || token.type == TokenType.Id) {
						field.name = token.strVal;
						Move();
					} else {
						throw new ParseError("Invalid field identifier", token);
					}
					Require(OpType.Colon);
					field.expr = GetExpression(Flags.None);
					objectInitializer.fields.Add(field);
					if (Match(OpType.Comma)) {
						Move();
					}
				}
				Require(OpType.RCurBracket);
				left = objectInitializer;
			}

			while (Match(OpType.Dot) || Match(OpType.LBracket) || Match(OpType.LSqBracket)) {
				if (Match(OpType.Dot)) {
					Move();
					string name = RequireId();
					left = new FieldLookup() {
						child = left,
						name = name 
					};
				} else if (Match(OpType.LSqBracket)) {
					Require(OpType.LSqBracket);
					Node index = GetMultiExpression(Flags.None);
					Require(OpType.RSqBracket);
					left = new IndexLookup() {
						child = left,
						index = index
					};
				} else if (Match(OpType.LBracket)) {
					left = GetFunctionCall(left);
					if ((flags & Flags.IsConstructor) > 0) {
						return left;
					}
				}
			}

			return left;
		}

		Node GetConstructor(Flags flags) {
			return GetFactor(flags | Flags.IsConstructor);
		}

		Node GetFunctionDefinition(Flags flags) {
			Require(TokenType.Function);
			FunctionDefinition def = new FunctionDefinition();
			if (Match(OpType.Mul)) {
				Require(OpType.Mul);
				def.isGenerator = true;
			}
			if (Match(TokenType.Id)) {
				def.name = token.strVal;
				Move();
			}
			Require(OpType.LBracket);
			while (!Match(OpType.RBracket)) {
				if (Match(TokenType.Id)) {
					def.arguments.Add(token.strVal);
				} else {
					throw new ParseError("Unexpected token: " + token.ToString());
				}
				Move();
				if (Match(OpType.Comma)) {
					Move();
				}
			}
			Require(OpType.RBracket);

			def.body = GetBlock(flags);
			def.body.GetLocals(def.locals);

			if (def.name != null) {
				return new SequenceStatement() { 
					children = new List<Node>() {
						new DiscardedExpr(new AssignmentExpr() {
							left = new VarDeclaration() { name = def.name },
							right = def
						})
					}
				};
			} else {
				return def;
			}
		}

		Node GetFunctionCall(Node child) {
			Require(OpType.LBracket);
			FunctionCall call = new FunctionCall();
			call.child = child;
			while (!Match(OpType.RBracket)) {
				call.arguments.Add(GetExpression(Flags.None));
				if (Match(OpType.Comma)) {
					Move();
				}
			}
			Require(OpType.RBracket);
			return call;
		}

		Node GetNew(Flags flags) {
			Require(TokenType.New);
			Node ctor = GetConstructor(flags);
			if (ctor is FunctionCall) {
				ctor = new ConstructorCall() {
					child = ((FunctionCall)ctor).child,
					arguments = ((FunctionCall)ctor).arguments
				};
			} else {
				ctor = new ConstructorCall() {
					child = ctor
				};
			}
			return ctor;
		}

		Node GetVarDeclaration(Flags flags) {
			Require(TokenType.Var);
			SequenceStatement seq = new SequenceStatement();
			VarDeclaration decl = new VarDeclaration();
			seq.children.Add(decl);
			while (Match(TokenType.Id)) {
				decl.name = RequireId();
				if (Match(OpType.Asgn)) {
					Move();
					seq.children[seq.children.Count - 1] = 
						new AssignmentExpr() {
							left = decl,
							right = GetExpression(flags)
						};
				}
				if (Match(OpType.Comma)) {
					seq.children.Add(decl = new VarDeclaration());
					Move();
				}
			}
			if ((flags & Flags.LeaveSemicolon) == 0 && Match(OpType.Semicolon)) {
				Require(OpType.Semicolon);
			}
			return seq;
		}

		Node GetDelete(Flags flags) {
			Require(TokenType.Delete);
			DeleteStatement delete = new DeleteStatement();
			delete.child = GetFactor(flags);
			if ((flags & Flags.LeaveSemicolon) == 0 && Match(OpType.Semicolon)) {
				Require(OpType.Semicolon);
			}
			return delete;
		}

		Node GetWhile(Flags flags) {
			Require(TokenType.While);
			Require(OpType.LBracket);
			WhileStatement whileStatement = new WhileStatement();
			whileStatement.condition = GetMultiExpression(flags);
			Require(OpType.RBracket);
			whileStatement.body = GetStatement(flags);
			return whileStatement;
		}

		Node GetLeftSideForExpr(Flags flags) {
			if (MatchExpr()) {
				return GetMultiExpression(flags);
			} else if (Match(TokenType.Var)) {
				return GetVarDeclaration(flags);
			} else {
				throw new ParseError("Unexpected token: " + token.type, token);
			}
		}

		Node GetFor(Flags flags) {
			Require(TokenType.For);
			Require(OpType.LBracket);
			Node leftSide = null;
			if (!Match(OpType.Semicolon)) {
				leftSide = GetLeftSideForExpr(flags | Flags.NoIn | Flags.LeaveSemicolon);
			}
			if (Match(TokenType.In)) {
				Require(TokenType.In);
				ForInStatement forInStatement = new ForInStatement();
				forInStatement.leftSide = leftSide;
				forInStatement.iterator = GetExpression(flags);
				Require(OpType.RBracket);
				forInStatement.body = GetStatement(flags);
				return forInStatement;
			} else if (Match(TokenType.Of)) {
				Require(TokenType.Of);
				ForOfStatement forOfStatement = new ForOfStatement();
				forOfStatement.leftSide = leftSide;
				forOfStatement.iterator = GetExpression(flags);
				Require(OpType.RBracket);
				forOfStatement.body = GetStatement(flags);
				return forOfStatement;
			} else {
				Require(OpType.Semicolon);
				ForStatement forStatement = new ForStatement();
				if (leftSide != null) {
					forStatement.pre = leftSide;
				}

				if (!Match(OpType.Semicolon)) {
					forStatement.condition = GetMultiExpression(flags | Flags.LeaveSemicolon);
				}
				Require(OpType.Semicolon);
				if (!Match(OpType.RBracket)) {
					forStatement.increment = new DiscardedExpr(GetMultiExpression(flags));
				}
				Require(OpType.RBracket);
				forStatement.body = GetStatement(flags);

				return forStatement;
			}
		}

		Node GetIf(Flags flags) {
			Require(TokenType.If);
			Require(OpType.LBracket);
			IfStatement ifStatement = new IfStatement();
			ifStatement.condition = GetMultiExpression(flags);
			Require(OpType.RBracket);
			ifStatement.positive = GetStatement(flags);
			if (Match(TokenType.Else)) {
				Move();
				ifStatement.negative = GetStatement(flags);
			}
			return ifStatement;
		}

		Node GetSwitch(Flags flags) {
			Require(TokenType.Switch);
			Require(OpType.LBracket);
			SwitchStatement switchStatement = new SwitchStatement();
			switchStatement.value = GetMultiExpression(flags);
			Require(OpType.RBracket);
			Require(OpType.LCurBracket);
			while (Match(TokenType.Case) || Match(TokenType.Default)) {
				if (Match(TokenType.Case)) {
					Require(TokenType.Case);
					switchStatement.matches.Add(GetExpression(flags | Flags.NoLabels));
					Require(OpType.Colon);
					switchStatement.cases.Add(GetSequence(flags));
				} else {
					Require(TokenType.Default);
					Require(OpType.Colon);
					switchStatement.defaultCase = GetSequence(flags);
				}
			}
			Require(OpType.RCurBracket);
			return switchStatement;
		}

		Node GetTry(Flags flags) {
			Require(TokenType.Try);
			TryStatement tryStatement = new TryStatement();
			tryStatement.tryBody = GetStatement(flags);
			if (Match(TokenType.Catch)) {
				Require(TokenType.Catch);
				Require(OpType.LBracket);
				tryStatement.varName = RequireId();
				Require(OpType.RBracket);
				tryStatement.catchBody = GetStatement(flags);
			}
			if (Match(TokenType.Finally)) {
				Require(TokenType.Finally);
				tryStatement.finallyBody = GetStatement(flags);
			}
			return tryStatement;
		}

		Node GetTypeOf(Flags flags) {
			Require(TokenType.TypeOf);
			return new TypeOfExpr() {
				child = GetFactor(flags) 
			};
		}
	}

	internal class Node {
		internal static Node Noop = new Noop();
		internal static SourceLocation CurrentLocation = null;

		internal SourceLocation location;

		internal Node() {
			location = Node.CurrentLocation;
		}

		internal virtual void Compile(Code code) {
			throw new TypeError("NotImplemented for " + this);
		}

		internal virtual void GetLocals(List<string> locals) {
		}
	}

	internal class Noop : Node {
		internal override void Compile(Code code) {}
	}

	internal class RootNode : Node {
		internal SequenceStatement program;
		internal List<string> locals = new List<string>();

		internal override void Compile(Code code) {
			code.currentLocation = location;
			for (int i = 0; i < locals.Count; ++i) {
				code.Emit(Operation.Var, locals[i]);
			}
			program.Compile(code);
			code.Emit(Operation.Leave);
		}
	}

	internal class DiscardedExpr : Node {
		internal Node child;

		internal DiscardedExpr(Node child) { this.child = child; }

		internal override void Compile(Code code) {
			child.Compile(code);
			code.currentLocation = location;
			code.Emit(Operation.Pop);
		}
	
		internal override void GetLocals(List<string> locals) {
			child.GetLocals(locals);
		}
	}

	internal class TryStatement : Node {
		internal Node tryBody;
		internal string varName = "";
		internal Node catchBody = Node.Noop;
		internal Node finallyBody = Node.Noop;

		internal override void Compile(Code code) {
			Code tryCode = new Code();
			Code catchCode = new Code();
			Code finallyCode = new Code();

			tryBody.Compile(tryCode);
			code.currentLocation = location;
			tryCode.Emit(Operation.Leave);

			if (catchBody != Node.Noop) {
				catchCode.Emit(Operation.Try, tryCode);
				Instruction skipCatch = catchCode.Emit(Operation.Jmp, 0);
				catchCode.Emit(Operation.ScopeLLookup, varName);
				catchCode.Emit(Operation.Swap);
				catchCode.Emit(Operation.Store);
				catchCode.Emit(Operation.Pop);
				catchBody.Compile(catchCode);
				code.currentLocation = location;
				skipCatch.param = catchCode.CurrentAddress;
				catchCode.Emit(Operation.Leave);
			}

			if (finallyBody != Node.Noop) {
				if (catchBody != Node.Noop) {
					finallyCode.Emit(Operation.Catch, catchCode);
				} else {
					finallyCode.Emit(Operation.Try, tryCode);
				}
				finallyBody.Compile(finallyCode);
				code.currentLocation = location;
				finallyCode.Emit(Operation.ForwardRet);

				code.Emit(Operation.Finally, finallyCode);
			} else {
				//TODO: Parser should assert catchBody != Node.Noop
				code.Emit(Operation.Catch, catchCode);
			}
		}

		internal override void GetLocals(List<string> locals) {
			tryBody.GetLocals(locals);
			catchBody.GetLocals(locals);
			finallyBody.GetLocals(locals);
		}

	}

	internal class SequenceStatement : Node {
		internal List<Node> children = new List<Node>();

		internal override void GetLocals(List<string> locals) {
			foreach (Node child in children) {
				child.GetLocals(locals);
			}
		}

		internal override void Compile(Code code) {
			foreach (Node child in children) {
				child.Compile(code);
			}
		}
	}

	internal class IfStatement : Node {
		internal Node condition;
		internal Node positive;
		internal Node negative = Node.Noop;

		internal override void GetLocals(List<string> locals) {
			condition.GetLocals(locals);
			positive.GetLocals(locals);
			negative.GetLocals(locals);
		}

		internal override void Compile(Code code) {
			condition.Compile(code);
			code.currentLocation = location;
			Instruction jz = code.Emit(Operation.Jz);
			positive.Compile(code);
			code.currentLocation = location;
			Instruction jmp = code.Emit(Operation.Jmp, code.CurrentAddress);
			jz.param = code.CurrentAddress;
			negative.Compile(code);
			jmp.param = code.CurrentAddress;
		}
	}

	internal class LabeledStatement : Node {
		internal string label;
		internal Node child;

		internal override void GetLocals(List<string> locals) {
			child.GetLocals(locals);
		}

		internal override void Compile(Code code) {
			code.BeginLabel(label);
			child.Compile(code);
			code.EndLabel();
		}
	}

	internal class SwitchStatement : Node {
		internal Node value;
		internal List<Node> matches = new List<Node>();
		internal List<Node> cases = new List<Node>();
		internal Node defaultCase = Node.Noop;

		internal override void GetLocals(List<string> locals) {
			for (int i = 0; i < cases.Count; ++i) {
				cases[i].GetLocals(locals);
			}
			defaultCase.GetLocals(locals);
		}

		internal override void Compile(Code code) {
			value.Compile(code);
			code.currentLocation = location;

			List<Instruction> jumps = new List<Instruction>();

			for (int i = 0; i < matches.Count; ++i) {
				matches[i].Compile(code);
				code.currentLocation = location;
				code.Emit(Operation.RelEqInvPeekL);
				jumps.Add(code.Emit(Operation.Jnz, 0));
			}
			Instruction jmpDefault = code.Emit(Operation.Jmp, 0);

			code.BeginLabel(); 
			for (int i = 0; i < cases.Count; ++i) {
				jumps[i].param = code.CurrentAddress;
				cases[i].Compile(code);
			}
			jmpDefault.param = code.CurrentAddress;
			defaultCase.Compile(code);
			code.EndLabel();
			code.Emit(Operation.Pop);
		}
	}

	internal class ForStatement : Node {
		internal Node pre = Node.Noop;
		internal Node condition;
		internal Node increment = Node.Noop;
		internal Node body;
		internal string label = "";

		internal override void GetLocals(List<string> locals) {
			pre.GetLocals(locals);
			body.GetLocals(locals);
		}

		internal override void Compile(Code code) {
			pre.Compile(code);
			int beginAddr = code.CurrentAddress;
			code.BeginLabel();
			if (label != "") {
				code.BeginLabel(label);
			}
			Instruction jz = null;
			if (condition != null) {
				condition.Compile(code);
				code.currentLocation = location;
				jz = code.Emit(Operation.Jz);
			}
			body.Compile(code);
			code.MarkContinueAddress();
			if (label != "") {
				code.MarkContinueAddress(label);
			}
			increment.Compile(code);
			code.currentLocation = location;
			code.Emit(Operation.Jmp, beginAddr);
			if (jz != null) {
				jz.param = code.CurrentAddress;
			}
			if (label != "") {
				code.EndLabel();
			}
			code.EndLabel();
		}
	}

	internal class WhileStatement : Node {
		internal Node condition;
		internal Node body;

		internal override void GetLocals(List<string> locals) {
			condition.GetLocals(locals);
			body.GetLocals(locals);
		}

		internal override void Compile(Code code) {
			code.BeginLabel();
			code.MarkContinueAddress();
			condition.Compile(code);
			code.currentLocation = location;
			Instruction jz = code.Emit(Operation.Jz);
			body.Compile(code);
			code.currentLocation = location;
			code.Emit(Operation.Continue, "");
			jz.param = code.CurrentAddress;
			code.EndLabel();
		}
	}

	internal class ForOfStatement : Node {
		internal Node leftSide;
		internal Node iterator;
		internal Node body;

		internal override void GetLocals(List<string> locals) {
			leftSide.GetLocals(locals);
			body.GetLocals(locals);
		}

		internal override void Compile(Code code) {
			iterator.Compile(code);
			code.currentLocation = location;
			code.BeginLabel();
			code.MarkContinueAddress();
			code.Emit(Operation.Dup, 1);
			leftSide.Compile(code);
			code.currentLocation = location;
			code.Last.MakeLLookup();
			code.Emit(Operation.Swap);
			code.Emit(Operation.LookupName, "next");
			code.Emit(Operation.Call, 0);
			code.Emit(Operation.Dup, 1);
			code.Emit(Operation.LookupName, "done");
			Instruction jnz = code.Emit(Operation.Jnz, 0);
			code.Emit(Operation.LookupName, "value");
			code.Emit(Operation.Store);
			code.Emit(Operation.Pop);
			body.Compile(code);
			code.currentLocation = location;
			code.Emit(Operation.Continue, "");
			jnz.param = code.CurrentAddress;
			code.Emit(Operation.Pop);
			code.Emit(Operation.Pop);
			code.EndLabel();
		}
	}

	internal class ForInStatement : Node {
		internal Node leftSide;
		internal Node iterator;
		internal Node body;

		internal override void Compile(Code code) {
			iterator.Compile(code);
			code.currentLocation = location;
			code.Emit(Operation.LookupName, "__iterableKeys__");
			code.Emit(Operation.Call, 0);
			code.BeginLabel();
			code.MarkContinueAddress();
			code.Emit(Operation.Dup, 1);
			leftSide.Compile(code);
			code.currentLocation = location;
			code.Last.MakeLLookup();
			code.Emit(Operation.Swap);
			code.Emit(Operation.LookupName, "next");
			code.Emit(Operation.Call, 0);
			code.Emit(Operation.Dup, 1);
			code.Emit(Operation.RelIsValue, Data.EndOfIterator);
			Instruction jnz = code.Emit(Operation.Jnz, 0);
			code.Emit(Operation.Store);
			code.Emit(Operation.Pop);
			body.Compile(code);
			code.currentLocation = location;
			code.Emit(Operation.Continue, "");
			jnz.param = code.CurrentAddress;
			code.Emit(Operation.Pop);
			code.Emit(Operation.Pop);
			code.EndLabel();
		}

		internal override void GetLocals(List<string> locals) {
			iterator.GetLocals(locals);
			leftSide.GetLocals(locals);
			body.GetLocals(locals);
		}

	}


	internal class ReturnStatement : Node {
		internal Node child;

		internal override void Compile(Code code) {
			code.currentLocation = location;
			if (child != null) {
				child.Compile(code);
				code.currentLocation = location;
			} else {
				code.Emit(Operation.PushData, Data.Undefined);
			}
			code.Emit(Operation.Ret);
		}
	}

	internal class ThrowStatement : Node {
		internal Node child;

		internal override void Compile(Code code) {
			child.Compile(code);
			code.currentLocation = location;
			code.Emit(Operation.Throw);
		}
	}

	internal class WithStatement : Node {
		internal Node item;
		internal Node child;

		internal override void GetLocals(List<string> locals) {
			child.GetLocals(locals);
		}

		internal override void Compile(Code code) {
			item.Compile(code);
			code.currentLocation = location;
			Instruction with = code.Emit(Operation.With);
			child.Compile(code);
			code.currentLocation = location;
			code.Emit(Operation.Leave);
			with.param = code.CurrentAddress;
		}
	}

	internal class YieldStatement : Node {
		internal Node child;

		internal override void Compile(Code code) {
			child.Compile(code);
			code.currentLocation = location;
			code.Emit(Operation.Yield);
		}
	}

	internal class BreakStatement : Node {
		internal string label = "";

		internal override void Compile(Code code) {
			code.currentLocation = location;
			code.Emit(Operation.Break, label);
		}
	}

	internal class ContinueStatement : Node {
		internal string label = "";

		internal override void Compile(Code code) {
			code.currentLocation = location;
			code.Emit(Operation.Continue, label);
		}
	}

	internal class DeleteStatement : Node {
		internal Node child;

		internal override void Compile(Code code) {
			child.Compile(code);
			code.Last.MakeLLookup();
			code.currentLocation = location;
			code.Emit(Operation.Del);
		}
	}

	internal class TypeOfExpr : Node {
		internal Node child;

		internal override void Compile(Code code) {
			child.Compile(code);
			code.currentLocation = location;
			code.Emit(Operation.TypeOf);
		}
	}

	
	internal class ObjectInitializer : Node {
		internal class FieldInitializer {
			internal string name;
			internal Node expr;
		}

		internal List<FieldInitializer> fields = new List<FieldInitializer>();

		internal override void Compile(Code code) {
			code.currentLocation = location;
			code.Emit(Operation.PushData, Object.ObjectConstructor);
			code.Emit(Operation.CtorCall, 0);
			if (fields.Count > 0) {
				code.Emit(Operation.Dup, fields.Count);
			}
			foreach (FieldInitializer field in fields) {
				code.Emit(Operation.LLookupName, field.name);
				field.expr.Compile(code);
				code.currentLocation = location;
				code.Emit(Operation.Store);
				code.Emit(Operation.Pop);
			}
		}
	}

	internal class ArrayInitializer : Node {
		internal List<Node> items = new List<Node>();

		internal override void Compile(Code code) {
			code.currentLocation = location;
			code.Emit(Operation.PushData, Array.ArrayConstructor);
			code.Emit(Operation.CtorCall, 0);
			if (items.Count > 0) {
				code.Emit(Operation.Dup, items.Count + 1);
				code.Emit(Operation.LLookupName, "length");
				code.Emit(Operation.PushData, new Number(items.Count));
				code.Emit(Operation.Store);
				code.Emit(Operation.Pop);
				for (int i = 0; i < items.Count; ++i) {
					code.Emit(Operation.LLookupName, i.ToString());
					items[i].Compile(code);
					code.currentLocation = location;
					code.Emit(Operation.Store);
					code.Emit(Operation.Pop);
				}
			}
		}
	}

	internal class VarDeclaration : Node {
		internal string name;

		internal override void Compile(Code code) {
			code.Emit(Operation.ScopeLookup, name);
		}

		internal override void GetLocals(List<string> locals) {
			locals.Add(name);
		}
	}

	internal class FunctionDefinition : Node {
		internal string name;
		internal List<string> arguments = new List<string>();
		internal List<string> locals = new List<string>();
		internal Node body;

		internal Code code;
		internal bool isGenerator;

		internal override void Compile(Code code) {
			this.code = new Code();
			body.Compile(this.code);
			if (isGenerator) {
				this.code.Emit(Operation.YieldBreak);
			} else {
				this.code.Emit(Operation.PushData, Data.Undefined);
				this.code.Emit(Operation.Ret);
			}

			code.currentLocation = location;
			code.Emit(Operation.PushFunction, this);
		}

		internal override void GetLocals(List<string> locals) {
			if (name != null) {
				locals.Add(name);
			}
		}
	}

	internal class FunctionCall : Node {
		internal Node child;
		internal List<Node> arguments = new List<Node>();

		internal override void Compile(Code code) {
			for (int i = 0; i < arguments.Count; ++i) {
				arguments[i].Compile(code);
			}
			child.Compile(code);
			code.currentLocation = location;
			code.Emit(Operation.Call, arguments.Count);
		}
	}

	internal class ConstructorCall : Node {
		internal Node child;
		internal List<Node> arguments = new List<Node>();

		internal override void Compile(Code code) {
			for (int i = 0; i < arguments.Count; ++i) {
				arguments[i].Compile(code);
			}
			child.Compile(code);
			code.currentLocation = location;
			code.Emit(Operation.CtorCall, arguments.Count);
		}
	}

	internal class FieldLookup : Node {
		internal Node child;
		internal string name;

		internal override void Compile(Code code) {
			if (child != null) {
				child.Compile(code);
				code.currentLocation = location;
				code.Emit(Operation.LookupName, name);
			} else {
				code.currentLocation = location;
				code.Emit(Operation.ScopeLookup, name);
			}
		}
	}

	internal class IndexLookup : Node {
		internal Node child;
		internal Node index;

		internal override void Compile(Code code) {
			index.Compile(code);
			child.Compile(code);
			code.currentLocation = location;
			code.Emit(Operation.Lookup);
		}
	}


	internal class AssignmentExpr : Node {
		internal Node left;
		internal Node right;

		internal override void Compile(Code code) {
			left.Compile(code);
			code.Last.MakeLLookup();
			right.Compile(code);
			code.currentLocation = location;
			code.Emit(Operation.Store);
		}

		internal override void GetLocals(List<string> locals) {
			left.GetLocals(locals);
			right.GetLocals(locals);
		}
	}

	internal class ConditionalExpr : IfStatement {
	}

	internal class BinaryExpr : Node {
		internal Node left;
		internal Node right;
		internal Operation operation;

		internal BinaryExpr(Operation operation) { 
			this.operation = operation;
		}

		internal override void Compile(Code code) {
			right.Compile(code);
			left.Compile(code);
			code.currentLocation = location;
			code.Emit(operation);
		}
	}

	internal class UnaryExpr : Node {
		internal Node child;
	}

	internal class LogAndExpr : BinaryExpr {
		internal LogAndExpr() : base(Operation.LogAnd) {}

		internal override void Compile(Code code) {
			left.Compile(code);
			code.currentLocation = location;
			Instruction jz = code.Emit(Operation.JzPeek);
			code.Emit(Operation.Pop);
			right.Compile(code);
			jz.param = code.CurrentAddress;
		}
	}

	internal class LogOrExpr : BinaryExpr {
		internal LogOrExpr() : base(Operation.LogOr) {}

		internal override void Compile(Code code) {
			left.Compile(code);
			code.currentLocation = location;
			Instruction jnz = code.Emit(Operation.JnzPeek);
			code.Emit(Operation.Pop);
			right.Compile(code);
			jnz.param = code.CurrentAddress;
		}
	}

	internal class PostIncrementExpr : UnaryExpr {
		internal override void Compile(Code code) {
			child.Compile(code);
			code.Last.MakeLLookup();
			child.Compile(code);
			code.currentLocation = location;
			code.Emit(Operation.NumPostInc);
		}
	}

	internal class PostDecrementExpr : UnaryExpr {
		internal override void Compile(Code code) {
			child.Compile(code);
			code.Last.MakeLLookup();
			child.Compile(code);
			code.currentLocation = location;
			code.Emit(Operation.NumPostDec);
		}
	}

	internal class PreIncrementExpr : UnaryExpr {
		internal override void Compile(Code code) {
			child.Compile(code);
			code.Last.MakeLLookup();
			child.Compile(code);
			code.currentLocation = location;
			code.Emit(Operation.NumPreInc);
		}
	}

	internal class PreDecrementExpr : UnaryExpr {
		internal override void Compile(Code code) {
			child.Compile(code);
			code.Last.MakeLLookup();
			child.Compile(code);
			code.currentLocation = location;
			code.Emit(Operation.NumPreDec);
		}
	}

	internal class NegExpr : UnaryExpr {
		internal override void Compile(Code code) {
			child.Compile(code);
			code.currentLocation = location;
			code.Emit(Operation.NumNeg);
		}
	}

	internal class NotExpr: UnaryExpr {
		internal override void Compile(Code code) {
			child.Compile(code);
			code.currentLocation = location;
			code.Emit(Operation.LogNot);
		}
	}

	internal class BitNotExpr : UnaryExpr {
		internal override void Compile(Code code) {
			child.Compile(code);
			code.currentLocation = location;
			code.Emit(Operation.BitNot);
		}
	}

	internal class UnaryPlusExpr : UnaryExpr {
		internal override void Compile(Code code) {
			child.Compile(code);
			code.currentLocation = location;
			code.Emit(Operation.ToNum);
		}
	}

	internal class BoolExpr : Node {
		internal bool value;

		internal override void Compile(Code code) {
			code.currentLocation = location;
			code.Emit(Operation.PushData, new Bool(value));
		}
	}

	internal class NumberExpr : Node {
		internal double value;

		internal override void Compile(Code code) {
			code.currentLocation = location;
			code.Emit(Operation.PushData, new Number(value));
		}
	}

	internal class StringExpr : Node {
		internal string value;

		internal override void Compile(Code code) {
			code.currentLocation = location;
			code.Emit(Operation.PushData, new String(value));
		}
	}

	internal class NullExpr : Node {
		internal override void Compile(Code code) {
			code.currentLocation = location;
			code.Emit(Operation.PushData, Data.Null);
		}
	}

	internal class UndefinedExpr : Node {
		internal override void Compile(Code code) {
			code.currentLocation = location;
			code.Emit(Operation.PushData, Data.Undefined);
		}
	}

	internal enum Operation {
		Call, CtorCall, 
		Var, ScopeLookup, ScopeLLookup, LookupName, LLookupName, Lookup, LLookup, Store, Del,
		PushData, PushFunction, Pop, Dup, Swap,
		RelEq, RelEqInv, RelEqInvPeekL, RelNeq, RelLeq, RelLess, RelIn, RelInstanceOf, RelRefEq, RelRefNeq,
		RelIsValue,
		LogOr, LogAnd, LogNot,
		BitOr, BitAnd, BitXor, BitNot, Shl, Shr,
		NumSub, NumMul, NumDiv, NumMod, NumPostInc, NumPostDec, NumPreInc, NumPreDec, NumNeg,
		Add, ToNum, TypeOf,
		Try, Catch, Finally, Throw, ForwardRet, 
		Break, Continue, Ret, Leave, Jmp, Jz, Jnz, JnzPeek, JzPeek, Yield, YieldBreak, With,
		Noop
	}

	internal class Instruction {
		internal Operation operation;
		internal object param;
		internal SourceLocation location;

		internal void MakeLLookup() {
			if (operation == Operation.LookupName) {
				operation = Operation.LLookupName;
			} else if (operation == Operation.Lookup) {
				operation = Operation.LLookup;
			} else if (operation == Operation.ScopeLookup) {
				operation = Operation.ScopeLLookup;
			}
		}

		public override string ToString() {
			return operation.ToString() + " (" + param + ")";
		}
	}

	internal class Code {
		internal class Label {
			internal string label;
			internal int instructionOffset;
			internal int continueAddress = -1;
		}

		internal List<Instruction> code;
		internal List<Label> labelStack;
		internal Stack<List<Instruction>> throwStack;
		internal SourceLocation currentLocation;

		internal Code() {
			code = new List<Instruction>();
			labelStack = new List<Label>();
			throwStack = new Stack<List<Instruction>>();
		}

		internal void BeginLabel(string label = "") {
			Label l = new Label();
			l.instructionOffset = CurrentAddress;
			l.label = label;
			labelStack.Add(l);
		}

		internal void MarkContinueAddress(string label = "") {
			for (int i = labelStack.Count - 1; i >= 0; --i) { 
				if (labelStack[i].label == label) {
					labelStack[i].continueAddress = CurrentAddress;
					return;
				}
			}
			throw new ParseError("Can't find label \"" + label + "\"", code[CurrentAddress - 1].location);
		}

		internal void EndLabel() {
			Label l = labelStack[labelStack.Count - 1];
			labelStack.RemoveAt(labelStack.Count - 1);
			for (int i = l.instructionOffset; i < CurrentAddress; ++i) {
				Instruction ins = code[i];
				if (ins.operation == Operation.Break) {
					if ((string)ins.param == l.label) {
						ins.operation = Operation.Jmp;
						ins.param = CurrentAddress;
					}
				} else if (ins.operation == Operation.Continue && l.continueAddress != -1) {
					if ((string)ins.param == l.label) {
						ins.operation = Operation.Jmp;
						ins.param = l.continueAddress;
					}
				}
			}
		}

		internal Instruction Emit(Operation operation, object param = null) {
			Instruction ins = new Instruction() { operation = operation, param = param, location = currentLocation };
			code.Add(ins);
			return ins;
		}

		internal int CurrentAddress { 
			get { return code.Count; }
		}

		internal Instruction Last { 
			get { return code[code.Count - 1]; }
		}

		internal Instruction this[int n] {
			get { return code[n]; }
		}

		public override string ToString() {
			string r = "";
			foreach (Instruction i in code) {
				r += i + "\n";
			}
			return r;
		}

		internal static Code Leave;

		static Code() {
			Leave = new Code();
			Leave.Emit(Operation.Leave);
		}
	}

	internal class Frame {
		internal enum Type {
			Regular, Try, Catch, Finally
		}

		internal Type type = Type.Regular;
		internal int ip;
		internal Code code;
		internal Scope scope;
		internal StationaryStack<Ref> valueStack = new StationaryStack<Ref>();
	}

	internal class StackTraceEntry {
	}

	internal class CallStackTraceEntry : StackTraceEntry {
		internal SourceLocation location;

		override public string ToString() {
			return location.ToString();
		}
	}

	internal class ContextSwitchStackTraceEntry : StackTraceEntry {
		internal int machineId;

		override public string ToString() {
			return "Context switch to " + machineId;
		}
	}

	internal static class StackTrace {
		internal static List<StackTraceEntry> stackTrace = new List<StackTraceEntry>();

		internal static void RecordCall(Instruction instruction) {
			if (instruction != null) {
				stackTrace.Add(new CallStackTraceEntry() { location = instruction.location });
				if (stackTrace.Count > Settings.stackTraceLimit) {
					stackTrace.RemoveAt(0);
				}
			}
		}

		internal static void RecordContextSwitch(Machine machine) {
			stackTrace.Add(new ContextSwitchStackTraceEntry() { machineId = machine.uniqueId });
			if (stackTrace.Count > Settings.stackTraceLimit) {
				stackTrace.RemoveAt(0);
			}
		}

		internal static string GetLog(string sourcemap) {
			string log = "";
			if (sourcemap != "") {
				SourceMap sm = SourceMap.Parse(sourcemap);
				for (int i = stackTrace.Count - 1; i >= 0; --i) {
					if (stackTrace[i] is CallStackTraceEntry) {
						log += sm.GetLocation(((CallStackTraceEntry)stackTrace[i]).location.row, 0) + "\n";
					} else {
						log += stackTrace[i] + "\n";
					}
				}
				return log;
			} else {
				for (int i = stackTrace.Count - 1; i >= 0; --i) {
					log += stackTrace[i] + "\n";
				}
				return log;
			}
		}

	}

	internal class Machine { 
		internal bool hasException;
		internal bool isTryCatchInterrupted;
		internal StationaryStack<Frame> frameStack;
		internal Instruction currentInstruction;
		internal Frame activeFrame;
		internal StationaryStack<Ref> activeStack;
		internal int uniqueId;

		internal static int uniqueIdGen = 0;

		internal Machine() {
			uniqueId = uniqueIdGen++;

			frameStack = new StationaryStack<Frame>();
			frameStack.Push(activeFrame = new Frame()); // "root" frame, just for program result storage
			activeStack = activeFrame.valueStack;
		}

		internal void Run() {
			UnsafeRun();
		}

		void UnsafeRun() {
			while (frameStack.Count > 1) {
				if (hasException) {
					if (activeFrame.type == Frame.Type.Catch) {
						hasException = false;
						activeFrame.ip = 2;
					} else if (activeFrame.type != Frame.Type.Finally) {
						PropagateResult();
						continue;
					}
				} else if (isTryCatchInterrupted) {
					if (activeFrame.type == Frame.Type.Try || activeFrame.type == Frame.Type.Catch) {
						PropagateResult();
						continue;
					} else if (activeFrame.type != Frame.Type.Finally) {
						PropagateResult();
						isTryCatchInterrupted = false;
					}
				}

				Instruction ins = activeFrame.code.code[activeFrame.ip++];
				if (Machine.Current != this) {
					Machine.Current = this;
					StackTrace.RecordContextSwitch(this);
				}
				currentInstruction = ins;
#if JS_VERBOSE
				JS.VerboseLog("I: " + ins.ToString());
#endif

				JS.GetProfiler().TrySample(ins.location);

				switch (ins.operation) {
					case Operation.Add: {
						Data left = PopData(), right = PopData();
						if (left is Number && right is Number) {
							Push(new Number(left.AsFloat() + right.AsFloat()));
						} else {
							Push(new String(left.AsString() + right.AsString()));
						}
						break;
					}

					case Operation.RelLess: {
						Data left = PopData(), right = PopData();
						if (left is Number && right is Number) {
							Push(new Bool(left.AsFloat() < right.AsFloat()));
						} else {
							Push(new Bool(left.AsString().CompareTo(right.AsString()) < 0));
						}
						break;
					}
					case Operation.RelLeq: {
						Data left = PopData(), right = PopData();
						if (left is Number && right is Number) {
							Push(new Bool(left.AsFloat() <= right.AsFloat()));
						} else {
							Push(new Bool(left.AsString().CompareTo(right.AsString()) <= 0));
						}
						break;
					}
					case Operation.RelEq: {
						Data left = PopData(), right = PopData();
						Push(new Bool(Data.DataEquals(left, right)));
						break;
					}
					case Operation.RelNeq: {
						Data left = PopData(), right = PopData();
						Push(new Bool(!Data.DataEquals(left, right)));
						break;
					}
					case Operation.RelEqInv: {
						Data right = PopData(), left = PopData();
						Push(new Bool(Data.DataEquals(left, right)));
						break;
					}
					case Operation.RelEqInvPeekL: {
						Data right = PopData(), left = PeekData();
						Push(new Bool(Data.DataEquals(left, right)));
						break;
					}
					case Operation.RelRefEq: {
						Data left = PopData(), right = PopData();
						Push(new Bool(left.GetType() == right.GetType() && left.StrictlyEquals(right)));
						break;
					}
					case Operation.RelRefNeq: {
						Data left = PopData(), right = PopData();
						Push(new Bool(left.GetType() != right.GetType() || !left.StrictlyEquals(right)));
						break;
					}
					case Operation.RelInstanceOf: {
						Data left = PopData(), right  = PopData();
						bool result = false;
						if (!(right is Function || right is NativeClass)) {
							throw new TypeError("Right side of instance of has to be a function at TODO:Location");
						} else {
							Data prototype = right.GetProperty("prototype").data;
							do {
								left = left.proto;
								if (left is Undefined || left is Null || left == null) {
									result = false;
									break;
								} else if (left == prototype) {
									result = true;
									break;
								}
							} while (!(left is Undefined));
							Push(new Bool(result));
						}
						break;
					}
					case Operation.RelIn: {
						Data left = PopData(), right  = PopData();
						Push(new Bool(right.HasProperty(left.AsString())));
						break;
					}
					case Operation.RelIsValue: {
						Data left = PopData();
						Push(new Bool(left == (Data)ins.param));
						break;
					}

					case Operation.NumSub: {
						Data left = PopData(), right = PopData();
						Push(new Number(left.AsFloat() - right.AsFloat()));
						break;
					}
					case Operation.NumMul: {
						Data left = PopData(), right = PopData();
						Push(new Number(left.AsFloat() * right.AsFloat()));
						break;
					}
					case Operation.NumDiv: {
						Data left = PopData(), right = PopData();
						Push(new Number(left.AsFloat() / right.AsFloat()));
						break;
					}
					case Operation.NumMod: {
						Data left = PopData(), right = PopData();
						Push(new Number(left.AsFloat() % right.AsFloat()));
						break;
					}
					case Operation.NumPostInc: {
						Data old = PopData();
						Ref lval = PopRef();
						lval.Write(this, new Number(old.AsFloat() + 1));
						Push(old);
						break;
					}
					case Operation.NumPostDec: {
						Data old = PopData();
						Ref lval = PopRef();
						lval.Write(this, new Number(old.AsFloat() - 1));
						Push(old);
						break;
					}
					case Operation.NumPreInc: {
						Data old = PopData();
						Ref lval = PopRef();
						old = new Number(old.AsFloat() + 1);
						lval.Write(this, old);
						Push(old);
						break;
					}
					case Operation.NumPreDec: {
						Data old = PopData();
						Ref lval = PopRef();
						old = new Number(old.AsFloat() - 1);
						lval.Write(this, old);
						Push(old);
						break;
					}
					case Operation.NumNeg: {
						Data left = PopData();
						Push(new Number(-left.AsFloat()));
						break;
					}
					case Operation.ToNum: {
						Data left = PopData();
						Push(new Number(left.AsFloat()));
						break;
					}

					case Operation.BitOr: {
						Data left = PopData(), right = PopData();
						Push(new Number(left.AsInt32() | right.AsInt32()));
						break;
					}
					case Operation.BitAnd: {
						Data left = PopData(), right = PopData();
						Push(new Number(left.AsInt32() & right.AsInt32()));
						break;
					}
					case Operation.BitXor: {
						Data left = PopData(), right = PopData();
						Push(new Number(left.AsInt32() ^ right.AsInt32()));
						break;
					}
					case Operation.BitNot: {
						Data left = PopData();
						Push(new Number(~left.AsInt32()));
						break;
					}
					case Operation.Shl: {
						Data left = PopData(), right = PopData();
						Push(new Number(left.AsInt32() << right.AsInt32()));
						break;
					}
					case Operation.Shr: {
						Data left = PopData(), right = PopData();
						Push(new Number(left.AsInt32() >> right.AsInt32()));
						break;
					}

					case Operation.LogNot: {
						Data left = PopData();
						Push(new Bool(!left.AsBool()));
						break;
					}

					case Operation.Var:{
						ActiveScope.AddVar((string)ins.param);
						break;
					}
					case Operation.Jmp:{
						activeFrame.ip = (int)ins.param;
						break;
				  	}
					case Operation.JnzPeek:{
						if (PeekData().AsBool()) {
							activeFrame.ip = (int)ins.param;
						}
						break;
				  	}
					case Operation.JzPeek:{
						if (!PeekData().AsBool()) {
							activeFrame.ip = (int)ins.param;
						}
						break;
				  	}
					case Operation.Jz:{
						if (!PopData().AsBool()) {
							activeFrame.ip = (int)ins.param;
						}
						break;
				  	}
					case Operation.Jnz:{
						if (PopData().AsBool()) {
							activeFrame.ip = (int)ins.param;
						}
						break;
				  	}
					case Operation.PushData: {
						Push((Data)ins.param);
						break;
					}
					case Operation.PushFunction: {
						Push(new Function((FunctionDefinition)ins.param, ActiveScope));
						break;
					}
					case Operation.Pop: {
						PopRef();
						break;
					}
					case Operation.Dup: {
						for (int i = 0; i < (int)ins.param; ++i) {
							Push(Peek());
						}
						break;
					}
					case Operation.Swap: {
						Swap();
						break;
					}
					case Operation.TypeOf: {
						Push((Data)PopData().TypeOf);
						break;
					}
					case Operation.Call:
					case Operation.CtorCall:{
						int argc = (int)ins.param;
						Ref callee = PopRef();
						Data[] argv = new Data[argc];
						for (int i = argc - 1; i >= 0; --i) {
							argv[i] = PopData();
						}
						if (ins.operation == Operation.Call) {
							callee.Call(this, argv);
						} else {
							callee.CtorCall(this, argv);
						}
						break;
					}
					case Operation.ScopeLookup: {
						ActiveScope.Lookup(this, (string)ins.param);
						break;
					}
					case Operation.ScopeLLookup: {
						ActiveScope.LLookup(this, (string)ins.param);
						break;
					}
					case Operation.LookupName: {
						PopRef().LookupField(this, (string)ins.param);
						break;
					}
					case Operation.LLookupName: {
						PopRef().LLookupField(this, (string)ins.param);
						break;
					}
					case Operation.Lookup: {
						Ref owner = PopRef();
						Data index = PopData();
						owner.LookupField(this, index.AsString());
						break;
					}
					case Operation.LLookup: {
						Ref owner = PopRef();
						Data index = PopData();
						owner.LLookupField(this, index.AsString());
						break;
					}
					case Operation.Try: {
						Enter((Code)ins.param, activeFrame.scope.CreateChildScope(), Frame.Type.Try);
						break;
					}
					case Operation.Catch: {
						Enter((Code)ins.param, activeFrame.scope.CreateChildScope(), Frame.Type.Catch);
						break;
					}
					case Operation.Finally: {
						Enter((Code)ins.param, activeFrame.scope.CreateChildScope(), Frame.Type.Finally);
						break;
					}
					case Operation.Throw: {
						hasException = true;
						isTryCatchInterrupted = false;
						PropagateResult();
						break;
					}
					case Operation.Ret: {
						if (activeFrame.type == Frame.Type.Finally || activeFrame.type == Frame.Type.Try || activeFrame.type == Frame.Type.Catch) {
							hasException = false;
							isTryCatchInterrupted = true;
						}
						Ref result = PopRef();
						Leave();
						Push(result);
						break;
					}
					case Operation.ForwardRet: {
						PropagateResult();
						break;
					}
					case Operation.Leave: {
						Leave();
						break;
					}
					case Operation.Yield: {
						Data val = PopData();
						Leave();
						Object result = new Object();
						result.SetPublicProperty("value", val);
						result.SetPublicProperty("done", new Bool(false));
						Push(result);
						break;
					}
					case Operation.YieldBreak: {
						Leave();
						Object result = new Object();
						result.SetPublicProperty("value", Data.Undefined);
						result.SetPublicProperty("done", new Bool(true));
						Push(result);
						break;
					}
					case Operation.With: {
						Data scopeChild = PopData();
						int ip = activeFrame.ip;
						activeFrame.ip = (int)ins.param;
						Frame f = Enter(activeFrame.code, activeFrame.scope.CreateChildScope());
						f.ip = ip;
						f.scope.SetActivationObject(scopeChild);
						break;
					}
					case Operation.Store: {
						Data right = PopData();
						Ref left = PopRef();
						Push(right);
						left.Write(this, right);
						break;
					}
					case Operation.Del: {
						Ref left = PopRef();
						left.Delete();
						break;
					}
					default: throw new RuntimeError("NotImplemented operation: " + ins.operation);
				}
			}

			if (hasException) {
				throw new RuntimeError("Uncaught exception: " + PopData().AsString());
			}
		}

		internal Frame Enter(Code code, Scope scope, Frame.Type type = Frame.Type.Regular) {
#if JS_VERBOSE
			JS.VerboseLog("Entering frame " + (frameStack.Count) + " of type " + type);
#endif
			Frame frame = new Frame() { ip = 0, code = code, scope = scope, type = type };
			frameStack.Push(frame);
			activeFrame = frameStack.UnsafeTop();
			activeStack = activeFrame.valueStack;
			return frame;
		}

		internal void Resume(Frame frame) {
#if JS_VERBOSE
			JS.VerboseLog("Resuming frame " + (frameStack.Count));
#endif
			frameStack.Push(frame);
			activeFrame = frameStack.UnsafeTop();
			activeStack = activeFrame.valueStack;
		}

		internal void Leave() {
			frameStack.UnsafePop();
			activeFrame = frameStack.UnsafeTop();
			activeStack = activeFrame.valueStack;
#if JS_VERBOSE
			JS.VerboseLog("Leaving frame " + (frameStack.Count));
#endif
		}

		internal void PropagateResult() {
			Ref forwardedResult = null;
			if (activeStack.Count > 0) {
				forwardedResult = PopRef();
			}
			Leave();
			if (forwardedResult != null) {
				Push(forwardedResult);
			}
		}

		internal Scope ActiveScope {
			get { return activeFrame.scope; }
		}
		
		internal void Push(Data d) {
#if JS_VERBOSE
			JS.VerboseLog("Pushing " + d.DebugString);
#endif
			activeStack.Push(new Value(ActiveScope, d));
		}
		
		internal void Push(Ref r) {
#if JS_VERBOSE
			JS.VerboseLog("Pushing ref " + r.data.DebugString);
#endif
			activeStack.Push(r);
		}

		internal Data PopData() {
			Ref last = activeStack.UnsafeTop();
			activeStack.UnsafePop();
#if JS_VERBOSE
			JS.VerboseLog("Popping " + last.data.DebugString);
#endif
			return last.data;
		}

		internal Ref PopRef() {
			Ref last = activeStack.UnsafeTop();
			activeStack.UnsafePop();
#if JS_VERBOSE
			JS.VerboseLog("Popping ref " + last.data.DebugString);
#endif
			return last;
		}

		internal Ref Peek() {
			return activeStack.UnsafeTop();
		}

		internal Data PeekData() {
			return activeStack.UnsafeTop().data;
		}

		internal void Swap() {
			Ref a = PopRef();
			Ref b = PopRef();
			Push(a);
			Push(b);
		}

		internal Data PopResult() {
			if (activeStack.Count > 0) {
				return PopData();
			} else {
				return Data.Undefined;
			}
		}

		internal static Machine Current;

		internal static Data CallFunction(Function function, Data thisObject, Data[] argv) {
			Machine machine = new Machine();
			function.Call(machine, thisObject, argv);
			machine.Run();
			return machine.PopResult();
		}

	}

	internal class Ref {
		internal Data data;

		internal void LookupField(Machine machine, string name) {
			Property prop = data.GetProperty(name);
			if (prop.getter != null) {
				prop.getter.Call(machine, data, Data.EmptyArgs);
			} else {
				machine.Push(new PropertyRef() { owner= data, data= prop.data });
			}
		}

		internal void LLookupField(Machine machine, string name) {
			Property prop = data.GetProperty(name);
			if (prop.IsUndefined) {
				data.SetPublicProperty(name, new Undefined(name));
				prop = data.GetProperty(name);
			}

			if (prop.setter != null) {
				machine.Push(new SetterPropertyRef() { owner = data, data = prop.setter }); 
			} else {
				machine.Push(new NamedPropertyRef() { owner = data, data = prop.data, name = name });
			}
		}

		internal virtual void CtorCall(Machine machine, Data[] argv) {
			Data newObject = new Object();
			newObject.SetProperty("constructor", data);
			if (data is NativeClass) { // Native ctor does return value
				data.Call(machine, newObject, argv);
			} else {
				newObject.proto = ((Function)data).Prototype;
				machine.Push(newObject);
				machine.Enter(Code.Leave, null); // discarding frame
				data.Call(machine, newObject, argv);
			}
		}

		internal virtual void Delete() {}
		internal virtual void Write(Machine machine, Data val) {}
		internal virtual void Call(Machine machine, Data[] argv) {}
	}

	internal class Value : Ref {
		internal Scope scope;

		internal Value(Scope scope, Data data) {
			this.scope = scope;
			this.data = data;
		}

		internal override void Call(Machine machine, Data[] argv) {
			data.Call(machine, scope.activationObject, argv);
		}
	}

	internal class PropertyRef : Ref {
		internal Data owner;

		internal PropertyRef() {
		}

		internal override void Call(Machine machine, Data[] args) {
			data.Call(machine, owner, args);
		}
	}

	internal class NamedPropertyRef : PropertyRef {
		internal string name;

		internal NamedPropertyRef() : base() {
		}

		internal override void Write(Machine machine, Data val) {
#if JS_VERBOSE
			JS.VerboseLog("Setting prop " + name + " of @" + owner.uniqueId + " to " + val.DebugString);
#endif
			owner.SetPublicProperty(name, val);
			data = val;
		}

		internal override void Delete() {
			owner.DeleteProperty(name);
		}
	}

	internal class SetterPropertyRef : PropertyRef {
		internal SetterPropertyRef() : base() {}

		internal override void Write(Machine machine, Data val) {
#if JS_VERBOSE
			JS.VerboseLog("Calling setter @" + data.uniqueId + " of @" + owner.uniqueId + " with " + val.DebugString);
#endif
			machine.Enter(Code.Leave, null); // discarding frame
			data.Call(machine, owner, new Data[] { val });
		}
	}


	internal class Scope {
		internal Ref activationObjectRef;
		internal Data activationObject;
		internal Data thisObject;
		internal Scope parent;

		internal Scope() {
			SetActivationObject(new Object());
		}

		internal Scope CreateChildScope(Data thisObject) {
			Scope childScope = new Scope();
			childScope.thisObject = thisObject;
			childScope.parent = this;
			childScope.activationObject.SetProperty("this", childScope.thisObject);
			return childScope;
		}

		internal Scope CreateChildScope() {
			return CreateChildScope(thisObject);
		}

		internal void SetActivationObject(Data d) {
			activationObject = d;
			activationObjectRef = new Value(null, activationObject);
		}

		internal void SetLocals(List<string> locals) {
			foreach (string name in locals) {
				activationObject.SetProperty(name, new Undefined(name));
			}
		}

		internal void AddVar(string name) {
			activationObject.SetProperty(name, new Undefined(name));
		}

		internal void SetFunctionArguments(FunctionDefinition function, Data[] argv) {
			for (int i=0; i < function.arguments.Count; ++i) {
				if (i < argv.Length) {
					activationObject.SetProperty(function.arguments[i], argv[i]);
				} else {
					activationObject.SetProperty(function.arguments[i], new Undefined(function.arguments[i]));
				}
			}
			//TODO: arguments object should not be an Array
			activationObject.SetProperty("arguments", new Arguments(argv));
			SetLocals(function.locals);
		}

		Scope FindParentScope(string name) {
			Scope scope = this;
			while (scope != null) {
				if (scope.activationObject.HasProperty(name)) {
					return scope;
				} else {
					scope = scope.parent;
				}
			}
			return null;
		}

		internal void Lookup(Machine machine, string name) {
			Scope parentScope = FindParentScope(name);
			if (parentScope != null) {
				parentScope.activationObjectRef.LookupField(machine, name);
			} else {
				machine.Push(new Undefined(name));
#if JS_VERBOSE
				JS.VerboseLog("Prop " + name + " not found in scope");
#endif
			}
		}

		internal void LLookup(Machine machine, string name) {
			Scope parentScope = FindParentScope(name);
			if (parentScope != null) {
				parentScope.activationObjectRef.LLookupField(machine, name);
			} else {
				machine.frameStack.buffer[1].scope.activationObjectRef.LLookupField(machine, name);
			}
		}
	}

	internal class Property {
		[System.FlagsAttribute]
		internal enum Flags : int {
			None = 0,
			Writtable = 1,
			Enumerable = 2
		}

		internal static Property Null = new Property() { data = Data.Null };

		internal Flags flags;
		internal Data getter;
		internal Data setter;
		internal Data data;

		internal bool IsEnumerable { get { return (flags & Flags.Enumerable) != 0; } }
		internal bool IsUndefined { get { return data != null && data.IsUndefined; } }

		internal Data Read(Data owner = null) {
			if (getter != null) {
				//TODO: native getters
				return Machine.CallFunction((Function)getter, owner, Data.EmptyArgs);
			} else {
				return data;
			}
		}

		internal static Property Undefined(string name) {
			return new Property() { data = new Undefined(name) };
		}

	}

	public class Data {

		internal static Data Undefined = new Undefined();
		internal static Data Null = new Null();
		internal static Data EndOfIterator = new EndOfIterator();
		internal static Data[] EmptyArgs = new Data[] { };
		internal static int UniqueIdGen = 0;
		internal static BindingFlags InternalBinding = BindingFlags.Instance | BindingFlags.NonPublic;

		internal int uniqueId = UniqueIdGen++;

		internal Data proto;
		internal Dictionary<string,Property> hash = null;

		internal Data() {
		}

		internal void SetProperty(string name, Data data) {
			SetProperty(name, data, Property.Flags.None);
		}

		internal void SetPublicProperty(string name, Data data) {
			SetProperty(name, data, Property.Flags.Writtable | Property.Flags.Enumerable);
		}

		internal void SetIndex(int index, Data o) {
			SetProperty(index.ToString(), o);
		}

		internal virtual void SetProperty(string name, Data o, Property.Flags flags) {
			throw new RuntimeError("Cannot set property " + name + " of " + this);
		}

		internal Property GetIndex(int index) {
			return GetProperty(index.ToString());
		}

		internal virtual Property GetProperty(string name) {
			throw new RuntimeError("Cannot get property " + name + " of " + this);
		}

		internal virtual bool DeleteProperty(string name) {
			return true;
		}

		internal virtual bool HasOwnProperty(string name) {
			return false;
		}

		internal virtual void Call(Machine machine, Data thisObject, Data[] arguments) {
			throw new RuntimeError("Not callable: " + ToString() + " of type " + GetType() + " on " + thisObject);
		}

		internal virtual IEnumerable<Data> IterableKeys {
			get { yield break; }
		}

		internal Iterator GetIterableKeysIterator() {
			return new Iterator(IterableKeys.GetEnumerator());
		}

		internal virtual string TypeOf { get { return "none"; } }

		internal virtual bool StrictlyEquals(Data rhs) {
			throw new TypeError("NotImplemented for " + this);
		}

		internal virtual bool IsNullValue { get { return false; } }

		internal virtual bool AsBool() {
			return true;
		}

		internal virtual double AsFloat() {
			double f;
			string str = AsString();
			if (str == "") {
				return 0;
			} else {
				if (double.TryParse(str, NumberStyles.Any, System.Globalization.CultureInfo.InvariantCulture, out f)) {
					return f;
				} else {
					return double.NaN;
				}
			}
		}

		internal virtual string AsString() {
			throw new TypeError("NotImplemented for " + this.GetType());
		}

		internal void CreateHash() {
			if (hash == null) {
				hash = new Dictionary<string, Property>();
			}
		}

		// XXX: JS requires to have NaN casted to 0, not it's binary representation.
		internal int AsInt32() {
			double number = AsFloat();
			if (double.IsNaN(number) || double.IsInfinity(number)) {
				return 0;
			} else {
				return (int)number;
			}
		}

		// Note: I don't care about setter property here, because it's being handled by LPropertyRef
		internal void SetHashProperty(string name, Data data, Property.Flags flags) {
			Property prop;
			if (hash != null && hash.TryGetValue(name, out prop)) {
				prop.flags = flags;
				prop.data = data;
			} else {
				CreateHash();
				prop = new Property() {};
				prop.data = data;
				prop.flags = flags;
				hash[name] = prop;
			}
		}

		internal bool HasProperty(string name) {
			if (HasOwnProperty(name)) {
				return true;
			} else if (proto != null) {
				return proto.HasProperty(name);
			} 
			return false;
		}

		internal Property GetHashProperty(string name) {
			Property prop;
			if (hash != null && hash.TryGetValue(name, out prop)) {
				return prop;
			} else if (proto != null) {
#if JS_VERBOSE
				JS.VerboseLog("Looking for " + name + " in proto @" + proto.uniqueId + " of @" + uniqueId);
#endif
				return proto.GetProperty(name);
			} else {
				return Property.Undefined(name);
			}
		}

		internal bool DeleteHashProperty(string name) {
			if (hash != null && hash.ContainsKey(name)) {
				hash.Remove(name);
			}
			return true;
		}

		internal IEnumerable<Data> IterableHashKeys {
			get {
				if (hash != null) {
					foreach (KeyValuePair<string,Property> item in hash) {
						if (item.Value.IsEnumerable) {
							yield return item.Key;
						}
					}
				}
			}
		}

		internal void DefineGetter(string name, Data getter) {
			CreateHash();
			Property prop = hash.ContainsKey(name) ? hash[name] : (hash[name] = new Property());
			prop.getter = getter;
		}

		internal void DefineSetter(string name, Data setter) {
			CreateHash();
			Property prop = hash.ContainsKey(name) ? hash[name] : (hash[name] = new Property());
			prop.setter = setter;
		}

		internal Data ReadProperty(string name) {
			Property prop = GetProperty(name);
			return prop.Read(this);
		}
		
		internal Data CallMethod(string name, params Data[] args) {
			Machine machine = new Machine();
			ReadProperty(name).Call(machine, this, args);
			machine.Run();
			return machine.PopResult();
		}

		internal int StringToIndex(string name) {
			int v = 0, d = 0;
			for (int i = 0; i < name.Length; ++i ) {
				d = name[i] - '0';
				if (d >= 0 && d <= 9) {
					v = v * 10 + d;
				} else {
					return -1;
				}
			}
			return v;
		}

		internal virtual string DebugString { get { return ""; } }

		static void StaticCall(Machine machine, Data thisObject, Data[] argv) {
			Data[] fargv = new Data[argv.Length - 1];
			for (int i = 1; i < argv.Length; ++i) {
				fargv[i - 1] = argv[i];
			}
			thisObject.Call(machine, argv[0], fargv);
		}

		internal static void StaticApply(Machine machine, Data thisObject, Data[] argv) {
			if (argv[1] is Array) {
				Array a = (Array)argv[1];
				Data[] fargv = new Data[a.array.Count];
				for (int i = 0; i < a.array.Count; ++i) {
					fargv[i] = a.ReadProperty(i.ToString());
				}
				thisObject.Call(machine, argv[0], fargv);
			} else {
				Arguments a = (Arguments)argv[1];
				thisObject.Call(machine, argv[0], a.array);
			}
		}

		internal static bool DataEquals(Data left, Data right) {
			bool result = true;
			if (left.GetType() == right.GetType()) {
				result = left.StrictlyEquals(right);
			} else {
				if ((left.IsNullValue && right.IsNullValue)) {
					result = true;
				} else if (left.IsNullValue || right.IsNullValue) {
					result = false;
				} else if (left.IsNumber || left.IsBool || right.IsNumber || right.IsBool) {
					result = left.AsFloat() == right.AsFloat();
				} else {
					result = left.AsString() == right.AsString();
				}
			}
			return result;
		}

		// Public stuff

		public static Data Of(object o, bool useRawValue = false) {
			if (!useRawValue && o is System.Type) {
				System.Type t = (System.Type)o;
				if (t.IsEnum) {
					return new NativeEnum((System.Type)o);
				} else {
					return new NativeClass((System.Type)o);
				}
			} else {
				if (o is Data) {
					return (Data)o;
				} else {
					return Serialization.SerializeFlat(o);
				}
			}
		}

		public static Data NewObject() {
			return new Object();
		}

		public Data this[int index] {
			get {
				return this[index.ToString()];
			} 
		}

		public Data this[string key] {
			get {
				Property prop = GetProperty(key);
				if (!prop.IsUndefined) {
					return ReadProperty(key);
				} else {
					Object o = new Object();
					SetPublicProperty(key, o);
					return o;
				}
			}
			set {
				SetPublicProperty(key, value);
			}
		}

		public override string ToString() {
			return JSON.stringify(this);
		}

		public int Count {
			get {
				if (this is Array) {
					return ((Array)this).GetLength();
				} else {
					return 0;
				}
			}
		}

		public virtual IEnumerable<Data> GetGenerator() {
			yield break;
		}

		public bool IsPrimitive {
			get {
				return this is Number || this is String || this is Bool || this is Null || this is Undefined;
			}
		}

		public bool IsNumber { get { return this is Number; } }
		public bool IsString { get { return this is String; } }
		public bool IsBool { get { return this is Bool; } }
		public bool IsArray { get { return this is Array; } }
		public bool IsNative { get { return this is Native; } }
		public bool IsObject { get { return this is Object; } }
		public bool IsFunction { get { return this is Function; } }
		public bool IsGenerator { get { return this is Generator; } }
		public bool IsIterator { get { return this is Iterator; } }
		public bool IsUndefined { get { return this is Undefined; } }
		public bool IsNull { get { return this == Data.Null; } }

		public static explicit operator int(Data v) {
			return v.AsInt32();
		}
		public static explicit operator string(Data v) {
			return v.AsString();
		}
		public static explicit operator long(Data v) {
			return (long)v.AsFloat();
		}
		public static explicit operator float(Data v) {
			return (float)v.AsFloat();
		}
		public static explicit operator double(Data v) {
			return v.AsFloat();
		}
		public static explicit operator bool(Data v) {
			return v.AsBool();
		}

		public static implicit operator Data(int v) {
			return new Number(v);
		}
		public static implicit operator Data(float v) {
			return new Number(v);
		}
		public static implicit operator Data(bool v) {
			return new Bool(v);
		}
		public static implicit operator Data(string v) {
			return new String(v);
		}

		public virtual Data Apply(Data thisObject, Data[] argv) {
			throw new RuntimeError("Not callable: " + GetType());
		}

		public Data Construct(Data[] argv) {
			Object thisObject = new Object();
			thisObject.proto = ((Function)this).Prototype;
			Machine.CallFunction((Function)this, thisObject, argv);
			return thisObject;
		}
	}

	internal class Object : Data {
		internal static Data ObjectConstructor;
		static Object ObjectProto;

		static Object() {
			ObjectConstructor = new NativeClass(typeof(Object));
			ObjectProto = new Object();

			ObjectConstructor.SetProperty("prototype", ObjectProto, Property.Flags.Writtable);
			ObjectConstructor.SetProperty("create", new NativeFunction(Util.GetStaticMethod(typeof(Object), "Create")));

			ObjectProto.SetProperty("toString", new NativeFunction(Util.GetStaticMethod(typeof(Object), "ToString")));
			ObjectProto.SetProperty("hasOwnProperty", new NativeMethod(Util.GetMethods(typeof(Object), "HasOwnProperty")));
			ObjectProto.SetProperty("__defineGetter__", new NativeMethod(Util.GetMethods(typeof(Data), "DefineGetter")));
			ObjectProto.SetProperty("__defineSetter__", new NativeMethod(Util.GetMethods(typeof(Data), "DefineSetter")));
			ObjectProto.SetProperty("__iterableKeys__", new NativeMethod(Util.GetMethods(typeof(Data), "GetIterableKeysIterator")));
		}

		static void ToString(Machine machine, Data thisObject, Data[] argv) {
			machine.Push(new String("[object " + thisObject.GetType().Name + "]"));
		}

		static void Create(Machine machine, Data thisObject, Data[] argv) {
			Object o = new Object();
			o.proto = argv[0];
			//TODO: define oroperties given within argv[1]
			machine.Push(o);
		}

		internal Object() : base() {
			proto = ObjectProto;
			//XXX: we do not want to have hash in every data subtype just only for the built-in/native add-ons
			SetProperty("constructor", Object.ObjectConstructor, Property.Flags.Writtable);
		}

		internal override void SetProperty(string name, Data o, Property.Flags flags) {
			SetHashProperty(name, o, flags);
		}

		internal override Property GetProperty(string name) {
			return GetHashProperty(name);
		}

		internal override bool DeleteProperty(string name) {
			return DeleteHashProperty(name);
		}

		internal override bool HasOwnProperty(string name) {
			return hash != null && hash.ContainsKey(name);
		}

		internal override IEnumerable<Data> IterableKeys {
			get {
				foreach (Data item in IterableHashKeys ) {
					yield return item;
				}
				if (proto != null) {
					foreach (Data item in proto.IterableKeys) {
						yield return item;
					}
				}
			}
		}

		internal override bool StrictlyEquals(Data rhs) {
			return this == rhs;
		}

		internal override string TypeOf { get { return "object"; } }

		internal override string AsString() {
			return (string)CallMethod("toString");
		}

		internal override string DebugString { get { return "@" + uniqueId + ": Object"; } }
	}

	// This subtype of Data is used only internally within the for-in loop, so it doesn't need to be complete
	internal class Iterator : Data {
		static Object IteratorProto;

		internal IEnumerator<Data> enumerator;
		internal bool hasFinished;

		static Iterator() {
			IteratorProto = new Object();

			IteratorProto.SetProperty("next", new NativeFunction(Util.GetStaticMethod(typeof(Iterator), "Next")));
		}

		internal Iterator(IEnumerator<Data> enumerator) : base() {
			this.proto = IteratorProto;
			this.enumerator = enumerator;
			hasFinished = !enumerator.MoveNext();
		}

		internal override Property GetProperty(string name) {
			return GetHashProperty(name);
		}

		internal static void Next(Machine machine, Data thisObject, Data[] argv) {
			Iterator it = (Iterator)thisObject;
			if (it.hasFinished) {
				machine.Push(Data.EndOfIterator);
			} else {
				machine.Push(it.enumerator.Current);
				it.hasFinished = !it.enumerator.MoveNext();
			}
		}
	}

	internal class Generator : Data {
		static Object GeneratorProto;

		internal FunctionDefinition definition; 
		internal Scope scope;
		internal Frame frame;

		static Generator() {
			GeneratorProto = new Object();

			GeneratorProto.SetPublicProperty("next", new NativeFunction(Util.GetStaticMethod(typeof(Generator), "Next")));
			GeneratorProto.SetPublicProperty("send", new NativeFunction(Util.GetStaticMethod(typeof(Generator), "Send")));
			GeneratorProto.SetPublicProperty("iterator", new NativeFunction(Util.GetStaticMethod(typeof(Generator), "GetIterator")));
		}

		internal Generator(FunctionDefinition definition, Scope scope) : base() {
			this.proto = GeneratorProto;
			this.definition = definition;
			this.scope = scope;
		}

		internal override void SetProperty(string name, Data o, Property.Flags flags) {
			SetHashProperty(name, o, flags);
		}

		internal override Property GetProperty(string name) {
			return GetHashProperty(name);
		}

		internal override bool DeleteProperty(string name) {
			return DeleteHashProperty(name);
		}

		internal override bool HasOwnProperty(string name) {
			return hash != null && hash.ContainsKey(name);
		}

		internal override bool StrictlyEquals(Data rhs) {
			return this == rhs;
		}

		internal override string TypeOf { get { return "object"; } }

		internal override string AsString() {
			return "[object Generator]";
		}

		internal static void Next(Machine machine, Data thisObject, Data[] argv) {
			Generator.Send(machine, thisObject, new Data[] { Data.Undefined });
		}

		internal static void Send(Machine machine, Data thisObject, Data[] argv) {
			Generator g = thisObject as Generator;
			if (g != null) {
				if (g.frame == null) {
					if (!argv[0].IsUndefined) {
					} else {
						g.frame = machine.Enter(g.definition.code, g.scope);
						StackTrace.RecordCall(machine.currentInstruction);
					}
				} else {
					machine.Resume(g.frame);
					machine.Push(argv[0]);
				}
			} else {
				throw new TypeError("this is not generator");
			}
		}

		internal static void GetIterator(Machine machine, Data thisObject, Data[] argv) {
			machine.Push(thisObject);
		}

		internal override string DebugString { get { return "@" + uniqueId + ": Generator"; } }

		public override IEnumerable<Data> GetGenerator() {
			Machine machine = new Machine();
			for (;;) {
				Generator.Next(machine, this, new Data[] { Data.Undefined });
				machine.Run();
				Data item = machine.PopResult();
				if (item.GetProperty("done").data.AsString() == "true") {
					break;
				} else {
					yield return item.GetProperty("value").data;
				}
			}
		}

	}

	internal class Arguments : Data {
		internal static Data ArgumentsConstructor;
		internal static Object ArgumentsProto;

		static Arguments() {
			ArgumentsConstructor = new NativeClass(typeof(Arguments));
			ArgumentsProto = new Object();

			ArgumentsConstructor.SetProperty("prototype", ArgumentsProto, Property.Flags.Writtable);

			ArgumentsProto.SetProperty("constructor", ArgumentsConstructor, Property.Flags.None);
			ArgumentsProto.DefineGetter("length", new NativeMethod(Util.GetMethods(typeof(Arguments), "GetLength")));
			ArgumentsProto.SetProperty("toString", new NativeMethod(Util.GetMethods(typeof(Arguments), "AsString")));
		}

		internal Data[] array;

		internal Arguments(Data[] array) : base() {
			this.array = array;
			proto = ArgumentsProto;
		}

		internal int GetLength() {
			return array.Length;
		}

		internal override void SetProperty(string name, Data o,  Property.Flags flags) {
			SetHashProperty(name, o, flags);
		}

		internal override Property GetProperty(string name) {
			int index = StringToIndex(name);
			if (index >= 0 && index < array.Length) {
				return new Property() { data = array[index] };
			} else {
				return GetHashProperty(name);
			}
		}

		internal override bool DeleteProperty(string name) {
			return DeleteHashProperty(name);
		}

		internal override bool HasOwnProperty(string name) {
			if (hash != null && hash.ContainsKey(name)) {
				return true;
			} else {
				int index = StringToIndex(name);
				return index >= 0 && index < array.Length && !array[index].IsUndefined;
			}
		}

		internal override IEnumerable<Data> IterableKeys {
			get {
				for (int i = 0; i < array.Length; ++i ){
					yield return i;
				}
				foreach (Data item in IterableHashKeys) {
					yield return item;
				}
			}
		}

		internal override bool StrictlyEquals(Data rhs) {
			return rhs is Arguments && array == ((Arguments)rhs).array;
		}
		
		internal override string TypeOf { get { return "object"; } }

		internal override string AsString() {
			string result = "";
			for (var i = 0; i < array.Length; ++i) {
				if (i != 0) {
					result += ",";
				}

				if (!array[i].IsUndefined && array[i] != Data.Null) {
					result += array[i].AsString();
				}
			}
			return result;
		}

		internal override string DebugString { get { return "@" + uniqueId + ": Array"; } }

	}

	internal class Array : Data {
		internal static Data ArrayConstructor;
		internal static Object ArrayProto;

		static Array() {
			ArrayConstructor = new NativeClass(typeof(Array));
			ArrayProto = new Object();

			ArrayConstructor.SetProperty("prototype", ArrayProto, Property.Flags.Writtable);

			ArrayProto.SetProperty("constructor", ArrayConstructor, Property.Flags.None);
			ArrayProto.DefineGetter("length", new NativeMethod(Util.GetMethods(typeof(Array), "GetLength")));
			ArrayProto.DefineSetter("length", new NativeMethod(Util.GetMethods(typeof(Array), "SetLength")));
			ArrayProto.SetProperty("toString", new NativeMethod(Util.GetMethods(typeof(Array), "AsString")));

			ArrayProto.SetProperty("push", new NativeFunction(Util.GetStaticMethod(typeof(Array), "Push")), Property.Flags.None);
			ArrayProto.SetProperty("concat", new NativeFunction(Util.GetStaticMethod(typeof(Array), "Concat")), Property.Flags.None);
			ArrayProto.SetProperty("splice", new NativeFunction(Util.GetStaticMethod(typeof(Array), "Splice")), Property.Flags.None);
			ArrayProto.SetProperty("slice", new NativeFunction(Util.GetStaticMethod(typeof(Array), "Slice")), Property.Flags.None);
			ArrayProto.SetProperty("shift", new NativeFunction(Util.GetStaticMethod(typeof(Array), "Shift")), Property.Flags.None);
			ArrayProto.SetProperty("join", new NativeFunction(Util.GetStaticMethod(typeof(Array), "Join")), Property.Flags.None);
			ArrayProto.SetProperty("indexOf", new NativeFunction(Util.GetStaticMethod(typeof(Array), "IndexOf")), Property.Flags.None);
			ArrayProto.SetProperty("lastIndexOf", new NativeFunction(Util.GetStaticMethod(typeof(Array), "LastIndexOf")), Property.Flags.None);
			ArrayProto.SetProperty("reverse", new NativeFunction(Util.GetStaticMethod(typeof(Array), "Reverse")), Property.Flags.None);

			ArrayProto.SetProperty("map", new Function(
				@"function(callback, context) {
					var result = [];
					if (context !== undefined) {
						for (var i = 0; i < this.length; ++i) {
							result.push(callback.call(context, this[i], i, this));
						} 
					} else {
						for (var i = 0; i < this.length; ++i) {
							result.push(callback(this[i], i, this));
						} 
					}
					return result;
				}"));

			ArrayProto.SetProperty("filter", new Function(
				@"function(callback, context) {
					var result = [];
					if (context !== undefined) {
						for (var i = 0; i < this.length; ++i) {
							if (callback.call(context, this[i], i, this)) {
								result.push(this[i]);
							}
						} 
					} else {
						for (var i = 0; i < this.length; ++i) {
							if (callback(this[i], i, this)) {
								result.push(this[i]);
							}
						} 
					}
					return result;
				}"));
		}

		internal List<Property> array;

		internal Array(Data[] array) : this() {
			for (int i = 0; i < array.Length; ++i) {
				this.array.Add(new Property() { data = array[i] });
			}
		}

		internal Array(List<Property> array) : this() {
			this.array = array;
		}

		internal Array() {
			array = new List<Property>();
			proto = ArrayProto;
		}

		internal int GetLength() {
			return array.Count;
		}

		internal int SetLength(int length) {
			if (length > array.Count) {
				ExtendArray(length - 1);
			} else if (length < array.Count) {
				array.RemoveRange(length, array.Count - length);
			}
			return length;
		}

		internal static void Splice(Machine machine, Data thisObject, Data[] argv) {
			Array self = (Array)thisObject;
			int index = argv[0].AsInt32();
			int count = int.MaxValue;
			if (argv.Length > 1) {
				count = argv[1].AsInt32();
			}

			if (index < 0) {
				index = self.array.Count - 1 + index;
				if (index < 0) {
					index = 0;
				}
			} else if (index >= self.array.Count) {
				index = self.array.Count - 1;
			}
			if (count > self.array.Count - index) {
				count = self.array.Count - index;
			}
			List<Property> removedElements = self.array.GetRange(index, count);
			self.array.RemoveRange(index, count);

			if (argv.Length > 2) {
				List<Property> newElements = new List<Property>();
				for (int i = 2; i < argv.Length; ++i) {
					newElements.Add(new Property() { data = argv[i] });
				}
				self.array.InsertRange(index, newElements);
			}
			machine.Push(new Array(removedElements));

		}

		internal static void Slice(Machine machine, Data thisObject, Data[] argv) {
			Array self = (Array)thisObject;
			int start = argv[0].AsInt32();
			int end = int.MaxValue;
			if (argv.Length > 1) {
				end = argv[1].AsInt32();
			}

			if (start < 0) {
				start = self.array.Count + start;
				if (start < 0) {
					start = 0;
				}
			}
			if (start > self.array.Count) {
				start = self.array.Count;
			}
			if (end < 0) {
				end = self.array.Count + end;
				if (end < 0) {
					end = 0;
				}
			}
			if (end > self.array.Count) {
				end = self.array.Count;
			}
			if (end < start) {
				end = start;
			}
			List<Property> slicedElements = self.array.GetRange(start, end - start);
			machine.Push(new Array(slicedElements));

		}

		internal static void Join(Machine machine, Data thisObject, Data[] argv) {
			Array self = (Array)thisObject;
			StringBuilder sb = new StringBuilder();
			string separator = "";
			if (argv.Length > 0) {
				separator = argv[0].AsString();
			}
			for (int i = 0; i < self.array.Count; ++i) {
				if (i > 0) {
					sb.Append(separator);
				}
				sb.Append(self.array[i].Read(self).AsString());
			}
			machine.Push(new String(sb.ToString()));
		}

		internal static void IndexOf(Machine machine, Data thisObject, Data[] argv) {
			Native native = thisObject as Native;
			if (native != null) { // check, because Array.prototype.indexOf.call may occur
				if (native.native is IList) {
					Native.NativeListIndexOf(machine, thisObject, argv);
				} else if (native.native is System.Array) {
					Native.NativeArrayIndexOf(machine, thisObject, argv);
				}
			} else {
				Array self = (Array)thisObject;
				int s = 0;
				int r = -1;
				Data e = argv[0];
				if (argv.Length > 1) {
					s = argv[1].AsInt32();
				}

				for (int i = s; i < self.array.Count; ++i) {
					if (self.array[i].data.StrictlyEquals(e)) {
						r = i;
						break;
					}
				}
				machine.Push(new Number(r));
			}
		}

		internal static void LastIndexOf(Machine machine, Data thisObject, Data[] argv) {
			Array self = (Array)thisObject;
			int s = self.array.Count - 1;
			int r = -1;
			Data e = argv[0];
			if (argv.Length > 1) {
				s = argv[1].AsInt32();
			}

			for (int i = s; i >= 0; --i) {
				if (self.array[i].data.StrictlyEquals(e)) {
					r = i;
					break;
				}
			}
			machine.Push(new Number(r));
		}

		internal static void Reverse(Machine machine, Data thisObject, Data[] argv) {
			Array self = (Array)thisObject;
			self.array.Reverse();
			machine.Push(self);
		}


		internal static void Shift(Machine machine, Data thisObject, Data[] argv) {
			Array self = (Array)thisObject;
			Data item = self.array[0].data;
			self.array.RemoveAt(0);
			machine.Push(item);
		}

		internal static void Push(Machine machine, Data thisObject, Data[] argv) {
			Array self = (Array)thisObject;
			for (int i = 0; i < argv.Length; ++i) {
				self.array.Add(new Property() { data = argv[i] });
			}
			machine.Push(Data.Undefined);
		}

		internal static void Concat(Machine machine, Data thisObject, Data[] argv) {
			Array self = (Array)thisObject;
			List<Property> sum = new List<Property>();
			sum.AddRange(self.array);
			for (int i = 0; i < argv.Length; ++i) {
				try {
					Array a = (Array)argv[i];
					sum.AddRange(a.array);
				} catch (System.Exception e) {
					throw e;
				}
			}
			machine.Push(new Array(sum));
		}

		void ExtendArray(int maxIndex) {
			for (int i = array.Count; i <= maxIndex; ++i) {
				array.Add(Property.Undefined(maxIndex.ToString()));
			}
		}

		internal override void SetProperty(string name, Data o,  Property.Flags flags) {
			int index = StringToIndex(name);
			if (index >= 0) {
				if (index >= array.Count) {
					ExtendArray(index);
				}
				array[index] = new Property() { data = o };
			} else {
				SetHashProperty(name, o, flags);
			}
		}

		internal override Property GetProperty(string name) {
			int index = StringToIndex(name);
			if (index >= 0 && index < array.Count) {
				return array[index];
			} else {
				return GetHashProperty(name);
			}
		}

		internal override bool DeleteProperty(string name) {
			int index = StringToIndex(name);
			if (index >= 0 && index < array.Count) {
				array[index] = Property.Null;
				return true;
			} else {
				return DeleteHashProperty(name);
			}
		}

		internal override bool HasOwnProperty(string name) {
			if (hash != null && hash.ContainsKey(name)) {
				return true;
			} else {
				int index = StringToIndex(name);
				return index >= 0 && index < array.Count && !array[index].IsUndefined;
			}
		}

		internal override IEnumerable<Data> IterableKeys {
			get {
				for (int i = 0; i < array.Count; ++i ){
					yield return i;
				}
				foreach (Data item in IterableHashKeys) {
					yield return item;
				}
			}
		}

		internal override bool StrictlyEquals(Data rhs) {
			return rhs is Array && array == ((Array)rhs).array;
		}
		
		internal override string TypeOf { get { return "object"; } }

		internal override string AsString() {
			string result = "";
			for (var i = 0; i < array.Count; ++i) {
				if (i != 0) {
					result += ",";
				}

				if (!array[i].IsUndefined) {
					Data d = array[i].Read(this);
					if (!d.IsNull) {
						result += array[i].data.AsString();
					}
				}
			}
			return result;
		}

		public IList ConvertToNativeList(System.Type listType) {
			//TODO: Handle getters within array (maybe via ReadProperty)
			if (listType.IsGenericType) {
				System.Type param = listType.GetGenericArguments()[0];
				IList list = (IList)System.Activator.CreateInstance(listType);
				foreach (Property p in array) {
					list.Add(Serialization.Deserialize(param, p.data));
				}
				return list;
			} else {
				IList list = new List<Data>();
				foreach (Property p in array) {
					list.Add(p.data);
				}
				return list;
			}
		}

		internal override string DebugString { get { return "@" + uniqueId + ": Array"; } }
	}

	internal class Function : Data {
		static Object FunctionProto;

		static Function() {
			FunctionProto = new Object();

			FunctionProto.SetProperty("apply", new NativeFunction(Util.GetStaticMethod(typeof(Function), "Apply")));
			FunctionProto.SetProperty("call", new NativeFunction(Util.GetStaticMethod(typeof(Data), "StaticCall")));
		}

		internal FunctionDefinition definition;
		internal Scope scope;

		internal Function(FunctionDefinition definition, Scope scope) : base() {
			this.proto = FunctionProto;
			this.definition = definition;
			this.scope = scope;
			SetProperty("prototype", new Object(), Property.Flags.Writtable);
			SetProperty("length", new Number(definition.arguments.Count));
		}

		internal Function(string code) : this(Parser.ParseFunction(code), new Scope()) {
		}

		internal Data Prototype { get { return hash["prototype"].data; } }

		internal override void SetProperty(string name, Data o, Property.Flags flags) {
			SetHashProperty(name, o, flags);
		}

		internal override Property GetProperty(string name) {
			return GetHashProperty(name);
		}

		internal override bool DeleteProperty(string name) {
			return DeleteHashProperty(name);
		}

		internal override bool HasOwnProperty(string name) {
			return hash != null && hash.ContainsKey(name);
		}

		internal override void Call(Machine machine, Data thisObject, Data[] argv) {
			Scope callScope = scope.CreateChildScope(thisObject);
			callScope.SetFunctionArguments(definition, argv);

			if (definition.isGenerator) {
				machine.Push(new Generator(definition, callScope));
			} else {
				machine.Enter(definition.code, callScope);
				StackTrace.RecordCall(machine.currentInstruction);
			}
		}

		internal override IEnumerable<Data> IterableKeys {
			get {
				return IterableHashKeys;
			}
		}

		internal override bool StrictlyEquals(Data rhs) {
			return this == rhs;
		}

		internal override string TypeOf { get { return "function"; } }

		internal override string AsString() {
			return "[Function]";
		}

		public override Data Apply(Data thisObject, Data[] argv) {
			return Machine.CallFunction((Function)this, thisObject, argv);
		}

		internal static void Apply(Machine machine, Data thisObject, Data[] argv) {
			Function f = (Function)thisObject;
			if (argv[1] is Array) {
				Array a = (Array)argv[1];
				Data[] fargv = new Data[a.array.Count];
				for (int i = 0; i < a.array.Count; ++i) {
					fargv[i] = a.ReadProperty(i.ToString());
				}
				f.Call(machine, argv[0], fargv);
			} else {
				Arguments a = (Arguments)argv[1];
				f.Call(machine, argv[0], a.array);
			}
		}

		internal override string DebugString { get { return "@" + uniqueId + ": Function=" + definition.name; } }
	}

	internal class Number : Data {
		internal static Data NumberConstructor;

		static Object NumberProto;
		static Number() {
			NumberConstructor = new NativeClass(typeof(Number));
			NumberProto = new Object();

			NumberConstructor.SetProperty("NaN", new Number(double.NaN));
			NumberConstructor.SetProperty("prototype", NumberProto, Property.Flags.Writtable);

			NumberProto.SetProperty("constructor", NumberConstructor, Property.Flags.None);
			NumberProto.SetProperty("toString", new NativeMethod(Util.GetMethods(typeof(Number), "AsString")));
			NumberProto.SetProperty("hasOwnProperty", new NativeMethod(Util.GetMethods(typeof(Number), "HasOwnProperty")));
		}

		internal double v;

		internal Number(double v) : base() {
			this.v = v;
			proto = NumberProto;
		}

		internal override void SetProperty(string name, Data o, Property.Flags flags) {
		}

		internal override Property GetProperty(string name) {
			return proto.GetProperty(name);
		}

		internal override bool HasOwnProperty(string name) {
			return false;
		}

		internal override bool StrictlyEquals(Data rhs) {
			return rhs is Number && v == (rhs as Number).v;
		}

		internal override string TypeOf { get { return "number"; } }

		internal override bool AsBool() {
			return v != 0 && v != double.NaN;
		}

		internal override double AsFloat() {
			return v;
		}

		internal override string AsString() {
			return v.ToString();
		}

		internal override string DebugString { get { return "@" + uniqueId + ": Number=" + v; } }
	}

	internal class String : Data {
		internal static Data StringConstructor;
		static Object StringProto;

		static String() {
			StringConstructor = new NativeClass(typeof(String));
			StringConstructor.SetProperty("fromCharCode", new NativeFunction(Util.GetStaticMethod(typeof(String), "FromCharCode")), Property.Flags.None);

			StringProto = new Object();

			StringConstructor.SetProperty("prototype", StringProto, Property.Flags.Writtable);

			StringProto.SetProperty("constructor", StringConstructor, Property.Flags.None);
			StringProto.SetProperty("toString", new NativeMethod(Util.GetMethods(typeof(String), "AsString")));
			StringProto.SetProperty("hasOwnProperty", new NativeMethod(Util.GetMethods(typeof(String), "HasOwnProperty")));

			StringProto.SetProperty("charAt", new NativeMethod(Util.GetMethods(typeof(String), "CharAt")));
			StringProto.SetProperty("charCodeAt", new NativeMethod(Util.GetMethods(typeof(String), "CharCodeAt")));
			StringProto.DefineGetter("length", new NativeMethod(Util.GetMethods(typeof(String), "GetLength")));
			StringProto.SetProperty("concat", new NativeFunction(Util.GetStaticMethod(typeof(String), "Concat")), Property.Flags.None);
			StringProto.SetProperty("indexOf", new NativeFunction(Util.GetStaticMethod(typeof(String), "IndexOf")), Property.Flags.None);
			StringProto.SetProperty("lastIndexOf", new NativeFunction(Util.GetStaticMethod(typeof(String), "LastIndexOf")), Property.Flags.None);
			StringProto.SetProperty("slice", new NativeFunction(Util.GetStaticMethod(typeof(String), "Slice")), Property.Flags.None);
			StringProto.SetProperty("split", new NativeFunction(Util.GetStaticMethod(typeof(String), "Split")), Property.Flags.None);
			StringProto.SetProperty("substr", new NativeFunction(Util.GetStaticMethod(typeof(String), "Substr")), Property.Flags.None);
			StringProto.SetProperty("substring", new NativeFunction(Util.GetStaticMethod(typeof(String), "Substring")), Property.Flags.None);
			StringProto.SetProperty("toLowerCase", new NativeFunction(Util.GetStaticMethod(typeof(String), "ToLowerCase")), Property.Flags.None);
			StringProto.SetProperty("toUpperCase", new NativeFunction(Util.GetStaticMethod(typeof(String), "ToUpperCase")), Property.Flags.None);
			StringProto.SetProperty("trim", new NativeFunction(Util.GetStaticMethod(typeof(String), "Trim")), Property.Flags.None);
		}

		internal string v;

		internal String(string v) : base() {
			this.v = v;
			proto = StringProto;
		}

		internal override void SetProperty(string name, Data o, Property.Flags flags) {
		}

		internal override Property GetProperty(string name) {
			int index = StringToIndex(name);
			if (index >= 0 && index < v.Length) {
				return new Property() { data = new String(v[index].ToString()) };
			} else {
				return proto.GetProperty(name);
			}
		}

		internal override bool HasOwnProperty(string name) {
			return false;
		}

		internal override IEnumerable<Data> IterableKeys {
			get {
				for (var i = 0; i < v.Length; ++i) {
					yield return i;
				}
			}
		}

		internal override bool StrictlyEquals(Data rhs) {
			return rhs is String && v == (rhs as String).v;
		}

		internal override string TypeOf { get { return "string"; } }

		internal override bool AsBool() {
			return v.Length > 0;
		}

		internal override string AsString() {
			return v;
		}

		internal string CharAt(int n) {
			if (n >= 0 && n < v.Length) {
				return v[n].ToString();
			} else {
				return "";
			}
		}

		internal double CharCodeAt(int n) {
			if (n >= 0 && n < v.Length) {
				return (double)((int)v[n]);
			} else {
				return double.NaN;
			}
		}

		internal int GetLength() {
			return v.Length;
		}

		internal static void Concat(Machine machine, Data thisObject, Data[] argv) {
			String self = (String)thisObject;
			StringBuilder result = new StringBuilder(self.v);
			for (int i = 0; i < argv.Length; ++i) {
				result.Append((string)argv[i]);
			}
			machine.Push(new String(result.ToString()));
		}

		internal static void IndexOf(Machine machine, Data thisObject, Data[] argv) {
			String self = (String)thisObject;
			string pattern = (string)argv[0];
			int start = 0;
			if (argv.Length > 1) {
				start = (int)argv[1];
			}
			machine.Push(new Number(self.v.IndexOf(pattern, start)));
		}

		internal static void LastIndexOf(Machine machine, Data thisObject, Data[] argv) {
			String self = (String)thisObject;
			string pattern = (string)argv[0];
			int start = 0;
			if (argv.Length > 1) {
				start = (int)argv[1];
			}
			machine.Push(new Number(self.v.LastIndexOf(pattern, start)));
		}

		internal static void Slice(Machine machine, Data thisObject, Data[] argv) {
			String self = (String)thisObject;
			int start = (int)argv[0];
			if (start < 0) {
				start = self.v.Length + start;
				if (start < 0) {
					start = 0;
				}
			}
			if (start > self.v.Length) {
				start = self.v.Length;
			}
			int end = self.v.Length;
			if (argv.Length > 1) {
				end = (int)argv[1];
				if (end < 0) {
					end = self.v.Length + end;
				}
				if (end > self.v.Length) {
					end = self.v.Length;
				}
			}
			if (end < start) {
				end = start;
			}
			machine.Push(new String(self.v.Substring(start, end - start)));
		}

		internal static void Split(Machine machine, Data thisObject, Data[] argv) {
			String self = (String)thisObject;
			List<Property> result = new List<Property>();
			if (argv.Length == 0) {
				result.Add(new Property() { data = new String(self.v) });
			} else {
				string separator = (string)argv[0];
				int limit = System.Int32.MaxValue;
				if (argv.Length > 1) {
					limit = (int)argv[1];
				}
				int c = 0;
				if (separator.Length > 0) {
					while (c < self.v.Length && result.Count < limit) {
						int i = self.v.IndexOf(separator, c);
						if (i < 0) {
							result.Add(new Property() { data = new String(self.v.Substring(c)) } );
							break;
						} else {
							result.Add(new Property() { data = new String(self.v.Substring(c, i - c)) } );
							c = i + separator.Length;
						}
					}
				} else {
					while (c < self.v.Length && result.Count < limit) {
						result.Add(new Property() { data = new String(self.v.Substring(c, 1)) } );
						c++;
					}
				}
			}
			machine.Push(new Array(result));
		}

		internal static void Substr(Machine machine, Data thisObject, Data[] argv) {
			String self = (String)thisObject;
			int start = (int)argv[0];
			if (start < 0) {
				start = self.v.Length + start;
				if (start < 0) {
					start = 0;
				}
			}
			if (start > self.v.Length) {
				start = self.v.Length;
			}

			int length = self.v.Length - start;
			if (argv.Length > 1) {
				length = (int)argv[1];
				if (length < 0) {
					length = 0;
				}
				if (length > self.v.Length - start) {
					length = self.v.Length - start;
				}
			}
			machine.Push(new String(self.v.Substring(start, length)));
		}

		internal static void Substring(Machine machine, Data thisObject, Data[] argv) {
			String self = (String)thisObject;
			int start = (int)argv[0];
			if (start < 0) {
				start = 0;
			} else if (start > self.v.Length) {
				start = self.v.Length;
			}
			int end = self.v.Length;
			if (argv.Length > 1) {
				end = (int)argv[1];
				if (end < 0) {
					end = 0;
				} else if (end > self.v.Length) {
					end = self.v.Length;
				}
			}
			if (end < start) {
				start ^= end;
				end ^= start;
				start ^= end;
			}
			machine.Push(new String(self.v.Substring(start, end - start)));
		}

		internal static void ToLowerCase(Machine machine, Data thisObject, Data[] argv) {
			String self = (String)thisObject;
			machine.Push(new String(self.v.ToLower()));
		}

		internal static void ToUpperCase(Machine machine, Data thisObject, Data[] argv) {
			String self = (String)thisObject;
			machine.Push(new String(self.v.ToUpper()));
		}

		internal static void Trim(Machine machine, Data thisObject, Data[] argv) {
			String self = (String)thisObject;
			machine.Push(new String(self.v.Trim()));
		}

		internal static void FromCharCode(Machine machine, Data thisObject, Data[] argv) {
			Number code = (Number)argv[0];
			string s = "" + (char)((int)code.v);
			machine.Push(new String(s));
		}

		internal static string Escape(string str) {
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < str.Length; ++i) {
				if (str[i] == '"') {
					sb.Append("\\\"");
				} else if (str[i] == '\\') {
					sb.Append("\\\\");
				} else if (str[i] == '\n') {
					sb.Append("\\n");
				} else if (str[i] == '\b') {
					sb.Append("\\b");
				} else if (str[i] == '\f') {
					sb.Append("\\f");
				} else if (str[i] == '\r') {
					sb.Append("\\r");
				} else if (str[i] == '\t') {
					sb.Append("\\t");
				} else { //TODO: unicode
					sb.Append(str[i]);
				}
			}
			return sb.ToString();
		}

		internal override string DebugString { get { return "@" + uniqueId + ": String=\"" + v + "\""; } }
	}

	internal class Bool : Data {
		internal static Data BoolConstructor;
		static Object BoolProto;

		static Bool() {
			BoolConstructor = new NativeClass(typeof(Bool));
			BoolProto = new Object();

			BoolConstructor.SetProperty("prototype", BoolProto, Property.Flags.Writtable);

			BoolProto.SetProperty("constructor", BoolConstructor, Property.Flags.None);

			BoolProto.SetProperty("toString", new NativeMethod(Util.GetMethods(typeof(Bool), "AsString")));
			BoolProto.SetProperty("hasOwnProperty", new NativeMethod(Util.GetMethods(typeof(Bool), "HasOwnProperty")));
		}

		internal bool v;

		internal Bool(bool v) : base() {
			this.v = v;
			proto = BoolProto;
		}

		internal override void SetProperty(string name, Data o, Property.Flags flags) {
		}

		internal override Property GetProperty(string name) {
			return proto.GetProperty(name);
		}

		internal override bool HasOwnProperty(string name) {
			return false;
		}

		internal override bool StrictlyEquals(Data rhs) {
			return rhs is Bool && v == (rhs as Bool).v;
		}

		internal override string TypeOf { get { return "boolean"; } }

		internal override bool AsBool() {
			return v;
		}

		internal override double AsFloat() {
			return v ? 1 : 0;
		}

		internal override string AsString() {
			return v ? "true" : "false";
		}

		internal override string DebugString { get { return "@" + uniqueId + ": Bool=" + v; } }
	}

	internal class Undefined : Data {
		internal string lookupName;

		internal Undefined() : base() {
		}

		internal Undefined(string lookupName) : base() {
			this.lookupName = lookupName;
		}

		internal override bool StrictlyEquals(Data rhs) {
			return this is Undefined;
		}

		internal override bool IsNullValue { get { return true; } }

		internal override string TypeOf { get { return "undefined"; } }

		internal override bool AsBool() {
			return false;
		}

		internal override string AsString() {
			return "undefined";
		}

		internal override string DebugString { get { return "@" + uniqueId + ": Undefined"; } }

		public override string ToString() {
			return "[Undefined " + lookupName + "]";
		}
	}

	internal class Null : Data {
		internal Null() : base() {
		}

		internal override bool StrictlyEquals(Data rhs) {
			return this == rhs;
		}

		internal override bool IsNullValue { get { return true; } }

		internal override string TypeOf { get { return "object"; } }

		internal override double AsFloat() {
			return 0;
		}

		internal override bool AsBool() {
			return false;
		}

		internal override string AsString() {
			return "null";
		}

		internal override string DebugString { get { return "@" + uniqueId + ": Null"; } }
	}

	internal class EndOfIterator : Null {
	}

	internal class Date : Data {
		internal static Data DateConstructor;
		static Object DateProto;

		static Date() {
			DateConstructor = new NativeClass(typeof(Date));
			DateProto = new Object();

			DateConstructor.SetProperty("prototype", DateProto, Property.Flags.Writtable);

			DateProto.SetProperty("constructor", DateConstructor, Property.Flags.None);

			DateProto.SetProperty("toString", new NativeMethod(Util.GetMethods(typeof(Date), "AsString")));
			DateProto.SetProperty("hasOwnProperty", new NativeMethod(Util.GetMethods(typeof(Date), "HasOwnProperty")));
		}

		internal Date() : base() {
			proto = DateProto;
		}

		internal override void SetProperty(string name, Data o, Property.Flags flags) {
		}

		internal override Property GetProperty(string name) {
			return proto.GetProperty(name);
		}

		internal override bool HasOwnProperty(string name) {
			return false;
		}

		internal override bool StrictlyEquals(Data rhs) {
			return rhs is Date;
		}

		internal override string TypeOf { get { return "object"; } }

		internal override bool AsBool() {
			return true;
		}

		internal override double AsFloat() {
			return 0;
		}

		internal override string AsString() {
			return "";
		}

		internal override string DebugString { get { return "@" + uniqueId + ": Date=" + ""; } }
	}

	internal class NativeEnum : Data {
		System.Type type;
		
		internal NativeEnum(System.Type type) : base() {
			this.type = type;
		}

		internal override Property GetProperty(string name) {
			return new Property() { data = Serialization.SerializeFlat(System.Enum.Parse(type, name)) };
		}
	}

	internal class NativeClass : Data {
		System.Type type;
		ConstructorInfo[] ctors;

		internal NativeClass(System.Type type) : base() {
			this.type = type;
			ctors = type.GetConstructors(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
		}

		internal override Property GetProperty(string name) {
			FieldInfo fieldInfo = type.GetField(name, BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
			if (fieldInfo != null) {
				return new Property() { data = Serialization.SerializeFlat(fieldInfo.GetValue(null)) };
			} else {
				PropertyInfo propInfo = type.GetProperty(name, BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
				if (propInfo != null) {
					return new Property() { data = Serialization.SerializeFlat(propInfo.GetValue(null, null)) };
				} else {
					MethodInfo[] methodInfos = Util.GetMethods(type, name, BindingFlags.Static);
					if (methodInfos.Length > 0) {
						return new Property() { data = new NativeMethod(methodInfos) };
					}
					return GetHashProperty(name);
				}
			}
		}

		internal override void SetProperty(string name, Data d, Property.Flags flags) {
			FieldInfo fieldInfo = type.GetField(name, BindingFlags.Static);
			if (fieldInfo != null) {
				object nativeValue = Serialization.Deserialize(fieldInfo.FieldType, d);
				fieldInfo.SetValue(null, nativeValue);
			} else {
				SetHashProperty(name, d, flags);
			}
		}

		internal override void Call(Machine machine, Data thisObject, Data[] argv) {
			object result = null;
			ConstructorInfo ctor = null;
			object[] nativeArgs = null;
			for (int i = 0; i < ctors.Length; ++i) {
				ParameterInfo[] args = ctors[i].GetParameters();
				if (args.Length >= argv.Length) {
					try {
						if (ctor == null || ctor.GetParameters().Length > args.Length) {
							nativeArgs = Native.GetConvertedArguments(argv, args);
							ctor = ctors[i];
						}
					} catch (DeserializationException) {
					}
				}
			}
			if (ctor != null) {
				result = ctor.Invoke(nativeArgs);
				machine.Push(Serialization.SerializeFlat(result));
			} else {
				throw new RuntimeError("No matching native constructor found.");
			}
		}

		internal override bool StrictlyEquals(Data rhs) {
			return rhs is NativeClass && type == (rhs as NativeClass).type;
		}

		internal override string AsString() {
			return "__NativeClass__" + type;
		}
	}

	internal class NativeMethod : Data {
		static Object NativeMethodProto;

		static NativeMethod() {
			NativeMethodProto = new Object();
			NativeMethodProto.SetProperty("call", new NativeFunction(Util.GetStaticMethod(typeof(Data), "StaticCall")));
		}

		internal MethodInfo[] methodInfos;

		internal NativeMethod(MethodInfo[] mis) {
			methodInfos = mis;
			proto = NativeMethodProto;
		}

		internal override Property GetProperty(string name) {
			return GetHashProperty(name);
		}

		internal override void Call(Machine machine, Data thisObject, Data[] argv) {
			object result = null;
			MethodInfo method = null;
			object[] nativeArgs = null;
			object caller;
			for (int i = 0; i < methodInfos.Length; ++i) {
				ParameterInfo[] args = methodInfos[i].GetParameters();
				if (args.Length >= argv.Length) {
					try {
						if (method == null || method.GetParameters().Length > args.Length) {
							nativeArgs = Native.GetConvertedArguments(argv, args);
							method = methodInfos[i];
						}
					} catch (DeserializationException) {
					}
				}
			}
			if (method != null) {
				if (method.IsStatic) {
					caller = null;
				} else {
					//XXX: It would be better if used AsNativeCaller() instead.
					if (method.DeclaringType == typeof(Data) || method.DeclaringType.IsSubclassOf(typeof(Data))) {
						caller = thisObject;
					} else {
						caller = Serialization.Deserialize(method.DeclaringType, thisObject);
					}
				}
				result = method.Invoke(caller, nativeArgs);
				machine.Push(Serialization.SerializeFlat(result));
			} else {
				throw new RuntimeError("No matching native method found for " + methodInfos[0].Name + ".");
			}
		}

		internal override string AsString() {
			return "<native method: " + methodInfos[0].Name + ">";
		}
	}

	internal class Native : Data {
		static Object NativeProto;
		static Object NativeListProto;

		internal static Object NativeListConstructor;

		static Object NativeArrayProto;
		internal static Object NativeArrayConstructor;

		static Native() {
			NativeProto = new Object();

			NativeProto.SetProperty("next", new NativeFunction(Util.GetStaticMethod(typeof(Native), "Next")));
			NativeProto.SetProperty("iterator", new NativeFunction(Util.GetStaticMethod(typeof(Native), "GetIterator")));

			NativeArrayConstructor = new Object();
			NativeArrayProto = new Object();

			NativeArrayConstructor.SetProperty("prototype", Array.ArrayProto, Property.Flags.None);

			NativeArrayProto.proto = Array.ArrayProto;
			NativeArrayProto.SetProperty("constructor", NativeArrayConstructor, Property.Flags.None);
			NativeArrayProto.SetProperty("next", new NativeFunction(Util.GetStaticMethod(typeof(Native), "Next")));
			NativeArrayProto.SetProperty("iterator", new NativeFunction(Util.GetStaticMethod(typeof(Native), "GetIterator")));
			NativeArrayProto.DefineGetter("length", new NativeMethod(Util.GetMethods(typeof(Native), "GetNativeArrayLength")));
			NativeArrayProto.SetProperty("toArray", new NativeFunction(Util.GetStaticMethod(typeof(Native), "NativeArrayToArray")));
			NativeArrayProto.DefineGetter("indexOf", new NativeFunction(Util.GetStaticMethod(typeof(Native), "NativeArrayIndexOf")));
			NativeArrayProto.SetProperty("slice", new NativeFunction(Util.GetStaticMethod(typeof(Native), "NativeArraySlice")));

			NativeListConstructor = new Object();
			NativeListProto = new Object();

			NativeListConstructor.SetProperty("prototype", Array.ArrayProto, Property.Flags.None);

			NativeListProto.proto = Array.ArrayProto;
			NativeListProto.SetProperty("constructor", NativeListConstructor, Property.Flags.None);
			NativeListProto.SetProperty("next", new NativeFunction(Util.GetStaticMethod(typeof(Native), "Next")));
			NativeListProto.SetProperty("iterator", new NativeFunction(Util.GetStaticMethod(typeof(Native), "GetIterator")));
			NativeListProto.DefineGetter("length", new NativeMethod(Util.GetMethods(typeof(Native), "GetNativeListLength")));
			NativeListProto.SetProperty("push", new NativeFunction(Util.GetStaticMethod(typeof(Native), "NativeListPush")));
			NativeListProto.SetProperty("concat", new NativeFunction(Util.GetStaticMethod(typeof(Native), "NativeListConcat")));
			NativeListProto.SetProperty("toArray", new NativeFunction(Util.GetStaticMethod(typeof(Native), "NativeListToArray")));
			NativeListProto.SetProperty("indexOf", new NativeFunction(Util.GetStaticMethod(typeof(Native), "NativeListIndexOf")));
		}

		internal object native;
		internal IEnumerator enumerator;

		internal Native(object native) : base() {
			this.native = native;

			// workaround to keep js array and c# native array-like collections interface similar
			if (native is System.Array) {
				this.proto = NativeArrayProto;
			} else if (native is IList) {
				this.proto = NativeListProto;
			} else {
				this.proto = NativeProto;
			}
		}

		internal override Property GetProperty(string name) {
			int index = StringToIndex(name);
			Data p = null;
			if (index >= 0) {
				if (native is System.Array) {
					p = Serialization.SerializeFlat(((System.Array)native).GetValue(index));
				} else if (native is IList) {
					p = Serialization.SerializeFlat(typeof(IList).GetProperty("Item").GetValue(native, new object[] { index }));
				} else {
					throw new TypeError("Native property is not indexed.");
				}
			} else {
				FieldInfo fieldInfo = native.GetType().GetField(name);
				if (fieldInfo != null) {
					p = Serialization.SerializeFlat(fieldInfo.GetValue(native));
				} else {
					PropertyInfo propInfo = native.GetType().GetProperty(name);
					if (propInfo != null) {
						Property prop = new Property();
						if (propInfo.GetGetMethod() != null) {
							prop.getter = new NativeMethod(new MethodInfo[] { propInfo.GetGetMethod() });
						}
						if (propInfo.GetSetMethod() != null) {
							prop.setter = new NativeMethod(new MethodInfo[] { propInfo.GetSetMethod() });
						}
						return prop;
					} else {
						MethodInfo[] methodInfos = Util.GetMethods(native.GetType(), name);
						if (methodInfos.Length > 0) {
							p = new NativeMethod(methodInfos);
						} else if (native is IDictionary) {
							p = Serialization.SerializeFlat(typeof(IDictionary).GetProperty("Item").GetValue(native, new object[] { name }));
						}
					}
				} 
			}
			if (p != null) {
				return new Property() { data = p };
			} else {
				return GetHashProperty(name);
			}
		}

		internal override void SetProperty(string name, Data d, Property.Flags flags) {
			int index = StringToIndex(name);
			if (index >= 0) {
				if (native is System.Array) {
					((System.Array)native).SetValue(Serialization.Deserialize(typeof(object), d), index);
				} else if (native is IList) {
					typeof(IList).GetProperty("Item").SetValue(native, Serialization.Deserialize(typeof(object), d), new object[] { index });
				} else {
					throw new TypeError("Native property is not indexed.");
				}
			} else {
				FieldInfo fieldInfo = native.GetType().GetField(name);
				if (fieldInfo != null) {
					object nativeValue = Serialization.Deserialize(fieldInfo.FieldType, d);
					fieldInfo.SetValue(native, nativeValue);
				} else {
					PropertyInfo propInfo = native.GetType().GetProperty(name);
					if (propInfo != null) {
						object nativeValue = Serialization.Deserialize(propInfo.PropertyType, d);
						propInfo.SetValue(native, nativeValue, null);
					} else if (native is IDictionary) {
						typeof(IDictionary).GetProperty("Item").SetValue(native, Serialization.Deserialize(typeof(object), d), new object[] { name });
					} else {
						SetHashProperty(name, d, flags);
					}
				}
			}
		}

		internal static object[] GetConvertedArguments(Data[] argv, ParameterInfo[] parametersInfo) {
			return Native.ConvertArgumentsToNative(argv, parametersInfo);
		}

		internal override void Call(Machine machine, Data thisObject, Data[] argv) {
			object result = null;
			System.Delegate delg =  (System.Delegate)native;
			MethodInfo method;
			object caller;
			method = delg.Method;
			caller = delg.Target;

			result = method.Invoke(caller, Native.GetConvertedArguments(argv, method.GetParameters()));

			machine.Push(Serialization.SerializeFlat(result));
		}

		internal override bool StrictlyEquals(Data rhs) {
			return rhs is Native && ((native == (rhs as Native).native) || (native != null && native.Equals((rhs as Native).native)));
		}
		internal override bool IsNullValue { get { return native == null; } }

		internal override string TypeOf { get { return "object"; } }

		internal override double AsFloat() {
			return double.NaN;
		}

		internal override string AsString() {
			if (native != null) {
				return native.ToString();
			} else {
				return "<native null>";
			}
		}

		internal override bool AsBool() {
			return native != null;
		}

		internal static object[] ConvertArgumentsToNative(Data[] values, ParameterInfo[] parametersInfo) {
			int numArgs = parametersInfo.Length < values.Length ? parametersInfo.Length : values.Length;
			object[] result = new object[parametersInfo.Length];
			for (int i = 0; i < numArgs; ++i) {
				System.Type paramType = parametersInfo[i].ParameterType;
				result[i] = Serialization.Deserialize(paramType, values[i]);
			}
//#if UNITY_EDITOR
			//TODO: Check for workaround
			for (int i = numArgs; i < parametersInfo.Length; ++i) {
				System.Type paramType = parametersInfo[i].ParameterType;
				if (paramType.IsValueType) {
					result[i] = System.Activator.CreateInstance(paramType);
				} else {
					result[i] = null;
				}
			}
//#endif
			for (int i = numArgs; i < parametersInfo.Length; ++i) {
				result[i] = System.Type.Missing;
			}
			return result;
		}

		internal static System.Delegate CastDelegate(System.Delegate source, System.Type type) {
			System.Delegate[] delegates = source.GetInvocationList();
			if (delegates.Length == 1) {
				return System.Delegate.CreateDelegate(type, delegates[0].Target, delegates[0].Method);
			} else {
				throw new TypeError("Multi-invocation delegate not supported yet!");
			}
		}

		internal static void Next(Machine machine, Data thisObject, Data[] argv) {
			Native n = (Native)thisObject;

			if (n.enumerator == null) {
				n.enumerator = ((System.Collections.IEnumerable)n.native).GetEnumerator();
			}

			Object o = new Object();
			if (n.enumerator.MoveNext()) {
				o.SetProperty("value", Serialization.SerializeFlat(n.enumerator.Current));
				o.SetProperty("done", new Bool(false));
			} else {
				o.SetProperty("value", Data.Undefined);
				o.SetProperty("done", new Bool(true));
			}
			machine.Push(o);
		}

		internal static void GetIterator(Machine machine, Data thisObject, Data[] argv) {
			machine.Push(thisObject);
		}

		internal int GetNativeArrayLength() {
			return (native as System.Array).Length;
		}

		internal int GetNativeListLength() {
			return (native as IList).Count;
		}

		internal static void NativeArrayToArray(Machine machine, Data thisObject, Data[] argv) {
			Native self = (Native)thisObject;
			System.Array na = (System.Array)self.native;
			List<Property> a = new List<Property>();
			for (int i = 0; i < na.Length; ++i) {
				a.Add(new Property() { data = Serialization.SerializeFlat(na.GetValue(i)) });
			}
			machine.Push(new Array(a));
		}

		internal static void NativeArrayIndexOf(Machine machine, Data thisObject, Data[] argv) {
			Native self = (Native)thisObject;
			System.Array na = (System.Array)self.native;
			Data e = argv[0];
			int s = 0, r = -1;
			if (argv.Length > 1) {
				s = argv[1].AsInt32();
			}
			if (e is Native) { // fast case
				Native ne = (Native)e;
				for (int i = s; i < na.Length; ++i) {
					if (ne.native == na.GetValue(i)) {
						r = i;
						break;
					}
				}
			} else { // not optimized case
				for (int i = s; i < na.Length; ++i) {
					if (Data.DataEquals(Serialization.SerializeFlat(na.GetValue(i)), e)) {
						r = i;
						break;
					}
				}
			}
			machine.Push(new Number(r));
		}

		internal static void NativeListToArray(Machine machine, Data thisObject, Data[] argv) {
			Native self = (Native)thisObject;
			IList nl = (IList)self.native;
			List<Property> a = new List<Property>();
			for (int i = 0; i < nl.Count; ++i) {
				a.Add(new Property() { data = Serialization.SerializeFlat(nl[i]) });
			}
			machine.Push(new Array(a));
		}

		internal static void NativeArraySlice(Machine machine, Data thisObject, Data[] argv) {
			Native self = (Native)thisObject;
			System.Array na = self.native as System.Array;
			int s = argv[0].AsInt32();
			int e = argv[1].AsInt32();

			List<Property> a = new List<Property>();
			for (int i = s; i < e && i < na.Length; ++i) {
				a.Add(new Property() { data = Serialization.SerializeFlat(na.GetValue(i)) });
			}
			machine.Push(new Array(a));
		}


		internal static void NativeListIndexOf(Machine machine, Data thisObject, Data[] argv) {
			Native self = (Native)thisObject;
			IList nl = (IList)self.native;
			Data e = argv[0];
			int s = 0, r = -1;
			if (argv.Length > 1) {
				s = argv[1].AsInt32();
			}
			if (e is Native) { // fast case
				Native ne = (Native)e;
				for (int i = s; i < nl.Count; ++i) {
					if (ne.native == nl[i]) {
						r = i;
						break;
					}
				}
			} else { // not optimized case
				for (int i = s; i < nl.Count; ++i) {
					if (Data.DataEquals(Serialization.SerializeFlat(nl[i]), e)) {
						r = i;
						break;
					}
				}
			}
			machine.Push(new Number(r));
		}

		internal static void NativeListPush(Machine machine, Data thisObject, Data[] argv) {
			Native self = (Native)thisObject;
			(self.native as IList).Add(Serialization.Deserialize(typeof(object), argv[0]));
			machine.Push(thisObject);
		}

		internal static void NativeListConcat(Machine machine, Data thisObject, Data[] argv) {
			Native self = (Native)thisObject;
			IList selfList = (IList)self.native;

			List<Property> sum = new List<Property>();
			for (int i = 0; i < selfList.Count; ++i) {
				sum.Add(new Property() { data = Serialization.SerializeFlat(selfList[i]) });
			}

			for (int i = 0; i < argv.Length; ++i) {
				if (argv[i] is Array) {
					Array a = (Array)argv[i];
					sum.AddRange(a.array);
				} else if (argv[i] is Native) {
					Native n = (Native)argv[i];
					if (n.native is System.Array) {
						System.Array na = (System.Array)n.native;
						for (int j = 0; j < na.Length; ++j) {
							sum.Add(new Property() { data = Serialization.SerializeFlat(na.GetValue(j)) });
						}
					} else if (n.native is IList) {
						IList nl = (IList)n.native;
						for (int j = 0; j < nl.Count; ++j) {
							sum.Add(new Property() { data = Serialization.SerializeFlat(nl[j]) });
						}
					} else {
						throw new TypeError("Cannot concat " + n.native + " with native list.");
					}
				} else {
					throw new TypeError("Cannot concat " + argv[i].GetType().Name + " with native list.");
				}
			}

			machine.Push(new Array(sum));
		}

		internal override string DebugString { get { return "@" + uniqueId + ": Native=" + (native != null ? native.GetType().Name : "null"); } }
	}

	internal class NativeFunction : Data {
		static Object NativeFunctionProto;

		static NativeFunction() {
			NativeFunctionProto = new Object();
			NativeFunctionProto.SetProperty("call", new NativeFunction(Util.GetStaticMethod(typeof(Data), "StaticCall")));
			NativeFunctionProto.SetProperty("apply", new NativeFunction(Util.GetStaticMethod(typeof(Data), "StaticApply")));
		}

		internal MethodInfo function;

		internal NativeFunction(MethodInfo function) : base() {
			this.function= function;
			proto = NativeFunctionProto;
		}

		internal override Property GetProperty(string name) {
			return GetHashProperty(name);
		}

		internal override void SetProperty(string name, Data d, Property.Flags flags) {
		}

		internal override void Call(Machine machine, Data thisObject, Data[] argv) {
			function.Invoke(null, new object[] { machine, thisObject, argv });
		}

		internal override bool StrictlyEquals(Data rhs) {
			return rhs is NativeFunction && function == (rhs as NativeFunction).function;
		}

		internal override string TypeOf { get { return "function"; } }

		internal override double AsFloat() {
			return double.NaN;
		}

		internal override string AsString() {
			return "<NativeFunction>";
		}

		internal override bool AsBool() {
			return true;
		}

		internal override string DebugString { get { return "@" + uniqueId + ": NativeFunction"; } }
	}

	public class Console {
		public static void log(Data a) {
#if UNITY_EDITOR
			UnityEngine.Debug.Log((string)a);
#else
			System.Console.WriteLine((string)a);
#endif
		}

		public static void warn(Data a) {
#if UNITY_EDITOR
			UnityEngine.Debug.LogWarning((string)a);
#else
			System.Console.WriteLine((string)a);
#endif
		}

		public static void info(Data a) {
			log(a);
		}
	}

	public class Globals {
		internal static void ParseInt(Machine machine, Data thisObject, Data[] argv) {
			string arg = (string)argv[0];
			//TODO: parsing base param
			machine.Push(new Number(int.Parse(arg)));
		}

		internal static void ParseFloat(Machine machine, Data thisObject, Data[] argv) {
			string arg = (string)argv[0];
			machine.Push(new Number(double.Parse(arg, NumberStyles.Any, System.Globalization.CultureInfo.InvariantCulture)));
		}

		internal static void IsNaN(Machine machine, Data thisObject, Data[] argv) {
			double arg = (double)argv[0];
			machine.Push(new Bool(double.IsNaN(arg)));
		}
	}

	public class Math {
		static System.Random randomGenerator = new System.Random();

		public static double E = System.Math.E;
		public static double PI = System.Math.PI;
		public static double SQRT2;
		public static double SQRT1_2;
		public static double LN2;
		public static double LN10;
		public static double LOG2E;
		public static double LOG10E;

		static Math() {
			SQRT2 = System.Math.Sqrt(2.0);
			SQRT1_2 = System.Math.Sqrt(0.5);
			LN2 = System.Math.Log(2.0);
			LN10 = System.Math.Log(10.0);
			LOG2E = System.Math.Log(E, 2.0);
			LOG10E = System.Math.Log10(E);
		}

		public static double floor(double a) {
			return System.Math.Floor(a);
		}
		public static double ceil(double a) {
			return System.Math.Ceiling(a);
		}
		public static double round(double a) {
			return System.Math.Round(a);
		}
		public static double abs(double a) {
			return System.Math.Abs(a);
		}
		public static double random() {
			return randomGenerator.NextDouble(); 
		}
		public static double max(double a, double b) {
			return a > b ? a : b;
		}
		public static double min(double a, double b) {
			return a < b ? a : b;
		}
		public static double sqrt(double a) {
			return System.Math.Sqrt(a);
		}
		public static double pow(double a, double b) {
			return System.Math.Pow(a, b);
		}
		public static double sin(double a) {
			return System.Math.Sin(a);
		}
		public static double cos(double a) {
			return System.Math.Cos(a);
		}
		public static double tan(double a) {
			return System.Math.Tan(a);
		}
		public static double asin(double a) {
			return System.Math.Asin(a);
		}
		public static double acos(double a) {
			return System.Math.Asin(a);
		}
		public static double atan(double a) {
			return System.Math.Atan(a);
		}
		public static double atan2(double y, double x) {
			return System.Math.Atan2(y,x);
		}
		public static double exp(double a) {
			return System.Math.Exp(a);
		}
		public static double log(double a) {
			return System.Math.Log(a);
		}
	}

	public class JSON {
		public static string stringify(Data v) {
			return Stringify(v);
		}

		public static Data parse(string json) {
			try {
				Parser p = new Parser(json);
				Node jsonExpr = p.ParseJSON();
				Code code = new Code();
				jsonExpr.Compile(code);
				code.Emit(Operation.Ret);
				Machine machine = new Machine();
				machine.Enter(code, new Scope());
				machine.Run();
				Data result = machine.PopRef().data;
				return result;
			} catch (System.Exception e) {
				JS.VerboseLog(e.StackTrace);
				throw new JsonError(json);
			}
		}

		internal static string Stringify(Data v) {
			if (v.IsPrimitive) {
				if (v.IsString) {
					return '"'+String.Escape((string)v)+'"';
				} else {
					return (string)v;
				}
			} else if (v.IsArray) {
				Array a = (Array)v;
				string result = "[";
				bool st = true;
				for (int i = 0; i < a.array.Count; ++i) {
					Data item = a.ReadProperty(i.ToString());
					if (!st) {
						result += ",";
					} else {
						st = false;
					}
					result += Stringify(item);
				}
				result += "]";
				return result;
			} else {
				string result = "{";
				bool st = true;
				if (v.hash != null) {
					foreach (KeyValuePair<string, Property> ownProp in v.hash) {
						if (ownProp.Value.IsEnumerable) {
							Data item = v.ReadProperty(ownProp.Key);
							if (!st) {
								result += ",";
							} else {
								st = false;
							}
							result += '"'+ownProp.Key+"\":";
							result += Stringify(item);
						}
					}
				}
				result += "}";
				return result;
			}
		}

	}

	public class Base64 { 
		static int[] asciiToHect = new int[255];
		static string hectToAscii = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/="; 
		static int[] dbuf = new int[4];

		static Base64() {
			for (int i = 0; i < hectToAscii.Length; ++i) {
				asciiToHect[(int)hectToAscii[i]] = i;
			}
		}

		public static string DecodeString(string src) {
			StringBuilder res = new StringBuilder();
			int dp = 0;
			for (int i = 0; i < src.Length && src[i] != '='; ++i) {
				dbuf[dp++] = asciiToHect[src[i]];
				if (dp == 4) {
					int b0 = ((dbuf[0] << 2) | (dbuf[1] >> 4)) & 255;
					int b1 = ((dbuf[1] << 4) | (dbuf[2] >> 2)) & 255;
					int b2 = ((dbuf[2] << 6) | dbuf[3]) & 255;

					res.Append((char)b0);
					res.Append((char)b1);
					res.Append((char)b2);
					dp = 0;
				}
			}
			if (dp == 2) {
				int b0 = ((dbuf[0] << 2) | (dbuf[1] >> 4)) & 255;
				res.Append((char)b0);
			} else if (dp == 3) {
				int b0 = ((dbuf[0] << 2) | (dbuf[1] >> 4)) & 255;
				int b1 = ((dbuf[1] << 4) | (dbuf[2] >> 2)) & 255;
				res.Append((char)b0);
				res.Append((char)b1);
			}
			return res.ToString();
		}

		public static string EncodeString(string src) {
			StringBuilder res = new StringBuilder();
			int dp = 0;
			for (int i = 0; i < src.Length; ++i) {
				dbuf[dp++] = (byte)src[i];

				if (dp == 3) {
					int t = (dbuf[0] << 16) | (dbuf[1] << 8) | dbuf[2];
					int h0 = (t >> 18) & 63;
					int h1 = (t >> 12) & 63;
					int h2 = (t >> 6) & 63;
					int h3 = t & 63;

					res.Append(hectToAscii[h0]);
					res.Append(hectToAscii[h1]);
					res.Append(hectToAscii[h2]);
					res.Append(hectToAscii[h3]);

					dp = 0;
				}
			}
			if (dp == 1) {
				int t = (dbuf[0] << 16);
				int h0 = (t >> 18) & 63;
				int h1 = (t >> 12) & 63;
				res.Append(hectToAscii[h0]);
				res.Append(hectToAscii[h1]);
				res.Append(hectToAscii[64]);
				res.Append(hectToAscii[64]);
			} else if (dp == 2) {
				int t = (dbuf[0] << 16) | (dbuf[1] << 8);
				int h0 = (t >> 18) & 63;
				int h1 = (t >> 12) & 63;
				int h2 = (t >> 6) & 63;
				res.Append(hectToAscii[h0]);
				res.Append(hectToAscii[h1]);
				res.Append(hectToAscii[h2]);
				res.Append(hectToAscii[64]);
			}
			return res.ToString();
		}

		public static char EncodeDigit(int d) {
			return hectToAscii[d];
		}

		public static int DecodeDigit(char d) {
			return asciiToHect[d];
		}

		public static int DecodeVLQSegment(string src, ref int p) {
			int shl = 0;
			int r = 0;
			int cont = 1;
			while (cont != 0) {
				int d = DecodeDigit(src[p++]);
				cont = d & 32;
				d &= 31;
				r |= d << shl;
				shl += 5;
			}
			r = (r & 1) == 1 ? -(r >> 1) : (r >> 1);
			return r;
		}
	}

	public class Serialization {

		public enum Options : int {
			None = 0,
			Flat = 1,
			IgnoreFields = 2,
			IgnoreProperties = 4,
			PreserveTypeName = 8
		}

		public class Required : System.Attribute {
		}
		public class DoNotSerialize : System.Attribute {
		}

		public static T Deserialize<T>(Data d) {
			return (T)Deserialize(typeof(T), d);
		}

		public static object Deserialize(System.Type t, Data d) {
			if (t == typeof(Data) || (t.IsSubclassOf(typeof(Data)) && d.GetType().IsSubclassOf(t))) {
				return d;
			}

			if (d is Number) {
				Number dn = (Number)d;
				if (t == typeof(double)) {
					return dn.v;
				} else if (t == typeof(object)) {
					return dn.v;
				} else if (t == typeof(float)) {
					return (float)dn.v;
				} else if (t == typeof(int)) {
					return (int)dn.v;
				} else if (t == typeof(uint)) {
					return (uint)dn.v;
				} else if (t == typeof(long)) {
					return (long)dn.v;
				} else if (t == typeof(ulong)) {
					return (ulong)dn.v;
				} else if (t == typeof(short)) {
					return (short)dn.v;
				} else if (t == typeof(ushort)) {
					return (ushort)dn.v;
				} else if (t == typeof(byte)) {
					return (byte)dn.v;
				} else if (t == typeof(sbyte)) {
					return (sbyte)dn.v;
				} else if (t == typeof(char)) {
					return (char)dn.v;
				} else if (t == typeof(string)) {
					return dn.v.ToString();
				}
				throw new DeserializationException("Cannot convert " + d.GetType() + " to " + t);
			} else if (d is String) {
				String ds = (String)d;
				if (t == typeof(string)) {
					return ds.v;
				} else if (t == typeof(object)) {
					return ds.v;
				} else if (t == typeof(int)) {
					return int.Parse(ds.v, System.Globalization.CultureInfo.InvariantCulture);
				} else if (t == typeof(long)) {
					return long.Parse(ds.v, System.Globalization.CultureInfo.InvariantCulture);
				} else if (t.IsEnum) {
					return System.Enum.Parse(t, ds.v);
				}
				throw new DeserializationException("Cannot convert " + d.GetType() + " to " + t);
			} else if (d is Bool) {
				Bool db = (Bool)d;
				if (t == typeof(bool)) {
					return db.v;
				} else if (t == typeof(object)) {
					return db.v;
				} else if (t == typeof(string)) {
					return db.AsString();
				}
				throw new DeserializationException("Cannot convert " + d.GetType() + " to " + t);
			} else if (d is Null || d is Undefined) {
				if (t == typeof(bool)) {
					return false;
				} else {
					return null;
				}
			} else if (d is Native) {	// --> Complex types
				Native dn = (Native)d;
				if (dn.native == null) {
					return null;
				}

				System.Type nt = dn.native.GetType(); 
				if (nt == t || nt.IsSubclassOf(t)) {
					return ((Native)d).native;
				}

				//TODO: --> Native collections conversion
				throw new DeserializationException("Cannot convert " + d.GetType() + " to " + t);
			} else if (d is Function) { // --> Delegates
				if (t == typeof(System.Action)) {
					return new System.Action(() => { d.Apply(null, new Data[] { }); });
				} else if (t == typeof(System.Action<object>)) {
					return new System.Action<object>(p0 => { d.Apply(null, new Data[] { Data.Of(p0) }); });
				} else if (t == typeof(System.Action<object,object>)) {
					return new System.Action<object,object>((p0, p1) => { d.Apply(null, new Data[] { Data.Of(p0), Data.Of(p1) }); });
				} else if (t == typeof(System.Action<object,object,object>)) {
					return new System.Action<object,object,object>((p0, p1,p2) => { d.Apply(null, new Data[] { Data.Of(p0), Data.Of(p1), Data.Of(p2) }); });
				}

				if (t == typeof(System.Func<object>)) {
					return new System.Func<object>(() => { return d.Apply(null, new Data[] { }); });
				} else if (t == typeof(System.Func<object, object>)) {
					return new System.Func<object, object>((p0) => { return d.Apply(null, new Data[] { Data.Of(p0) }); });
				} else if (t == typeof(System.Func<object, object, object>)) {
					return new System.Func<object, object, object>((p0, p1) => { return d.Apply(null, new Data[] { Data.Of(p0), Data.Of(p1) }); });
				} else if (t == typeof(System.Func<object, object, object, object>)) {
					return new System.Func<object, object, object, object>((p0, p1, p2) => { return d.Apply(null, new Data[] { Data.Of(p0), Data.Of(p1), Data.Of(p2) }); });
				}
				throw new DeserializationException("Cannot convert " + d.GetType() + " to " + t);
			} else if (d is Array) {
				Array da = (Array)d;
				if (t.IsArray) {
					System.Type et = t.GetElementType();
					System.Array oa = System.Array.CreateInstance(et, da.array.Count);
					for (int i = 0 ; i < da.array.Count; ++i) {
						object e = Deserialize(et, da.array[i].Read(da));
						oa.SetValue(e, i);
					}
					return oa;
				} else if (t == typeof(ArrayList)) {
					ArrayList al = new ArrayList();
					for (int i = 0 ; i < da.array.Count; ++i) {
						al.Add(Deserialize(typeof(object), da.array[i].Read(da)));
					}
					return al;
				} else if (t.IsGenericType) {
					System.Type gt = t.GetGenericTypeDefinition();
					if (gt == typeof(List<>)) {
						System.Type et = t.GetGenericArguments()[0];
						IList li = (IList)System.Activator.CreateInstance(t);
						for (int i = 0 ; i < da.array.Count; ++i) {
							object e = Deserialize(et, da.array[i].Read(da));
							li.Add(e);
						}
						return li;
					}
				}
				throw new DeserializationException("Cannot convert " + d.GetType() + " to " + t);
			} else if (d is Object) {
				if (d.HasOwnProperty("$type")) {
					string typeName = d.ReadProperty("$type").AsString();
					t = System.Type.GetType(typeName);
				}

				if (t == typeof(Hashtable)) {
					Hashtable ht = new Hashtable();
					foreach (Data kd in d.IterableKeys) {
						string k = ((String)kd).v;
						ht.Add(k, Deserialize(typeof(object), d.ReadProperty(k)));
					}
					return ht;
				} else if (t.IsGenericType) {
					System.Type gt = t.GetGenericTypeDefinition();
					if (gt == typeof(Dictionary<,>)) {
						System.Type kt = t.GetGenericArguments()[0];
						if (kt == typeof(string)) {
							System.Type vt = t.GetGenericArguments()[1];
							IDictionary di = (IDictionary)System.Activator.CreateInstance(t);
							foreach (Data kd in d.IterableKeys) {
								string k = ((String)kd).v;
								di.Add(k, Deserialize(vt, d.ReadProperty(k)));
							}
							return di;
						}
					}
				}

				// --> Class
				object oc = System.Activator.CreateInstance(t);
				FieldInfo[] fis = t.GetFields();
				for (int i = 0; i < fis.Length; ++i) {
					if (d.HasProperty(fis[i].Name)) {
						object e = Deserialize(fis[i].FieldType, d[fis[i].Name]);
						fis[i].SetValue(oc, e);
					} else {
						if (System.Attribute.GetCustomAttribute(fis[i], typeof(Required)) != null) {
							throw new DeserializationConstraintException("Class " + t + " required field " + fis[i].Name + " cannot be defined.");
						}
					}
				}
				PropertyInfo[] pis = t.GetProperties();
				for (int i = 0; i < pis.Length; ++i) {
					if (pis[i].CanWrite) {
						if (d.HasProperty(pis[i].Name)) {
							object e = Deserialize(pis[i].PropertyType, d[pis[i].Name]);
							pis[i].SetValue(oc, e, null);
						} else {
							if (System.Attribute.GetCustomAttribute(pis[i], typeof(Required)) != null) {
								throw new DeserializationConstraintException("Class " + t + " required property " + pis[i].Name + " cannot be defined.");
							}
						}
					}
				}
				return oc;
			} else {
				throw new DeserializationException("Cannot convert " + d.GetType() + " to " + t);
			}
		}

		public static Data SerializeFlat(object n) {
			return Serialize(n, Serialization.Options.Flat);
		}

		public static Data Serialize(object n, Options options = Options.None) {
			try {
				if (n == null) {
					return Data.Null;
				} else if (n is Data) {
					return (Data)n;
				}

				System.Type nt = n.GetType();
				if (nt == typeof(int) || nt == typeof(float) || nt == typeof(long) || nt == typeof(double) || nt == typeof(uint) || nt == typeof(ulong)
					|| nt == typeof(short) || nt == typeof(ushort) || nt == typeof(char) || nt == typeof(byte) || nt == typeof(sbyte)) {
					return new Number(System.Convert.ToDouble(n));
				} else if (nt == typeof(string)) {
					return new String((string)n);
				} else if (nt == typeof(bool)) {
					return new Bool((bool)n);
				} else if (nt.IsEnum) {
					return new String(n.ToString());
				} else {
					if ((options & Options.Flat) == Options.None) {
						Object o = null;
						if (nt.IsArray) {
							System.Array na = (System.Array)n;
							Array a = new Array();
							for (int i = 0; i < na.Length; ++i) {
								a.array.Add(new Property() { data = Serialize(na.GetValue(i), options) });
							}
							return a;
						} else if (nt == typeof(ArrayList)) {
							ArrayList nal = (ArrayList)n;
							Array a = new Array();
							for (int i = 0; i < nal.Count; ++i) {
								a.array.Add(new Property() { data = Serialize(nal[i], options) });
							}
							return a;
						} else if (nt == typeof(Hashtable)) {
							Hashtable nht = (Hashtable)n;
							o = new Object();
							foreach (object k in nht.Keys) {
								if (k is string) {
									o[(string)k] = Serialize(nht[k], options);
								}
							}
							return o;
						} else if (nt.IsGenericType) {
							System.Type gt = nt.GetGenericTypeDefinition();
							if (gt == typeof(List<>)) {
								IList nl = (IList)n;
								Array a = new Array();
								for (int i = 0; i < nl.Count; ++i) {
									a.array.Add(new Property() { data = Serialize(nl[i], options) });
								}
								return a;
							} else if (gt == typeof(Dictionary<,>)) {
								System.Type kt = nt.GetGenericArguments()[0];
								if (kt == typeof(string)) {
									IDictionary nd = (IDictionary)n;
									o = new Object();
									foreach (object ok in nd.Keys) {
										string sk = (string)ok;
										o[sk] = Serialize(nd[ok], options);
									}
									return o;
								}
							}
						}

						o = new Object();
						if ((options & Options.IgnoreFields) == Options.None) {
							FieldInfo[] fis = nt.GetFields(BindingFlags.Instance | BindingFlags.Public);
							for (int i = 0; i < fis.Length; ++i) {
								if (System.Attribute.GetCustomAttribute(fis[i], typeof(DoNotSerialize)) == null) {
									o[fis[i].Name] = Serialize(fis[i].GetValue(n), options);
								}
							}
						}
						if ((options & Options.IgnoreProperties) == Options.None) {
							PropertyInfo[] pis = nt.GetProperties(BindingFlags.Instance | BindingFlags.Public);
							for (int i = 0; i < pis.Length; ++i) {
								if (pis[i].CanRead && pis[i].GetIndexParameters().Length == 0) {
									if (System.Attribute.GetCustomAttribute(pis[i], typeof(DoNotSerialize)) == null) {
										o[pis[i].Name] = Serialize(pis[i].GetValue(n, null), options);
									}
								}
							}
						}
						if ((options & Options.PreserveTypeName) != Options.None) {
							o["$type"] = Data.Of(nt.AssemblyQualifiedName);
						}
						return o;
					} else {
						return new Native(n);
					}
				}
			} catch (System.Exception e) {
				throw new SerializationException(e.Message);
			}
		}
	}

	internal class Util {
		internal static MethodInfo GetStaticMethod(System.Type type, string method) {
			return type.GetMethod(method, BindingFlags.Static | BindingFlags.NonPublic);
		}

		internal static MethodInfo[] GetMethods(System.Type type, string name, BindingFlags extraFlags = BindingFlags.Default) {
			MethodInfo[] allMethods = type.GetMethods(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public | extraFlags);
			int numMethods = 0;
			for (int i = 0; i < allMethods.Length; ++i) {
				if (allMethods[i].Name == name) {
					numMethods++;
				}
			}
			MethodInfo[] methods = new MethodInfo[numMethods];
			numMethods = 0;
			for (int i = 0; i < allMethods.Length; ++i) {
				if (allMethods[i].Name == name) {
					methods[numMethods++] = allMethods[i];
				}
			}
			return methods;
		}
	}

	public class SourceMap {
		class SegmentInfo {
			public int mapcolumn = 0;
			public int file = 0;
			public int line = 0;
			public int column = 0;
			public int name = 0;

			public SegmentInfo Copy() {
				return (SegmentInfo)this.MemberwiseClone();
			}
		}
		public int version = 0;
		public string file = "";
		public string sourceRoot = "";
		public string[] sources = new string[] {};
		public string[] names = new string[] {};
		public string mappings = "";

		List<List<SegmentInfo>> segments = new List<List<SegmentInfo>>();

		public static SourceMap Parse(string source) {
			Data json = JSON.parse(source);
			SourceMap sm = (SourceMap)Serialization.Deserialize(typeof(SourceMap), json);
			if (sm.version == 3) {
				SegmentInfo current = new SegmentInfo();
				sm.segments.Add(new List<SegmentInfo>());
				int pp = 0;
				while (pp < sm.mappings.Length) {
					if (sm.mappings[pp] == ';') {
						sm.segments.Add(new List<SegmentInfo>());
						current.mapcolumn = 0;
						pp++;
					} else if (sm.mappings[pp] == ',') {
						pp++;
					} else {
						current.mapcolumn += Base64.DecodeVLQSegment(sm.mappings, ref pp);
						if (sm.mappings[pp] != ';' && sm.mappings[pp] != ',') {
							current.file += Base64.DecodeVLQSegment(sm.mappings, ref pp);
							current.line += Base64.DecodeVLQSegment(sm.mappings, ref pp);
							current.column += Base64.DecodeVLQSegment(sm.mappings, ref pp);
							if (sm.mappings[pp] != ';' && sm.mappings[pp] != ',') {
								current.name += Base64.DecodeVLQSegment(sm.mappings, ref pp);
							}
						}
						sm.segments[sm.segments.Count - 1].Add(current.Copy());
					}
				}
				return sm;
			} else {
				throw new ParseError("Source map: version 3 required.");
			}
		}

		public string GetLocation(int mapline, int mapcolumn) {
			if (mapline < segments.Count) {
				List<SegmentInfo> ls = segments[mapline];
				if (ls.Count != 0) {
					int a = 0;
					int b = ls.Count - 1;
					while (a < b) {
						int mid = (a + b + 1) / 2;
						if (ls[mid].column > mapcolumn) {
							b = mid - 1;
						} else if (ls[mid].column <= mapcolumn) {
							a = mid;
						}
					}
					return sources[ls[a].file] + ":" + (ls[a].line + 1);
				}
			}
			return "unmapped(" + (mapline+1) + ":" + mapcolumn + ")?";
		}
	}

	public class Profiler {
		internal Dictionary<SourceLocation, int> samples;
		public int sampleDistance = 0;
		internal int cyclesToSample = 0;
		public bool isRecording = false;
		public SourceMap sourceMap = null;

		public Profiler() {
			samples = new Dictionary<SourceLocation, int>();
		}

		public void Reset() {
			samples.Clear();
		}

		internal void TrySample(SourceLocation location) {
			if (!isRecording) {
				return;
			}

			if (cyclesToSample == 0) {
				if (location == null) {
					AddSample(SourceLocation.nullLocation);
				} else {
					AddSample(location);
				}

				cyclesToSample = sampleDistance;
			} else {
				cyclesToSample--;
			}
		}

		internal void AddSample(SourceLocation location) {
			if (samples.ContainsKey(location)) {
				samples[location] = samples[location] + 1;
			} else {
				samples[location] = 1;
			}
		}

		public Dictionary<string, int> GenerateReport() {
			Dictionary<string, int> report = new Dictionary<string, int>();
			foreach (KeyValuePair<SourceLocation, int> item in samples) {
				if (sourceMap != null) {
					report[sourceMap.GetLocation(item.Key.row, 0)] = item.Value;
				} else {
					report[item.Key.ToString()] = item.Value;
				}
			}
			return report;
		} 
	}

	public class JS {
		Scope rootContext;
		Data thisValue;
		static Profiler profiler = new Profiler();

		public JS() {
			rootContext = new Scope();
			rootContext.thisObject = rootContext.activationObject;
			Data r = rootContext.thisObject;
			r.SetProperty("console", Data.Of(typeof(Console)));
			r.SetProperty("Math", Data.Of(typeof(Math)));
			r.SetProperty("JSON", Data.Of(typeof(JSON)));
			r.SetProperty("Object", Object.ObjectConstructor);
			r.SetProperty("Array", Array.ArrayConstructor);
			r.SetProperty("Number", Number.NumberConstructor);
			r.SetProperty("String", String.StringConstructor);
			r.SetProperty("Boolean", Bool.BoolConstructor);
			r.SetProperty("Date", Date.DateConstructor);

			r.SetProperty("parseInt", new NativeFunction(Util.GetStaticMethod(typeof(Globals), "ParseInt")));
			r.SetProperty("parseFloat", new NativeFunction(Util.GetStaticMethod(typeof(Globals), "ParseFloat")));
			r.SetProperty("isNaN", new NativeFunction(Util.GetStaticMethod(typeof(Globals), "IsNaN")));
			r.SetProperty("NaN", Data.Of(double.NaN));

			thisValue = rootContext.thisObject;
		}

		public Data this[string name] {
			get { return thisValue[name]; }
			set { thisValue[name] = value; }
		}
		
		public Data Eval(string source, string filename="") {
			Node rootNode = new Parser(source, filename).Parse();
			Machine machine = new Machine();
			Code code = new Code();
			rootNode.Compile(code);
#if JS_VERBOSE
			JS.VerboseLog("Compilation: ");
			JS.VerboseLog("===========");
			foreach (Instruction ins in code.code) {
				JS.VerboseLog("@@@: " + ins.operation + ", " + ins.param);
			}
			JS.VerboseLog("===========");
#endif
			machine.Enter(code, rootContext);
			machine.Run();
			return machine.PopResult();
		}

		public static void VerboseLog(string log) {
#if UNITY_EDITOR
			UnityEngine.Debug.LogWarning(log);
#else
			System.Console.WriteLine(log);
#endif
		}

		public static Profiler GetProfiler() {
			return JS.profiler;
		}

		public string CallLog(int size, string sourcemap="") {
			if (Machine.Current != null) {
				StackTrace.RecordCall(Machine.Current.currentInstruction);

				return StackTrace.GetLog(sourcemap);
			} else {
				return "";
			}
		}
	}
}
