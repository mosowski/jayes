using System;
using System.Collections.Generic;
using System.Linq;

class JSRun {
	public static void Main(string[] argsArray) {
		List<string> args = argsArray.ToList();
		if (args.IndexOf("--repl") != -1) {
			Repl();
		} else if (args.IndexOf("--sourcemap") != -1) {
			TestSourceMap(args[1]);
		} else {
			string code="", line;
			while ((line=Console.ReadLine()) != null) {
				code += line + "\n";
			}
			JS.JS js = new JS.JS();
			try {
				System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
				sw.Start();
				js.Eval(code);
				sw.Stop();
				if (args.IndexOf("--time") != -1) {
					Console.WriteLine("ExecutionTime = " + sw.ElapsedMilliseconds);
				}
			} catch (Exception e) {
				System.Console.WriteLine(js.CallLog(7));
				throw e;
			}
		}
	}

	public static void Repl() {
		JS.JS js = new JS.JS();
		while (true) {
			Console.Write(">");
			string code = Console.ReadLine();
			object result = js.Eval(code);
			Console.WriteLine(result);
		}
	}

	public static void TestSourceMap(string fileName) {
		string sourceMap = new System.IO.StreamReader(fileName).ReadToEnd();
		System.Console.WriteLine("Parsing Source map...");
		JS.SourceMap sm = JS.SourceMap.Parse(sourceMap);
		System.Console.WriteLine("...Ok  "  + sm.sources.Length);
	}
}
