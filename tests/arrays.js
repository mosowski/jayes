
var tests = [
	function() {
		var ob = {};
		ob.a = 2;
		ob["b"] = 3;
		ob[3] = 5;
		console.log(ob["a"] * ob.b * ob[3]);
	},

	function() {
		console.log("" + [1,2,[4,5,[6,7]],8,9]);
	},

	function() {
		var arr = [];
		console.log(arr.length);

		arr[5] = 123;
		console.log(arr.length);
		arr[100] = "0";
		console.log(arr.length);
		arr.length = 4;
		console.log(arr.length);
		console.log(arr[5]);
	},

	function() {
		var arr = [1,2,3,4,5,6,7];
		arr["b"] = 3;
		console.log(arr.indexOf(3));
		var s = arr.splice(arr.indexOf(3));

		console.log(s.toString());
		console.log(arr.toString());
		console.log(arr["b"]);
		arr = [4,1,2,1];
		console.log(arr.indexOf(1));
		console.log(arr.indexOf(5));
	},

	function() {
		arr = [1,2,3,4,5,6,7];
		while (arr.length) {
			console.log(arr.toString());
			arr.shift();
		}
	},

	function() {
		arr = [1,2,3,4,5,6,7];
		arr = arr.map(function(item, index, array) {
			return item*item;
		});
		console.log(arr.toString());
		var context = { param: 5 };
		arr = arr.map(function(item, index, array) {
			return item - this.param;
		}, context);
		console.log(arr.toString());
	},

	function() {
		var arr = [1,2,3,4,5,6,7];
		arr = arr.filter(function(item) {
			return item % 2 == 0;
		});
		console.log(arr.toString());
	},

	function() {
		arr = [];
		arr.push(1);
		arr.push(2);
		arr.push(8);
		arr.push(9);
		arr.splice(arr.indexOf(8),0,3,4,5,6,7);
		console.log(arr.toString());
	},

	function() {
		var o1 = {};
		var o2 = {};
		arr = [1, o1, null, o2];
		console.log(arr.indexOf(o2));
	},

	function() {
		var a = [1,2,3];
		var b = [4,5,6];
		var c = a.concat(b);
		console.log(c.toString());
	},

	function() {
		var a = [1,2,3,4,5,6,7,8,9];
		console.log(a.slice(3,3).toString());
		console.log(a.slice(-3,3).toString());
		console.log(a.slice(-4).toString());
	},

	function() {
		var a = [1,2,3,4,5,6,7,8,9];
		a.reverse();
		console.log(a.toString());
	},

	function() {
		var a = [1,2,3,1,2,3,1,2,3];
		console.log(a.lastIndexOf(1));
	}
];

for (var i = 0; i < tests.length; ++i) {
	console.log("Test " + i);
	tests[i]();
}
