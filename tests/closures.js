
var genericGenerator = function(changer) {
	var v = 1;
	return {
		current: function() {
			return v;
		},
		next: function() {
			v = changer(v);
		}
	};
};

var evens = genericGenerator(function(v) { return v+2; });
var pots = genericGenerator(function(v) { return v*2; } );

while (evens.current() < 20) {
	console.log(evens.current());
	evens.next();
	console.log(pots.current());
	pots.next();
}

var nestea = function(a,b,c) {
	a += 1;
	return function() {
		b += 1;
		return function() {
			c += 1;
			return function() {
				return a+b+c;
			};
		};
	};
};

var lastOne = nestea(4,10,30)()();
console.log(lastOne());
console.log(nestea.length);

function ternaryFunction(a,b,c) {
	console.log("A: "+ a);
	console.log("B: "+ b);
	console.log("C: "+ c);
}

ternaryFunction("first", 2);
