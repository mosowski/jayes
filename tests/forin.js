
var tests = [
	function() {
		var table = [6,5,4,3,2,1];
		table["x"] = 7;
		for (var i in table) {
			console.log(i + " --> " + table[i]);
		}
	},

	function() {
		var dict = { a: 10, b: 20, c: 30, d: 40 };
		for (var key in dict) {
			console.log(key + " --> " + dict[key]);
		}
	},

	function() {
		var str = "abc"

		for (var c in str) {
			console.log(c + " --> " + str[c]);
		}
	},

	function() {
		var baseClass = function() {
			this.x = 3;
		};

		baseClass.prototype.y = 4;

		var childClass = function() {
			this.z = 5;
		};

		childClass.prototype = new baseClass();

		var childInstance = new childClass();
		for (var key in childInstance) {
			console.log(key + " --> " + childInstance[key]);
		}
	}

];

for (var i = 0; i < tests.length; ++i) {
	console.log("Test " + i);
	tests[i]();
}
