
console.log("255 and 13 = " + (255 & 13));
console.log("63 or 192 = " + (63 | 192));
console.log("255 xor 192 = " + (255 ^ 192));
console.log("~~3.14 = " + ~~3.14);

var x = 3;
x <<= 4;
console.log("x = 3; x <<= 4; x = " + x);

x = 50;
x >>= 2;
console.log("x = 50; x >>= 2; x = " + x);
