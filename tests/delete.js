

console.log("----- test 1 -----");

var hash = { a: 1, b: 2, c: 3, nested: { a: 4, b: 5 }, nested2: {  a: 6 } };
delete hash.a;
delete hash.nested.b;
delete hash.nested2;

console.log(hash.a);
console.log(hash.b);
console.log(hash.nested.a);
console.log(hash.nested.b);
console.log(hash.nested2);

console.log("----- test 2 -----");

function Class() {
	this.field = 2;
}

Class.prototype.method = function() { this.field += this.sharedField; }
Class.prototype.sharedField = 10;

Class.Static = 123;

var c = new Class();
c.method();
console.log(c.field);
console.log(c.sharedField);
delete c.field;
delete c.sharedField;
console.log(c.field);
console.log(c.sharedField);

delete Class.Static;
console.log(Class.Static);

console.log("----- test 3 -----");
var array = [ 1, 2, 3, 4 ];
array["x"] = 100;
console.log(array.x);
delete array.x;
delete array[1];

console.log(array.toString());

console.log("----- test 3 -----");

delete 1;
delete "abc";
delete false;

