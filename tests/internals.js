
var item = { n: 1 };

item.__defineGetter__("prop", function() { return this.n; });
item.__defineSetter__("prop", function(v) { this.n = v*v; });

item.prop = 15;
console.log(item.prop);
