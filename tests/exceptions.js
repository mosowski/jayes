
var tests = [
	function() {
		var f = function() {
			for (var i = 0; i < 5; ++i) {
				console.log(i);
				break;
			}
			console.log("X");
		}

		try {
			console.log("Try");
			f();
		} finally {
			console.log("Finally");
		}
		console.log("Done");
	},

	function() {
		try {
			console.log("Try");
			for (var i = 0; i < 5; ++i) {
				console.log(i);
				break;
			}
			console.log("X");
		} finally {
			console.log("Finally");
		}
		console.log("Done");
	},

	function() {
		try {
			console.log("Try");
			for (var i = 0; i < 5; ++i) {
				console.log(i);
				return;
			}
			console.log("X");
		} finally {
			console.log("Finally");
		}
		console.log("Done");
	},

	function() {
		try {
			console.log("Try");
			for (var i = 0; i < 5; ++i) {
				console.log(i);
				throw "error";
			}
			console.log("X");
		} catch (e) {
			console.log("Catch " + e);
		} finally {
			console.log("Finally");
		}
		console.log("Done");
	},

	function() {
		console.log((function() {
			try {
				console.log("Try");
				for (var i = 0; i < 5; ++i) {
					console.log(i);
					throw "error";
				}
				console.log("X");
			} catch (e) {
				console.log("Catch");
				return 100;
			} finally {
				console.log("Finally");
				return 200;
			}
			console.log("AfterTry");
			return 300;
		})());
		console.log("Done");
	},

	function() {
		console.log((function() {
			try {
				console.log("Try");
				throw "error";
				console.log("X");
			} catch (e) {
				console.log("Catch");
				return 100;
			} finally {
				console.log("Finally");
			}
			console.log("B");
		})());
		console.log("Done");
	},

	function() {
		console.log((function() {
			try {
				try {
					console.log("Try");
					throw "error";
				} finally {
					console.log("X");
				}
			} catch (e) {
				console.log("Catch");
				return 100;
			} finally {
				console.log("Finally");
			}
			console.log("B");
		})());
		console.log("Done");
	},

	function() {
		console.log((function() {
			try {
				try {
					console.log("Try");
					throw "error";
				} catch (e) {
					console.log("Inner catch");
					return 50;
				} finally {
					console.log("X");
				}
			} catch (e) {
				console.log("Catch");
				return 100;
			} finally {
				console.log("Finally");
			}
			console.log("B");
		})());
		console.log("Done");
	},

	function() {
		var f = function() {
			console.log("A");
			throw "error";
			console.log("B");
		}

		try {
			f();
		} catch (e) {
			console.log("Catch");
		} finally {
			console.log("Finally");
		}
		console.log("Done");
	},

	function() {
		try {
			try {
				console.log("A");
				throw "error";
			} catch (e) {
				console.log("Inner catch");
				throw "error2";
			} finally {
				console.log("Inner finally");
			}
			console.log("B");
		} catch (e) {
			console.log("Catch");
		} finally {
			console.log("Finally");
		}
		console.log("Done");
	},

	function() {
		console.log((function() { 
			try {
				try {
					console.log("A");
					throw "error";
				} catch (e) {
					console.log("Inner catch");
					throw "error2";
				} finally {
					console.log("Inner finally");
					return 100;
				}
				console.log("B");
			} catch (e) {
				console.log("Catch " + e);
			} finally {
				console.log("Finally");
			}
		})());
		console.log("Done");
	},

	function() {
		console.log((function() {
			try {
				console.log("Try");
				throw "error";
			} catch (e) {
				console.log("Catch");
				return 100;
			}
			console.log("B");
		})());
		console.log("Done");
	},

	function() {
		console.log((function() {
			try {
				console.log("Outer Try");
				try {
					console.log("Try");
					return;
					console.log("B");
				} finally {
					console.log("Finally");
					throw "error";
					console.log("AfterThrow");
				}
			} catch (e) {
				console.log("OuterCatch");
				return 100;
			}
			console.log("AfterTry");
		})());
		console.log("Done");
	}
];

for (var i = 0; i < tests.length; ++i) {
	console.log("Test " + i);
	tests[i]();
}

