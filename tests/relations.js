
var tests = [
	function() {
		console.log("1 ----");
		console.log(2+2 == 4);
		console.log("a" == "a");
		console.log("0" == 0);
		console.log(2 == "2");
		console.log("2" == 2);
		console.log(2 != "2");
		console.log("2" != 2);

		console.log("1.1 ----");
		console.log(NaN != NaN);
		console.log(NaN == NaN);
		console.log([] == null);

		console.log("2 ----");

		console.log("a" === "a");
		var a = [1,2,3];
		var b = (function() { return a; })();
		console.log(b === a);

		console.log("aab" > "aaa");
		console.log("aaab" < "aaa");
		console.log("summer night city" == "summer night city");

		console.log("3 ----");

		function Base() { this.x = 100; };
		function Inherited() { this.y = 200; };
		Inherited.prototype = new Base();
		var oBase = new Base();
		var oInherited = new Inherited();
		console.log("x" in oInherited);
		console.log("y" in oInherited)
		console.log(oInherited.hasOwnProperty("x"));
		console.log(oInherited.hasOwnProperty("y"))

		console.log("4 ----");
		console.log(oBase instanceof Base);
		console.log(oBase instanceof Inherited);
		console.log(oInherited instanceof Base);
		console.log(oInherited instanceof Inherited);
	},

	function() {
		console.log("q1: "+ ([] instanceof Object));
		console.log("q2: "+ ([] instanceof Array));
		console.log("q3: "+ ({} instanceof Object));
		console.log("q4: "+ ((new Array()) instanceof Array));
	},

	function() {
		var a = undefined;
		console.log("b1: " + (a == null));
		console.log("c1: " + (a == undefined));
		console.log("d1: " + (a === undefined));
		console.log("e1: " + (a === null));
	},

	function() {
		var a = 123;
		console.log("b2: " + (a != null));
		console.log("c2: " + (a != undefined));
		console.log("d2: " + (a !== undefined));
		console.log("e2: " + (a !== null));
	},

	function() {
		var c = { };
		c.d = 1;
		console.log("b3: " + (c.e == null));
		console.log("c3: " + (c.d == null));
		console.log("d3: " + (c.e == undefined));
		console.log("e3: " + (c.d == undefined));
	},

	function() {
		console.log("z1: " + (1 == 3 ? 4 : 5));
		console.log("z2: " + (4 == (2+2) ? 4 : 5));
	},

	function() {
		var a = 2 == 3 ? 4 : 5;
		console.log(a);

		var f = function(n) { return n*n; };
		var b = f(3) == 9 ? 11 : 12;
		console.log(b);
	},

	function() {
		var f = function() { console.log("a"); };
		var g = function(f) { return f; }
		var a = typeof f == 'function' ? g(f) : f;
		console.log(a());
	},

	function() {
		var f = function() { console.log("a"); };
		var g = function(f) { return f; }
		var a = typeof (f) == 'function' ? g(f) : f;
		console.log(a());
	},

	function() {
		var f = 34 || false;
		var g = false || 42;
		var h = false || 0;
		var i = false || undefined;
		console.log(f);
		console.log(g);
		console.log(h);
		console.log(i);
	},

	function() {
		var f = 34 && true;
		var g = true && 42;
		var h = 1 && true;
		console.log(f);
		console.log(g);
		console.log(h);
	}
];

for (var i = 0; i < tests.length; ++i) {
	console.log("Test " + i);
	tests[i]();
}


