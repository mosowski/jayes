
var tests = [
	function() {
		console.log(typeof 1);
	},

	function() {
		console.log(typeof "2");
	},

	function() {
		console.log(typeof false);
	},

	function() {
		console.log(typeof {});
	},

	function() {
		console.log(typeof function() { });
	},

	function() {
		console.log(typeof null);
	},

	function() {
		console.log(typeof undefined);
	}
];

for (var i = 0; i < tests.length; ++i) {
	console.log("Test " + i);
	tests[i]();
}
