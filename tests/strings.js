
var tests = [
	function() {
		console.log("abc" + "def");
	},

	function() {
		var s = "abc";
		var t = "def";
		console.log(s.concat(t));
	},

	function() {
		var t = "abccbbccba";
		console.log(t.indexOf("cc", 1));
		console.log(t.lastIndexOf("cc", 4));
	},

	function() {
		var t = "abcdefghi";
		var s = t.slice(3, 3);
		var u = t.slice(3);
		var v = t.slice(-3, -2);
		var s = t.slice(100, 100);
		console.log(t + s);
		console.log(u);
		console.log(v);
	},

	function() {
		var s = "abc  def  ghi";
		var t = "abc def ghi";
		var u = "abc,def,,ghi";
		console.log(s.split("  ").toString());
		console.log(s.split(" ").toString());
		console.log(t.split(" ").toString());
		console.log(u.split(",").toString());
		console.log(u.split("").toString());
		console.log(u.split().toString());
	},

	function() {
		var t = "abcdefghi";
		console.log(t.substr(3,3));
		console.log(t.substr(-3,3));
		console.log(t.substr(3));
		console.log(t.substr(100,100));
	},

	function() {
		var t = "abcdefghi";
		console.log(t.substring(3,6));
		console.log(t.substring(6,3));
		console.log(t.substring(-3));
		console.log(t.substring(100,100));
	},

	function() {
		console.log("hello WORLD ąć!".toLowerCase());
		console.log("hello WORLD ąć!".toUpperCase());
	},

	function() {
		console.log("  \n\t\r  remove whitspace    \n\n\r".trim());
	}


];

for (var i = 0; i < tests.length; ++i) {
	console.log("Test " + i);
	tests[i]();
}
