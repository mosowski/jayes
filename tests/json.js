
var objSrc = {
	fieldNumber: 123,
	fieldString: "abc",
	fieldBool: true,
	fieldExpr: 2 > 3 ? "nope" : 5,
	fieldObj: {
		nestedA: 321,
		nestedB: "b",
		"nestedC": "c"
	},
	array: [1,2,3,4,{ x: 3, y :12 }]
};


var json = JSON.stringify(objSrc);
console.log(json);
var obj = JSON.parse(json);

console.log(obj.fieldNumber);
console.log(obj.fieldString);
console.log(obj.fieldBool);
console.log(obj.fieldExpr);
console.log(obj.fieldObj.nestedA);
console.log(obj.fieldObj.nestedB);
console.log(obj.fieldObj.nestedC);

