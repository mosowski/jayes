
function* values() {
	for (var i = 0; i < 5; ++i) {
		yield [5,6,7,8,9][i];
	}
}

var tests = [
	function() {
		function* gen() {
			var a = 1;
			var b = 1;
			for (;;) {
				yield a;
				var t = a;
				a = b;
				b += t;
			}
		}

		var g = gen();

		for (var i = 0; i < 1000; i = g.next().value) {
			console.log(i);
		}
	},

	function() {
		function* square(what) {
			for (var j of what) {
				yield j*j;
			}
		}

		for (var i of square(values())) {
			console.log(i);
		}
	},

	function() {
		function* duplicate(what, n) {
			while (n--) {
				yield* what();
			}
		}

		for (var i of duplicate(values, 3)) {
			console.log(i);
		}
	},

	function() {
		for (var j of (function*() { })()) {
			console.log("X");
		}
	},

	function() {
		// not supported by v8 yet
		//var arr = [0,0,0,0,0,0];
		//var i = 0;
		//for (arr[i++] of values()) {
			//console.log("ok " + arr[i-1]);
		//}
		//console.log(arr);
	}
];

for (var i = 0; i < tests.length; ++i) {
	console.log("Test " + i);
	tests[i]();
}
