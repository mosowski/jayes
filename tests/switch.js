
var tests = [
	function() {
		for (var i = 0; i < 5; ++i) {
			switch(i) {
				case 0:
					console.log("zero");
					break;
				case 1:
				case 2:
					console.log("one or two");
					break;
				case 3:
					console.log("three");
				default:
					console.log("default");
			}
		}
	},
	function() {
		var arr = ["a", "b", "c", "d"];
		for (var i in arr) {
			switch(arr[i]) {
				case "a":
					console.log("A");
				case "b":
					console.log("B");
				case "c":
					console.log("C");
					break;
				default:
					console.log("default");
			}
		}
	}
];

for (var i = 0; i < tests.length; ++i) {
	console.log("Test " + i);
	tests[i]();
}
