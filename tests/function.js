
var tests = [
	function() {
		var method = function(c) {
			return (this.a + this.b) * c;
		};

		var o = { a: 7, b: 11 };
		console.log(method.apply(o, [ 3 ]));
		console.log(method.call(o, 13));
	},

	function() {
		var o = Object.create(Object.prototype);
		o.f = 2;
		console.log(o.f);
	},

	function() {
		var baseClass = function(a) {
			this.x = a;
		};

		baseClass.prototype.baseMethod = function() {
			console.log(this.x * this.x);
		};

		var childClass = function(a, b) {
			baseClass.call(this, a);
			this.y = b;
		};

		childClass.prototype = Object.create(baseClass.prototype);

		childClass.prototype.method = function() {
			console.log(this.x * this.y);
		};

		var o = new childClass(3, 7);
		o.baseMethod();
		o.method();
	}
];

for (var i = 0; i < tests.length; ++i) {
	console.log("Test " + i);
	tests[i]();
}
