
tests = [
	function () {
		var u = 1;
		var o = {
			foo: function() {
				return u * 2;
			}
		};

		with(o) {
			console.log(foo());
		}
	},

	function () {
		this.x = 3;
		this.y = 7;
		var o = {
			foo: function(p) {
				return p.x * p.y;
			}
		};
		with (o) {
			console.log(foo(this));
		}
	}
];

for (var i = 0; i < tests.length; ++i) {
	console.log("Test " + i);
	tests[i]();
}
