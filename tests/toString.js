var tests = [
	function() {
		console.log({}.toString());
	},

	function() {
		console.log([].toString());
		console.log(Object.prototype.toString.call([]));
	},

	function() {
		var f = function() {
			console.log(Object.prototype.toString.call(arguments));
		};

		f();
	},

	function() {
		console.log("" + null);
		console.log("" + undefined);
		console.log("" + []);
		console.log("" + 1);
		console.log("" + true);
		console.log("" + false);
	}
];

for (var i = 0; i < tests.length; ++i) {
	console.log("Test " + i);
	tests[i]();
}
