
function Point(x,y) {
	this.x = x;
	this.y = y;
};

Point.prototype.dotProduct = function(rhs) {
	return this.x*rhs.x + this.y*rhs.y;
};

Point.XAxis = new Point(1,0);
Point.YAxis = new Point(0,1);

var tests = [
	function() {
		console.log(new Point(0.123, 0.456).dotProduct(Point.XAxis) + new Point(0.789, 0.012).dotProduct(Point.YAxis)); 
	},
	
	function() {
		var pointInstance = new Point(0,0);
		var indirectInstance = new pointInstance.constructor(3,4);
		console.log(indirectInstance.x);
	},

	function() {
		var simpleObject = { };
		var nativeObjectCtor = simpleObject.constructor;
		var antoherSimpleObject = new nativeObjectCtor();
		console.log(antoherSimpleObject.constructor === simpleObject.constructor);
	},

	function() {
		var baseClass = function() {
			this.baseField = 5;
			this.baseRefField = { m: 5 };
		};

		baseClass.prototype.baseMethod= function() {
			console.log("baseField: " + this.baseField);
			console.log("baseRefField: " + this.baseRefField.m);
		};

		var childClass = function() {
			this.childField = 17;
		};

		childClass.prototype = new baseClass();

		childClass.prototype.childMethod = function() {
			this.baseField *= 2;
			this.baseRefField.m *= 2;
			console.log("childField: " + this.childField);
		}

		var child = new childClass();
		child.childMethod();
		child.baseMethod();

		var child2 = new childClass();
		child2.baseMethod();
	}
];

for (var i = 0; i < tests.length; ++i) {
	console.log("Test " + i);
	tests[i]();
}


