
for (var i = 0; i < 100; i++) {
	console.log(i);
	if (i < 10) {
		console.log("continue");
		continue;
	}
	console.log(i*i);
	if (i > 20) {
		console.log("break");
		break;
	}
}

i = 0;
while (true) {
	console.log(i);
	if (i < 10) {
		i++;
		console.log("continue");
		continue;
	}
	console.log(i*i);
	if (i > 20) {
		console.log("break");
		break;
	}
	i++;
}
