
var a = 1;
function incA(n) {
	a += n;
}
var incA2 = function (n) {
	a += n;
}

while (a < 10) {
	console.log(a + 7);
	incA(1);
	incA2(1);
}

for (var i = 10; i < 20; i++) {
	console.log(i * i);
}

function getTrue() {
	console.log("TRUE");
	return true;
}

function getFalse() {
	console.log("FALSE");
	return false;
}

if (getTrue() && getFalse()) {
	console.log("OK1");
}

if (getFalse() && getTrue()) {
	console.log("OK2");
}

if (getTrue() || getFalse()) {
	console.log("OK3");
}

if (getFalse() || getTrue()) {
	console.log("OK4");
}
//console.log("123 + 431 = " + (123 + 421));
//console.log("123 + 431 = " + (123 + 421));
