
var tests = [
	function() {
		console.log("begin");
		l1: for (var i = 0; i < 100; ++i) {
			if (i > 50) {
				break l1;
			}
			console.log(i);
		}

		l2: for (var i = 0; i < 100; ++i) {
			if (i % 10 < 9) {
				continue l2;
			}
			console.log(i);
		}

		l22: {
			console.log("22");
			break l22;
			console.log("222");
		}

		console.log("end");
	},

	function() {
		console.log("begin");
		l1: for (var i = 0; i < 10; ++i) {
			if (i == 2) {
				continue l1;
			} else if (i == 5) {
				continue;
			} else if (i == 7) {
				break;
			}
			console.log(i);
		}
	}
];

for (var i = 0; i < tests.length; ++i) {
	console.log("Test " + i);
	tests[i]();
}
