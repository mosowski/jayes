using System.Collections;
using System.Collections.Generic;

public class Vec2 {
	public float x;
	public float y;

	// ctor
	public Vec2(float x=0, float y=0) {
		this.x = x;
		this.y = y;
	}

	// default ctor, required for mapping deserialziation
	public Vec2() {
		x = y = 0;
	}

	// method
	public float GetSqLength() {
		return x*x + y*y;
	}

	// setter/getter
	[JS.Serialization.DoNotSerialize]
	public float Magnitude {
		set {
			float l = value / (float)System.Math.Sqrt(GetSqLength());
			x *= l;
			y *= l;
		}
		get {
			return (float)System.Math.Sqrt(GetSqLength());
		}
	}
}

public class IntegratedClass {
	public int intField = 123;
	public string strField = "abc";
	public Vec2 vecField = new Vec2(1,2);

	// lists
	public List<string> strList = new List<string> { "a", "b", "c" };
	public List<int> intList = new List<int> { 1, 2, 3 };
	public List<Vec2> vec2List = new List<Vec2> { new Vec2(1,2), new Vec2(2,3), new Vec2(3,4) };
	public List<List<int>> nestedList = new List<List<int>>() { new List<int>() { 1, 2 }, new List<int>() {2, 3}, new List<int>() { 3, 4} };

	// arrays
	public string[] strArray = new string[] { "a", "b", "c" };
	public int[] intArray = new int[] { 1, 2, 3 };
	public Vec2[] vec2Array = new Vec2[] { new Vec2(1,2), new Vec2(2,3), new Vec2(3,4) };
	public int[][] nestedArray = new int[3][] { new int[2] { 1, 2}, new int[2] {2, 3}, new int[2] { 3, 4 } };

	// arguments conversion
	public void PrintIntList(List<int> l) {
		for (int i = 0; i < l.Count; ++i) {
			System.Console.WriteLine(l[i]);
		}
	
	}
	public void PrintIntArray(int[] l) {
		for (int i = 0; i < l.Length; ++i) {
			System.Console.WriteLine(l[i]);
		}
	}

	public void PrintNestedList(List<List<int>> l) {
		for (int i = 0; i < l.Count; ++i) {
			for (int j = 0; j < l[i].Count; ++j) {
				System.Console.WriteLine(l[i][j]);
			}
		}
	}

	// function conversion
	public void CallAction(System.Action a) {
		a();
	
	}
	public void CallUnaryAction(System.Action<object> a) {
		a("def");
	}

	public object CallUnaryFunc(System.Func<object,object> f) {
		return f("abc");
	}

	public void ValidateInstance(IntegratedClass ic) {
		System.Console.WriteLine(ic.intField == intField);
		System.Console.WriteLine(ic.strField == strField);
		System.Console.WriteLine(ic.vecField.x == vecField.x);
		System.Console.WriteLine(ic.vecField.y == vecField.y);
	}

	public void CheckVec2Dict(Dictionary<string,Vec2> vd) {
		foreach (string k in vd.Keys) {
			System.Console.WriteLine(k + ":" + vd[k].x + "," + vd[k].y);
		}
	}
}

class InteropTests {
	public static void Main(string[] args) {
		string script = @"
			console.log('----- 1 -----');
			console.log(ic.intField);
			console.log(ic.strField);
			console.log(ic.vecField.x);
			console.log(ic.vecField.y);

			console.log('----- 2 -----');
			for (var i = 0; i < ic.strList.length; ++i) {
				console.log(ic.strList[i]);
			}

			console.log('----- 3 -----');
			for (var i = 0; i < ic.intList.length; ++i) {
				console.log(ic.intList[i]);
			}

			console.log('----- 4 -----');
			for (var i = 0; i < ic.vec2List.length; ++i) {
				console.log(ic.vec2List[i].x);
				console.log(ic.vec2List[i].y);
			}

			console.log('----- 5 -----');
			for (var i = 0; i < ic.nestedList.length; ++i) {
				for (var j = 0; j < ic.nestedList[i].length; ++j) {
					console.log(ic.nestedList[i][j]);
				}
			}

			console.log('----- 6 -----');
			for (var i = 0; i < ic.strArray.length; ++i) {
				console.log(ic.strArray[i]);
			}

			console.log('----- 7 -----');
			for (var i = 0; i < ic.intArray.length; ++i) {
				console.log(ic.intArray[i]);
			}

			console.log('----- 8 -----');
			for (var i = 0; i < ic.vec2Array.length; ++i) {
				console.log(ic.vec2Array[i].x);
				console.log(ic.vec2Array[i].y);
			}

			console.log('----- 9 -----');
			for (var i = 0; i < ic.nestedArray.length; ++i) {
				for (var j = 0; j < ic.nestedArray[i].length; ++j) {
					console.log(ic.nestedArray[i][j]);
				}
			}
			
			console.log('-----10 -----');
			var v = new Vec2(5,7);
			console.log(v.x);
			console.log(v.y);
			console.log(v.GetSqLength());
			console.log(v.Magnitude);
			v.Magnitude = 12;
			console.log(v.x);
			console.log(v.y);
			console.log(v.Magnitude);

			console.log('-----11 -----');
			ic.strList.push('d');
			for (var i = 0; i < ic.strList.length; ++i) {
				console.log(ic.strList[i]);
	
			}
			console.log('-----12 -----');
			ic.PrintIntList([1,2,3]);
			ic.PrintIntArray([1,2,3]);
			ic.PrintNestedList([[1,2],[2,3],[3,4]]);

			console.log('-----13 -----');
			ic.CallAction(function() { console.log('abcdef'); });
			ic.CallUnaryAction(function(str) { console.log('abc' + str); });
			console.log(ic.CallUnaryFunc(function(str) { return str + 'def'; }));

			console.log('-----14 -----');
			ic.ValidateInstance({
				intField: 123,
				strField: 'abc',
				vecField: { x: 1, y: 2},

				strList: [ 'a', 'b', 'c' ],
				intList: [ 1, 2, 3],
				vec2List: [ { x: 1, y: 2 }, { x: 2, y: 3 }, { x: 3, y: 4 } ],
				nestedList: [ [ 1 ,2 ], [ 2, 3 ], [3, 4] ], 

				strArray: [ 'a', 'b', 'c' ],
				intArray: [ 1, 2, 3],
				vec2Array: [ { x: 1, y: 2 }, { x: 2, y: 3 }, { x: 3, y: 4 } ],
				nestedArray: [ [ 1 ,2 ], [ 2, 3 ], [3, 4] ]
			});

			console.log('-----15 -----');
			console.log(JSON.stringify(serializedIc));
			ic.ValidateInstance(serializedIc);

			console.log('-----16 -----');
			ic.CheckVec2Dict({
				k1: { x: 1, y: 2},
				k2: { x: 2, y: 3},
				k3: { x: 3, y: 4}
			});
		";
		IntegratedClass ic = new IntegratedClass();

		JS.JS js = new JS.JS();
		js["Vec2"] = JS.Data.Of(typeof(Vec2));
		js["ic"] = JS.Data.Of(ic);
		js["serializedIc"] = JS.Serialization.Serialize(ic);

		js.Eval(script);
	}
}