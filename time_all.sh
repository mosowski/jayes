#!/bin/bash

function run_jayes {
	for t in tests/*.js
	do
		mono JS.exe < $t > /dev/null
	done
}

function run_node {
	for t in tests/*.js
	do
		node --harmony $t > /dev/null
	done
}

time run_jayes
time run_node
