#!/bin/bash
echo "running interop tests..."
mcs -debug JS.cs interop-tests/InteropTests.cs -out:InteropTests.exe
mono --debug InteropTests.exe > tmp
diff -b interop-tests/interop-tests-out tmp
rm InteropTests.exe tmp
